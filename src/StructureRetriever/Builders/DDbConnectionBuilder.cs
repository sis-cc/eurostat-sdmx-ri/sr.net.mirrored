// -----------------------------------------------------------------------
// <copyright file="DDbConnectionBuilder.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Builders
{
    using System;
    using System.Data.Common;

    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Builder;

    /// <summary>
    ///     Dissemination database connection builder
    /// </summary>
    internal class DDbConnectionBuilder : IBuilder<DbConnection, StructureRetrievalInfo>
    {
        #region Constants and Fields

        /// <summary>
        /// The builder
        /// </summary>
        private readonly IBuilder<DbConnection, DdbConnectionEntity> _builder;

        /// <summary>
        /// Initializes a new instance of the <see cref="DDbConnectionBuilder"/> class.
        /// </summary>
        /// <param name="builder">The builder.</param>
        public DDbConnectionBuilder(IBuilder<DbConnection, DdbConnectionEntity> builder)
        {
            _builder = builder;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The method that builds a <see cref="DbConnection" /> from the specified <paramref name="info" />
        /// </summary>
        /// <param name="info">
        ///     The current Data retrieval state
        /// </param>
        /// <returns>
        ///     The <see cref="DbConnection" />
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="info"/> is <see langword="null" />.</exception>
        public DbConnection Build(StructureRetrievalInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            DbConnection dbConnection = this._builder.Build(info.MappingSet.DisseminationConnection);
            dbConnection.Open();
            return dbConnection;
        }

        #endregion
    }
}