// -----------------------------------------------------------------------
// <copyright file="StructureRetrievalInfoBuilder.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.StructureRetriever;

namespace Estat.Nsi.StructureRetriever.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.CustomRequests.Constants;
    using Estat.Sri.CustomRequests.Model;
    using Estat.Sri.Mapping.Api.Exceptions;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     Builds a <see cref="StructureRetrievalInfo" /> object
    /// </summary>
    internal class StructureRetrievalInfoBuilder : IStructureRetrievalInfoBuilder
    {
        /// <summary>
        ///     The logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(StructureRetrievalInfoBuilder));

        #region Public Methods and Operators

        /// <summary>
        /// Build a <see cref="StructureRetrievalInfo" /> from the specified parameters
        /// </summary>
        /// <param name="dataflow">The dataflow to get the available data for</param>
        /// <param name="allowedDataflows">The collection of allowed dataflows</param>
        /// <param name="settings">The settings.</param>
        /// <param name="dataflowMutable">The dataflow mutable.</param>
        /// <param name="dsd">The DSD.</param>
        /// <returns>
        /// a <see cref="StructureRetrievalInfo" /> from the specified parameters
        /// </returns>
        /// <exception cref="System.ArgumentNullException">settings
        /// or
        /// dataflow</exception>
        /// <exception cref="SdmxException">Parsing error or mapping store exception error</exception>
        /// <exception cref="ArgumentNullException">connectionStringSettings is null</exception>
        /// <exception cref="ArgumentNullException">connectionStringSettings is null</exception>
        public StructureRetrievalInfo Build(IConstrainableStructureReference dataflow, IList<IMaintainableRefObject> allowedDataflows, StructureRetrieverSettings settings, IDataflowMutableObject dataflowMutable, IDataStructureMutableObject dsd)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }

            if (dataflow == null)
            {
                throw new ArgumentNullException("dataflow");
            }

            var info = new StructureRetrievalInfo(allowedDataflows, settings);
            try
            {
                var dataflowObject = dataflowMutable.ImmutableInstance;
                var dataStructureObject = dsd.ImmutableInstance;
                info.DataStructure = dataStructureObject;
                info.Dataflow = dataflowObject;

                // TODO check with OECD. Do we need PIT with partial codelists (SDMX v2.0 / NSI Client legacy request)
                info.MappingSet = settings.MappingManager.GetBuilders(settings.StoreId, dataflowObject, dataStructureObject, null, false);
                if (info.MappingSet != null)
                {
                    ParserDataflowRef(dataflow, info, dataStructureObject);
                    Initialize(info, dataStructureObject);
                }
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException e)
            {
                string mesage = "Mapping Store connection error." + e.Message;
                _logger.Error(mesage);
                _logger.Error(e.ToString());
                throw new SdmxException(e, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), mesage);
            }
            catch (Exception e)
            {
                string mesage = string.Format(
                    CultureInfo.CurrentCulture, 
                    ErrorMessages.ErrorRetrievingMappingSetFormat4, 
                    dataflow.MaintainableReference.AgencyId, 
                    dataflow.MaintainableReference.MaintainableId, 
                    dataflow.MaintainableReference.Version, 
                    e.Message);
                _logger.Error(mesage);
                _logger.Error(e.ToString());
                throw new SdmxException(e, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), mesage);
            }

            return info;
        }

        /// <summary>
        /// Build a <see cref="StructureRetrievalInfo" /> from the specified parameters
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="settings">The settings.</param>
        /// <returns>a <see cref="StructureRetrievalInfo" /> from the specified parameters</returns>
        /// <exception cref="ArgumentNullException">connectionStringSettings is null</exception>
        /// <exception cref="SdmxException">Parsing error or mapping store exception error</exception>
        /// <exception cref="System.ArgumentNullException">connectionStringSettings is null</exception>
        /// <exception cref="ArgumentNullException">Parsing error or mapping store exception error</exception>
        public StructureRetrievalInfo Build(IAvailableConstraintQuery query, StructureRetrieverSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }

            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            if (query.Dataflow == null)
            {
                /* This is when datastructure available data is requested, 
                 * but there is no way of knowing for which dataflow to retrieve the mapping set for.
                 * Such a request could be made possible when the application is capable of attaching a mapping set to a DSD.
                 * The IAvailableConstraintQuery implementation in sdmxsource sets Dataflow to null instead of throwing an exception 
                 * in order not to distrupt the usage of the class in all cases.
                 * So this case must be handled here.
                 */
                throw new SdmxNoResultsException($"Could not find available data for datastructure {query.DataStructure}.");
            }

            // normally we already know if we can access a dataflow or not by now
            var info = new StructureRetrievalInfo(null, settings);
            try
            {
                info.DynamicQuery = query;
                var dataflowObject = query.Dataflow;
                var dataStructureObject = query.DataStructure;
                info.DataStructure = dataStructureObject;
                info.Dataflow = dataflowObject;

                // TODO check with OECD. Do we need PIT with available constraints
                info.MappingSet = settings.MappingManager.GetBuilders(settings.StoreId, dataflowObject, dataStructureObject, null, false);
                if (info.MappingSet != null)
                {
                    ParserDataflowRef(query, info);
                    Initialize(info, dataStructureObject);
                }
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException e)
            {
                string mesage = "Mapping Store connection error." + e.Message;
                _logger.Error(mesage);
                _logger.Error(e.ToString());
                throw new SdmxException(e, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), mesage);
            }
            catch (ResourceNotFoundException e)
            {
                string mesage = string.Format(
                    CultureInfo.CurrentCulture,
                    ErrorMessages.ErrorRetrievingMappingSetFormat4,
                    query.Dataflow.AgencyId,
                    query.Dataflow.Id,
                    query.Dataflow.Version,
                    e.Message);
                _logger.Error(mesage);
                _logger.Error(e.ToString());
                throw new SdmxException(e, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.NoResultsFound), mesage);
            }
            catch (Exception e)
            {
                string mesage = string.Format(
                    CultureInfo.CurrentCulture,
                    ErrorMessages.ErrorRetrievingMappingSetFormat4,
                    query.Dataflow.AgencyId,
                    query.Dataflow.Id,
                    query.Dataflow.Version,
                    e.Message);
                _logger.Error(mesage);
                _logger.Error(e.ToString());
                throw new SdmxException(e, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), mesage);
            }

            return info;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initialize component mappings for coded components and time dimension used in mappings in the dataflow
        /// </summary>
        /// <param name="info">The current structure retrieval state</param>
        /// <param name="dataStructureObject">The data structure object.</param>
        /// <remarks>
        /// This method should be called only once
        /// </remarks>
        private static void Initialize(StructureRetrievalInfo info, IDataStructureObject dataStructureObject)
        {
            info.MastoreAccess = info.Settings.SpecialRetrievalManager;
            info.XSMeasureDimensionConstraints.Clear();

            var dataset = info.MappingSet.Dataset;
            
            var query = dataset.EditorType == MultiQueryDatasetEditorEngine.EditorType
                ? MultiQueryDatasetEditorEngine.GenerateSqlQuery(dataset, ObservationActionEnumType.Active.ToString())
                : dataset.Query;

            info.ComponentMapping.Clear();
            info.InnerSqlQuery = query;
            info.MeasureComponent = null;
            info.TimeTranscoder = null;
            info.TimeMapping = null;
            info.TimeDimension = null;
            bool measureDimensionMapped = false;
            var measureDimension =
                dataStructureObject.GetDimensions(SdmxStructureEnumType.MeasureDimension).FirstOrDefault();
            foreach (var mapping in info.MappingSet.ComponentMappings)
            {
                var component = dataStructureObject.GetComponent(mapping.Value.Component.ObjectId);
                if (component.HasCodedRepresentation())
                {
                    if (component.Equals(measureDimension))
                    {
                        measureDimensionMapped = true;
                    }

                    var compInfo = new ComponentInfo
                                       {
                                           Mapping = mapping.Value,
                                           ComponentMapping =
                                               info.MappingSet.ComponentMappingBuilders[component.Id]
                                       };
                    compInfo.CodelistRef.MaintainableId = component.Representation.Representation.MaintainableId;
                    compInfo.CodelistRef.Version = component.Representation.Representation.Version;
                    compInfo.CodelistRef.AgencyId = component.Representation.Representation.AgencyId;
                    var id = component.Id;
                    info.ComponentMapping.Add(id, compInfo);
                    if (id.Equals(info.RequestedComponent))
                    {
                        info.RequestedComponentInfo = compInfo;
                    }

                    if (component.Equals(dataStructureObject.FrequencyDimension))
                    {
                        info.FrequencyInfo = compInfo;
                    }
                }
            }

            if (info.MappingSet.TimeDimensionMapping != null)
            {
                info.TimeTranscoder = info.MappingSet.TimeDimensionBuilder;
                info.TimeMapping = info.MappingSet.TimeDimensionMapping;
                info.TimeDimension = dataStructureObject.TimeDimension?.Id;
                Debug.Assert(info.TimeDimension != null, "DSD with no TimeDimension but TimeMapping is set");
            }

            if (!measureDimensionMapped)
            {
                info.MeasureComponent = measureDimension?.Id;

                var crossDsd = dataStructureObject as ICrossSectionalDataStructureObject;
                if (crossDsd != null)
                {
                    foreach (var xsMeasure in crossDsd.CrossSectionalMeasures)
                    {
                        info.XSMeasureDimensionConstraints.Add(xsMeasure.Id);
                    }
                }
            }
        }

        /// <summary>
        ///     Gets the requested component unique identifier.
        /// </summary>
        /// <param name="dsdEntity">The DSD entity.</param>
        /// <param name="conceptId">The requested component.</param>
        /// <returns>
        ///     The Component.Id
        /// </returns>
        private static string GetRequestedComponentId(IDataStructureObject dsdEntity, string conceptId)
        {
            // TODO double check why work with concept ids
            if (dsdEntity.TimeDimension != null && dsdEntity.TimeDimension.ConceptRef.ChildReference.Id.Equals(conceptId))
            {
                conceptId = dsdEntity.TimeDimension.Id;
            }
            else
            {
                var dimension = dsdEntity.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension).FirstOrDefault(entity => entity.ConceptRef.ChildReference.Id.Equals(conceptId));
                conceptId = dimension == null ? conceptId : dimension.Id;
            }

            return conceptId;
        }

        /// <summary>
        /// Parse the specified DataflowRefBean object and populate the
        /// <see cref="StructureRetrievalInfo.RequestedComponent" /> and <see cref="StructureRetrievalInfo.Criteria" /> fields
        /// </summary>
        /// <param name="d">The DataflowRefBean to parse</param>
        /// <param name="info">The current structure retrieval state</param>
        /// <param name="dataStructureObject">The data structure object.</param>
        private static void ParserDataflowRef(IConstrainableStructureReference d, StructureRetrievalInfo info, IDataStructureObject dataStructureObject)
        {
            if (d.ConstraintObject != null && d.ConstraintObject.IncludedCubeRegion != null)
            {
                foreach (IKeyValues member in d.ConstraintObject.IncludedCubeRegion.KeyValues)
                {
                    if (member.Values.Count == 0 ||
                        (member.Values.Count == 1 && SpecialValues.DummyMemberValue.Equals(member.Values[0])))
                    {
                        info.RequestedComponent = GetRequestedComponentId(dataStructureObject, member.Id);
                    }
                    else
                    {
                        IKeyValuesMutable normalizedMember = new KeyValuesMutableImpl(member)
                        {
                            Id = GetRequestedComponentId(dataStructureObject, member.Id)
                        };
                        var keyValuesCore = new KeyValuesCore(normalizedMember, member.Parent);
                        info.Criteria.Add(keyValuesCore);
                    }
                }

                info.ReferencePeriod = d.ConstraintObject.MutableInstance.ReferencePeriod;
            }
        }

        /// <summary>
        /// Parse the specified DataflowRefBean object and populate the
        /// <see cref="StructureRetrievalInfo.RequestedComponent" /> and <see cref="StructureRetrievalInfo.Criteria" /> fields
        /// </summary>
        /// <param name="d">The DataflowRefBean to parse</param>
        /// <param name="info">The current structure retrieval state</param>
        private static void ParserDataflowRef(IAvailableConstraintQuery d, StructureRetrievalInfo info)
        {
            // TODO we have multiple components
            // info.RequestedComponent = d.ComponentId;
            // info.CodelistRef = component.Representation.Representation;
            
            info.Criteria.AddAll(d.Selections);

            if (d is IAvailableConstraintQueryWithComponentFilters queryV2)
            {
                info.ComponentFilters.AddAll(queryV2.ComponentFilters);
            }

            if (d.DataStructure.TimeDimension != null)
            {
                info.StartPeriod = d.DateFrom;
                info.EndPeriod = d.DateTo;
            }
       }

        #endregion
    }
}