// -----------------------------------------------------------------------
// <copyright file="SqlBuilderBase.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Linq;
    using System.Text;

    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Utils;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     The base class for SQL Builders
    /// </summary>
    internal abstract class SqlBuilderBase
    {
        #region Methods

        /// <summary>
        ///     Returns string containing all column names used in the specified <paramref name="mappings" /> separated by comma.
        /// </summary>
        /// <param name="mappings">The mappings.</param>
        /// <param name="reservedKeywordToStringFormat"></param>
        /// <returns>A string containing all column names used in the specified <paramref name="mappings" /> separated by comma.</returns>
        /// <exception cref="System.ArgumentException">No mapping provided.</exception>
        protected static string ToColumnNameString(IMappingEntity[] mappings, string reservedKeywordToStringFormat)
        {
            if (mappings == null || mappings.Length == 0)
            {
                throw new ArgumentException("No mapping provided.");
            }

            var columnNames = new HashSet<string>(
                mappings.SelectMany(m => m.GetColumns()).Select(c => GetColumnName(c, reservedKeywordToStringFormat)),
                StringComparer.OrdinalIgnoreCase
            );

            return string.Join(", ", columnNames);
        }

        private static string GetColumnName(DataSetColumnEntity c, string reservedKeywordToStringFormat)
        {
            return string.IsNullOrEmpty(reservedKeywordToStringFormat)
                ? c.Name
                : string.Format(reservedKeywordToStringFormat, c.Name);
        }

        /// <summary>
        ///     This method generates the WHERE part of the SQL query that will be used against the DDB for retrieving the
        ///     available codes
        /// </summary>
        /// <param name="info">
        ///     The current structure retrieval information
        /// </param>
        /// <param name="componentId"></param>
        /// <param name="includeHistory"></param>
        /// <returns>
        ///     A string containing the WHERE part of the SQL Query or an Empty string
        /// </returns>
        protected string GenerateWhere(StructureRetrievalInfo info, string componentId, bool includeHistory)
        {
            var sb = new StringBuilder();
            int lastClause = 0;
            info.Parameters.Clear();
            foreach (IKeyValues member in info.Criteria)
            {
                if (!string.IsNullOrEmpty(member.Id))
                {
                    if (member.Id.Equals(info.TimeDimension))
                    {
                        if (member.Values.Count > 0)
                        {
                            ISdmxDate startDate = new SdmxDateCore(member.Values[0]);
                            ISdmxDate endDate = null;
                            if (member.Values.Count > 1)
                            {
                                endDate = new SdmxDateCore(member.Values[1]);
                            }

                            sb.Append("(");
                            sb.Append(
                                info.TimeTranscoder.GenerateWhere(startDate, endDate, null));
                            sb.Append(")");
                            lastClause = sb.Length;
                            sb.Append(" AND ");
                        }
                    }
                    else
                    {
                        IComponentMappingBuilder compInfo;
                        if (info.MappingSet.ComponentMappingBuilders.TryGetValue(member.Id, out compInfo))
                        {
                            if (info.DynamicQuery != null && info.DynamicQuery.Mode == AvailableConstraintQueryMode.Available && !string.IsNullOrEmpty(componentId) && member.Id == componentId)
                            {
                                continue;
                            }
                            sb.Append("(");
                            OperatorType operatorType = RESTQueryOperator2OperatorTypeConverter.Convert(member.OperatorType);
                            if (operatorType == OperatorType.Exact)
                            {
                                var valuesHash = new HashSet<string>(member.Values, StringComparer.Ordinal);
                                sb.Append(compInfo.GenerateComponentWhere(valuesHash, valuesHash.Contains(string.Empty)));
                                lastClause = sb.Length;
                            }
                            else
                            {
                                foreach (string value in member.Values)
                                {
                                    sb.Append(compInfo.GenerateComponentWhere(value, operatorType));
                                    lastClause = sb.Length;
                                    sb.Append(" OR ");
                                }
                            }

                            sb.Length = lastClause;
                            if (lastClause > 0)
                            {
                                sb.Append(")");
                                lastClause = sb.Length;
                                sb.Append(" AND ");
                            }
                        }
                    }
                }
            }

            if (info.ComponentFilters.Any(f => !f.IsTimeRange))
            {
                var builder = new ComponentFiltersSqlBuilder(info.ComponentFilters, info.MappingSet.ComponentMappingBuilders);
                sb.Append(builder.GetSqlQuery());
                lastClause = sb.Length;
                sb.Append(" AND ");
            }

            if (info.ReferencePeriod != null)
            {
                // Depreciated way
                IReferencePeriodMutableObject time = info.ReferencePeriod;

                sb.Append("(");
                sb.Append(info.TimeTranscoder.GenerateWhere(
                                                time.StartTime.HasValue ? new SdmxDateCore(time.StartTime, TimeFormatEnumType.DateTime) : null, 
                                                time.EndTime.HasValue ? new SdmxDateCore(time.EndTime, TimeFormatEnumType.DateTime) : null, 
                                                null));
                sb.Append(")");
                lastClause = sb.Length;
            }
            else if (info.StartPeriod != null || info.EndPeriod != null)
            {
                if (info.DataStructure.TimeDimension != null)
                {
                    //AvailableConstraintQueryMode.Available should display all available members
                    if (info.DynamicQuery == null || info.DynamicQuery.Mode != AvailableConstraintQueryMode.Available || string.IsNullOrEmpty(componentId) || info.DataStructure.TimeDimension.Id != componentId)
                    {
                        // new way
                        sb.Append("(");
                        sb.Append(
                            info.TimeTranscoder.GenerateWhere(info.StartPeriod, info.EndPeriod, null));
                        sb.Append(")");
                        lastClause = sb.Length;
                        sb.Append(" AND ");
                    }
                }
            }

            sb.Length = lastClause;
            lastClause = this.GenerateSpecialMappingsWhere(info, sb, lastClause, includeHistory);

            sb.Length = lastClause;
            if (sb.Length > 0)
            {
                return " where " + sb;
            }

            return string.Empty;
        }

        /// <summary>
        /// Generates the update status where.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="sb">The where buffer.</param>
        /// <param name="lastClause">The last clause.</param>
        /// <param name="includeHistory"></param>
        /// <returns>The last position of the <paramref name="sb"/> </returns>
        private int GenerateSpecialMappingsWhere(StructureRetrievalInfo info, StringBuilder sb, int lastClause, bool includeHistory)
        {
            IUpdateStatusMappingBuilder updateStatus = info.MappingSet.UpdateStatusMappingBuilder;

            SqlClause sqlClause = null;
            if (info.DynamicQuery == null)
            {
                if (info.MappingSet.ValidToMapping != null)
                {
                    var validToClause = info.MappingSet.ValidToMappingBuilder.GenerateWhere();
                    if (sb.Length > 0)
                    {
                        sb.Append(" AND ");
                    }

                    sb.Append(" ");
                    sb.Append(validToClause.WhereClause);
                    lastClause = sb.Length;
                }

                if (info.MappingSet.UpdateStatusMapping != null)
                {
                    sqlClause = updateStatus.GenerateWhere(ObservationAction.GetFromEnum(ObservationActionEnumType.Active));
                }
            }
            else
            {
                if (info.DynamicQuery.LastUpdatedDate != null)
                {
                    if (info.MappingSet.LastUpdateMapping == null)
                    {
                        return lastClause;
                    }

                    sqlClause = info.MappingSet.LastUpdateMappingBuilder.GenerateWhere(
                        new ITimeRange[]
                        {
                            new Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex.TimeRangeCore(
                                false,
                                info.DynamicQuery.LastUpdatedDate,
                                null,
                                true,
                                false)
                        });

                    if (info.MappingSet.ValidToMapping != null && !includeHistory)
                    {
                        var validToClause = info.MappingSet.ValidToMappingBuilder.GenerateWhere();
                        if (sb.Length > 0)
                        {
                            sb.Append(" AND ");
                        }

                        sb.Append(" ");
                        sb.Append(validToClause.WhereClause);
                        lastClause = sb.Length;
                    }
                }
                else if (info.MappingSet.UpdateStatusMapping != null)
                {
                    if (info.MappingSet.ValidToMapping != null && !includeHistory)
                    {
                        sqlClause = updateStatus.GenerateWhere(ObservationAction.GetFromEnum(ObservationActionEnumType.Active));

                        var validToClause = info.MappingSet.ValidToMappingBuilder.GenerateWhere();
                        if (sb.Length > 0)
                        {
                            sb.Append(" AND ");
                        }

                        sb.Append(" ");
                        sb.Append(validToClause.WhereClause);
                        lastClause = sb.Length;
                    }
                }
            }
            
            if (sqlClause != null && !string.IsNullOrWhiteSpace(sqlClause.WhereClause))
            {
                if (sb.Length > 0)
                {
                    sb.Append(" AND ");
                }

                sb.Append(" ");
                sb.Append(sqlClause.WhereClause);
                lastClause = sb.Length;
                info.Parameters.AddAll(sqlClause.Parameters);
            }

            return lastClause;
        }

        #endregion
    }
}
