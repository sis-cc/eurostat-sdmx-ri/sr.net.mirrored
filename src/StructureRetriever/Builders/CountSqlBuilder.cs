// -----------------------------------------------------------------------
// <copyright file="CountSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

namespace Estat.Nsi.StructureRetriever.Builders
{
    using System.Globalization;

    using Estat.Nsi.StructureRetriever.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     Build SQL query for Count requests
    /// </summary>
    internal class CountSqlBuilder : SqlBuilderBase, ISqlBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Generate the SQL for executing on the DDB
        /// </summary>
        /// <param name="info">
        ///     The current structure retrieval information
        /// </param>
        /// <param name="includeHistory"></param>
        /// <returns>
        ///     The generated SQL.
        /// </returns>
        public string GenerateSql(StructureRetrievalInfo info, bool includeHistory)
        {
            var whereClause = this.GenerateWhere(info, string.Empty, includeHistory);

            if (info.DynamicQuery == null ||
                (!info.DynamicQuery.FirstNObservations.HasValue && !info.DynamicQuery.LastNObservations.HasValue) ||
                string.IsNullOrEmpty(info.MappingSet.Dataset.DatasetPropertyEntity?.CrossApplyColumn))
            {
                return string.Format(CultureInfo.InvariantCulture,
                    "SELECT COUNT(*) \n FROM ({0}) virtualDataset {1}",
                    info.InnerSqlQuery, whereClause);
            }

            var observationCountPerSeries = info.DynamicQuery.FirstNObservations ?? 0 + info.DynamicQuery.LastNObservations ?? 0;

            var dimensionAtObservation = info.DynamicQuery.DimensionAtObservation.Equals("AllDimensions")
                ? info.TimeDimension
                : info.DynamicQuery.DimensionAtObservation;

            string groupByColumns;

            if (string.IsNullOrEmpty(dimensionAtObservation) || dimensionAtObservation.Equals(info.TimeDimension))
            {
                groupByColumns = info.MappingSet.Dataset.DatasetPropertyEntity.CrossApplyColumn;
            }
            else
            {
                var datasetDimColumns =
                    info.ComponentMapping
                        .Where(cm =>
                            info.DataStructure.GetDimension(cm.Key) != null && !cm.Key.Equals(dimensionAtObservation))
                        .SelectMany(cm => cm.Value.Mapping.GetColumns()).ToList();

                    if (info.MappingSet.TimeDimensionMapping != null)
                    {
                        datasetDimColumns = datasetDimColumns.Concat(info.MappingSet.TimeDimensionMapping.GetColumns()).ToList();
                    }

                    groupByColumns = string.Join(",", datasetDimColumns.Select(c => c.Name));
            }


            var sqlStatement = string.Format(
            "SELECT SUM(CC) FROM (SELECT {0}, CASE WHEN COUNT(*) > {1} THEN {1} ELSE COUNT(*) END CC FROM ({2}) virtualDataset {3} GROUP BY {0}) c",
            groupByColumns,
            observationCountPerSeries, info.InnerSqlQuery, whereClause);

            return sqlStatement;
        }

        /// <summary>
        /// Generate the SQL for executing on the DDB
        /// </summary>
        /// <param name="info">The current structure retrieval information</param>
        /// <param name="component">The component.</param>
        /// <param name="includeHistory"></param>
        /// <returns>The generated SQL.</returns>
        public string GenerateSql(StructureRetrievalInfo info, IComponent component, bool includeHistory)
        {
            return GenerateSql(info, includeHistory);
        }

        #endregion
    }
}