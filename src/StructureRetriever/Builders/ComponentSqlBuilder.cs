// -----------------------------------------------------------------------
// <copyright file="ComponentSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.Utils.Helper;

namespace Estat.Nsi.StructureRetriever.Builders
{
    using System.Globalization;
    using System.Xml;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.Mapping.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     SQL Builder for Components
    /// </summary>
    internal class ComponentSqlBuilder : SqlBuilderBase, ISqlBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Generate the SQL for executing on the DDB
        /// </summary>
        /// <param name="info">
        ///     The current structure retrieval information
        /// </param>
        /// <param name="includeHistory"></param>
        /// <returns>
        ///     The generated SQL.
        /// </returns>
        public string GenerateSql(StructureRetrievalInfo info, bool includeHistory)
        {
            IMappingEntity[] mapping;

            if (info.RequestedComponentInfo != null)
            {
                mapping = new[]
                {
                    info.RequestedComponentInfo.Mapping
                };
            }
            else if (info.RequestedComponent.Equals(info.TimeDimension))
            {
                mapping = info.FrequencyInfo != null
                    ? new IMappingEntity[]
                    {
                        info.TimeMapping, info.FrequencyInfo.Mapping
                    }
                    : new[]
                    {
                        info.TimeMapping
                    };
            }
            else
            {
                return null;
            }

            var ddbType = info.MappingSet.DisseminationConnection.DbType;
            var provider = DatabaseType.GetProviderName(ddbType);
            var databaseSetting = DatabaseType.DatabaseSettings[provider];
            var columnList = ToColumnNameString(mapping, databaseSetting?.ReservedKeywordToStringFormat);

            return string.Format(
                CultureInfo.InvariantCulture, 
                "SELECT DISTINCT {2} \n FROM ({0}) virtualDataset {1} \n ORDER BY {2} ", 
                info.InnerSqlQuery, this.GenerateWhere(info,string.Empty, includeHistory), 
                columnList);
        }

        public string GenerateSql(StructureRetrievalInfo info, IComponent component, bool includeHistory)
        {
            IMappingEntity[] mapping = GetMappings(info, component);
            if (mapping == null)
            {
                return null;
            }

            var ddbType = info.MappingSet.DisseminationConnection.DbType;
            var provider = DatabaseType.GetProviderName(ddbType);
            var databaseSetting = DatabaseType.DatabaseSettings[provider];
            var columnList = ToColumnNameString(mapping, databaseSetting?.ReservedKeywordToStringFormat);
            return this.BuildSqlquery(info, columnList,component.Id, false);
        }

        private static IMappingEntity[] GetMappings(StructureRetrievalInfo info, IComponent component)
        {
            IMappingEntity[] mapping = null;

            if (component.StructureType.EnumType == Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureEnumType.TimeDimension)
            {
                mapping = info.FrequencyInfo != null
                   ? new IMappingEntity[]
                   {
                        info.TimeMapping, info.FrequencyInfo.Mapping
                   }
                   : new[]
                   {
                        info.TimeMapping
                   };
            }
            else
            {
                ComponentMappingEntity componentMapping;
                if (info.MappingSet.ComponentMappings.TryGetValue(component.Id, out componentMapping))
                {
                    mapping = new[] { componentMapping };
                }
            }

            return mapping;
        }

        private string BuildSqlquery(StructureRetrievalInfo info, string columnList, string componentId, bool includeHistory)
        {
            return string.Format(
                CultureInfo.InvariantCulture,
                "SELECT DISTINCT {2} \n FROM ({0}) virtualDataset {1} \n ORDER BY {2} ",
                info.InnerSqlQuery, this.GenerateWhere(info, componentId, includeHistory),
                columnList);
        }

        #endregion
    }
}