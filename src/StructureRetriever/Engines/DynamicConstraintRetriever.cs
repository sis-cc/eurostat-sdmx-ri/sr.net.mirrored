// -----------------------------------------------------------------------
// <copyright file="DynamicConstraintRetriever.cs" company="EUROSTAT">
//   Date Created : 2018-4-29
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Nsi.StructureRetriever.Engines
{
    using System.Collections.Generic;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.Mapping.Api.Model;

    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    /// Class AvailableDataRetrieval.
    /// </summary>
    /// <seealso cref="Estat.Nsi.StructureRetriever.Engines.IAvailableDataRetrievalEngine" />
    class DynamicConstraintRetriever : IAvailableDataRetrievalEngine
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(DynamicConstraintRetriever));

        /// <summary>
        /// Gets the partial structure.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="component">The component.</param>
        /// <returns>The <see cref="IMaintainableMutableObject" />.</returns>
        public IMutableObjects GetPartialStructure(StructureRetrievalInfo info, IComponent component)
        {
            // TODO a proper factory
            return RetrieveAvailableData(info, component);
        }

        /// <summary>
        /// Retrieve the codelist that is referenced by
        /// </summary>
        /// <param name="info">The current structure retrieval state.</param>
        /// <param name="component">The component.</param>
        /// <returns>The partial structure</returns>
        private IMutableObjects RetrieveAvailableData(
            StructureRetrievalInfo info, IComponent component)
        {
            if (info.MappingSet == null)
            {
                _log.Warn("|-- Warning: Constraints not retrieved. Couldn't find a mapping set.");
                return null;
            }

            _log.InfoFormat("Starting dynamic constraint retriever for dataflow {0}  component {1}", info.Dataflow, component.Id);

            // set the codelist ref
            IMutableObjects mutableObjects = new MutableObjectsImpl();
          
            ComponentMappingEntity mapping;
            if (info.MappingSet.ComponentMappings.TryGetValue(component.Id, out mapping))
            {
                ISet<string> values = GetValues(info, component, mapping);

                IContentConstraintMutableObject contentConstraintMutableObject = CodeListHelper.ConvertToCubeRegion(component, values);
                mutableObjects.AddContentConstraint(contentConstraintMutableObject);
                return mutableObjects;
            }

            return null;
        }

        private static ISet<string> GetValues(StructureRetrievalInfo info, IComponent component, ComponentMappingEntity mapping)
        {
            IQueryEngine queryEngine;
            if (!string.IsNullOrWhiteSpace(mapping.ConstantValue))
            {
                queryEngine = new ConstantQueryEngine();
            }
            else
            {
                queryEngine = new DdbQueryEngine();
            }

            var values = queryEngine.GetValues(info, component);
            return values;
        }
    }
}
