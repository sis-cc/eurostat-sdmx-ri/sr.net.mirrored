// -----------------------------------------------------------------------
// <copyright file="ReferencePeriodRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2018-4-29
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Nsi.StructureRetriever.Engines
{
    using Estat.Nsi.StructureRetriever.Builders;
    using Estat.Nsi.StructureRetriever.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Date;

    /// <summary>
    /// ReferencePeriod Retrieval Engine.
    /// </summary>
    class ReferencePeriodRetrievalEngine
    {
        public IReferencePeriodMutableObject GetReferencePeriod(StructureRetrievalInfo info)
        {
            if (info.DataStructure.TimeDimension == null)
            {
                return null;
            }

            IReferencePeriodMutableObject referencePeriod = new ReferencePeriodMutableCore();
            if (!string.IsNullOrEmpty(info.TimeMapping.ConstantValue))
            {
                var minPeriod = new SdmxDateCore(info.TimeMapping.ConstantValue).Date;
                referencePeriod.StartTime = minPeriod;
                referencePeriod.EndTime = minPeriod;
            }
            else
            {
                ComponentSqlBuilder sqlBuilder = new ComponentSqlBuilder();
                info.SqlQuery = sqlBuilder.GenerateSql(info, info.DataStructure.TimeDimension, false);
                var startEnd = TimeDimensionCodeListRetrievalEngine.RetrieveStartEnd(info);
                referencePeriod.StartTime = startEnd.Item1.Date;
                referencePeriod.EndTime = DateUtil.FormatDateWithOffSet(startEnd.Item2.DateInSdmxFormat, false).DateTime;
            }

            return referencePeriod;
        }
    }
}
