// -----------------------------------------------------------------------
// <copyright file="DdbQueryEngine.cs" company="EUROSTAT">
//   Date Created : 2018-4-29
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using Estat.Nsi.StructureRetriever.Builders;
using Estat.Nsi.StructureRetriever.Model;
using Estat.Sri.Mapping.Api.Builder;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Extensions;
using log4net;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace Estat.Nsi.StructureRetriever.Engines
{
    /// <summary>
    /// Contains the methods that execute a sql query
    /// </summary>
    class DdbQueryEngine : IQueryEngine
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DdbQueryEngine));

        /// <summary>
        /// The SQL builder
        /// </summary>
        private readonly ISqlBuilder _sqlBuilder = new ComponentSqlBuilder();

        /// <summary>
        /// Retrieves the local values.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="cmap">The cmap.</param>
        /// <returns>The local values transcodded</returns>
        public static ISet<string> RetrieveLocalValues(StructureRetrievalInfo info, IComponentMappingBuilder cmap)
        {
            if (info.SqlQuery == null)
            {
                throw new ArgumentException("SQL Query not defined", nameof(info));
            }

            var codesSet = new HashSet<string>(StringComparer.Ordinal);
            using (DbConnection ddbConnection = info.ConnectionBuilder.Build(info))
            {
                using (IDbCommand cmd = ddbConnection.CreateCommand(info.SqlQuery, info.Parameters))
                {
                    cmd.CommandTimeout = 0;
                    _log.InfoFormat("Start executing SQL Query: {0}", info.SqlQuery);
                    using (IDataReader reader = cmd.ExecuteReaderAndLog())
                    {
                        _log.Info("Srart reading records");
                        while (reader.Read())
                        {
                            if (cmap is IComponentArrayMapper)
                            {
                                var arrayMapper = cmap as IComponentArrayMapper;
                                var localCodes = arrayMapper.MapComponentArray(reader);
                                foreach (var localCode in localCodes)
                                {
                                    if (localCode != null && !codesSet.Contains(localCode))
                                    {
                                        codesSet.Add(localCode);
                                    }
                                }
                            }
                            else
                            {
                                string dsdCode = cmap.MapComponent(reader);
                                if (dsdCode != null && !codesSet.Contains(dsdCode))
                                {
                                    codesSet.Add(dsdCode);
                                }
                            }
                        }
                    }
                    _log.Info("SQL Query executed and read");
                }
            }

            return codesSet;
        }

        /// <summary>
        /// Gets the values for the specified <paramref name="component" />.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="component">The component.</param>
        /// <returns>The set of values</returns>
        public ISet<string> GetValues(StructureRetrievalInfo info, IComponent component)
        {
            IComponentMappingBuilder cmap;
            if (info.MappingSet.ComponentMappingBuilders.TryGetValue(component.Id, out cmap))
            {
                info.SqlQuery = _sqlBuilder.GenerateSql(info, component, false);
                _log.InfoFormat("SQL for dissemination database generated: {0}", info.SqlQuery);
                return RetrieveLocalValues(info, cmap);
            }

            return new HashSet<string>(StringComparer.Ordinal);
        }
    }
}
