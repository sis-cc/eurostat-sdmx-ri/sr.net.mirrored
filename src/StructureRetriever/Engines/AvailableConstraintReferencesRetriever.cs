// -----------------------------------------------------------------------
// <copyright file="AvailableConstraintReferencesRetriever.cs" company="EUROSTAT">
//   Date Created : 2018-5-28
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Nsi.StructureRetriever.Engines
{
    
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Nsi.StructureRetriever.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    using Estat.Sdmxsource.Extension.Util;
    using System.Diagnostics;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

    /// <summary>
    /// A decorator that handles referenes.
    /// </summary>
    /// <seealso cref="Estat.Nsi.StructureRetriever.Engines.IAvailableDataRetrievalEngine" />
    class AvailableConstraintReferencesRetriever : IAvailableDataRetrievalEngine
    {
        /// <summary>
        /// The decorated
        /// </summary>
        private readonly IAvailableDataRetrievalEngine _decorated;

        /// <summary>
        /// The codelist filter
        /// </summary>
        private readonly CodelistFilter _codelistFilter = new CodelistFilter();

        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableConstraintReferencesRetriever"/> class.
        /// </summary>
        /// <param name="decorated">The decorated.</param>
        /// <exception cref="ArgumentNullException">decorated</exception>
        public AvailableConstraintReferencesRetriever(IAvailableDataRetrievalEngine decorated)
        {
            if (decorated == null)
            {
                throw new ArgumentNullException(nameof(decorated));
            }

            _decorated = decorated;
        }

        /// <summary>
        /// Gets the partial structure.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="component">The component.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        public IMutableObjects GetPartialStructure(StructureRetrievalInfo info, IComponent component)
        {
            var mutableObjects = _decorated.GetPartialStructure(info, component);
            if (mutableObjects != null)
            {
                IStructureReference structureReference = GetReference(info, component);

                if (structureReference != null)
                {
                    var values = new HashSet<string>(mutableObjects.ContentConstraints.SelectMany(c => c.IncludedCubeRegion.KeyValues).First(k => k.Id.Equals(component.Id)).KeyValues, StringComparer.Ordinal);
                    Debug.Assert(values.Count > 0, "Empty values", "for component {0} returned empty", component.Id);
                    // TODO check if we get cached partial codelisr

                    ICommonStructureQuery structureQuery = CommonStructureQueryCore.Builder
                   .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                   .SetStructureIdentification(structureReference)
                   .SetRequestedDetail(ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full))
                   .SetMaintainableTarget(SdmxStructureEnumType.CodeList)
                   .Build();
                    var itemScheme = info.RetrievalManager.GetMaintainables(structureQuery);

                    var codelist = itemScheme.Codelists.FirstOrDefault().MutableInstance;
                    //var codelist = itemScheme.GetAllMaintainables().FirstOrDefault().MutableInstance as ICodelistMutableObject;

                    Debug.Assert(codelist.Items.Count > 0, "Empty codes", "for component {0} returned empty", component.Id);
                    if (codelist != null)
                    {
                        // we clone the codelist because the codelist mutable object is cached. 
                        // If we change it and is resued we will get back the altered version
                        // we don't use mutable to immutable and back to mutable because it can be very expensive
                        ICodelistMutableObject filteredCodelist = CreateCopy(codelist, new CodelistMutableCore());

                        var filteredCode = _codelistFilter.For(codelist, values).ToArray();
                        Debug.Assert(filteredCode.Length > 0, "Empty filter codes", "filter for component {0} returned empty whith values {1} ", component.Id, values);
                        filteredCodelist.IsPartial = filteredCode.Length < codelist.Items.Count;
                        filteredCodelist.Items.AddAll(filteredCode);
                        mutableObjects.AddCodelist(filteredCodelist);
                    }
                }
            }

            return mutableObjects;
        }

        private static TMain CreateCopy<TMain>(TMain sourceArtefact, TMain copyArterfact) where TMain : IMaintainableMutableObject
        {
            copyArterfact.Id = sourceArtefact.Id;
            copyArterfact.AgencyId = sourceArtefact.AgencyId;
            copyArterfact.Version = sourceArtefact.Version;
            copyArterfact.FinalStructure = sourceArtefact.FinalStructure;
            copyArterfact.StartDate = sourceArtefact.StartDate;
            copyArterfact.EndDate = sourceArtefact.EndDate;
            copyArterfact.StructureURL = sourceArtefact.StructureURL;
            copyArterfact.ServiceURL = sourceArtefact.ServiceURL;
            copyArterfact.Names.AddAll(sourceArtefact.Names);
            if (sourceArtefact.Descriptions != null)
            {
                copyArterfact.Descriptions.AddAll(sourceArtefact.Descriptions);
            }
            if (sourceArtefact.Annotations != null)
            {
                copyArterfact.Annotations.AddAll(sourceArtefact.Annotations);
            }
            return copyArterfact;
        }

        /// <summary>
        /// Gets the reference.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="component">The component.</param>
        /// <returns>The <see cref="IStructureReference" />.</returns>
        private static IStructureReference GetReference(StructureRetrievalInfo info, IComponent component)
        {
            if (info.DynamicQuery == null)
            {
                if (component.HasCodedRepresentation())
                {
                    return component.Representation.Representation;
                }
                return null;
            }

            if(info.DynamicQuery.SpecificStructureReference == null)
            {
                return null;
            }

            IStructureReference structureReference = null;

            SdmxStructureEnumType enumType = info.DynamicQuery.SpecificStructureReference.EnumType;
            if (component.StructureType.EnumType == SdmxStructureEnumType.MeasureDimension)
            {
                var crossDsd = component.MaintainableParent as ICrossSectionalDataStructureObject;
                if (crossDsd == null)
                {
                    if (enumType.IsOneOf(SdmxStructureEnumType.ConceptScheme, SdmxStructureEnumType.Any))
                    {
                        structureReference = component.Representation.Representation;
                    }
                }
                else if (enumType.IsOneOf(SdmxStructureEnumType.CodeList, SdmxStructureEnumType.Any))
                {
                    structureReference = crossDsd.GetCodelistForMeasureDimension(component.Id);
                }
            }
            else if (component.HasCodedRepresentation() && enumType.IsOneOf(SdmxStructureEnumType.CodeList, SdmxStructureEnumType.Any))
            {
                structureReference = component.Representation.Representation;
            }

            return structureReference;
        }
    }
}
