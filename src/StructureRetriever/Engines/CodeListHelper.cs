// -----------------------------------------------------------------------
// <copyright file="CodeListHelper.cs" company="EUROSTAT">
//   Date Created : 2012-04-06
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines
{
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;

    /// <summary>
    ///     This class contains helper method for codelist retrieval engines
    /// </summary>
    internal static class CodeListHelper
    {
        /// <summary>
        /// Converts to cube region.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="items">The codelist.</param>
        /// <returns>The <see cref=" IContentConstraintMutableObject"/>.</returns>
        public static IContentConstraintMutableObject ConvertToCubeRegion(IComponent component, IEnumerable<string> items)
        {
            var contentConstraint = new ContentConstraintMutableCore();
            var keyable = new KeyValuesMutableImpl();
            keyable.Id = component.Id;
            foreach (var item in items)
            {
                keyable.AddValue(item);
            }

            contentConstraint.IncludedCubeRegion = new CubeRegionMutableCore();
            contentConstraint.IncludedCubeRegion.AddKeyValue(keyable);
            return contentConstraint;
        }

        /// <summary>
        /// Converts to cube region.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="codelist">The codelist.</param>
        /// <returns>The <see cref=" IContentConstraintMutableObject"/>.</returns>
        public static IContentConstraintMutableObject ConvertToCubeRegion(IComponent component, ICodelistMutableObject codelist)
        {
            var contentConstraint = new ContentConstraintMutableCore();
            var keyable = new KeyValuesMutableImpl();
            keyable.Id = component.Id;
            foreach (var item in codelist.Items)
            {
                keyable.AddValue(item.Id);
            }

            contentConstraint.IncludedCubeRegion = new CubeRegionMutableCore();
            contentConstraint.IncludedCubeRegion.AddKeyValue(keyable);
            return contentConstraint;
        }

        /// <summary>
        ///     Gets the first codelist from <paramref name="codeLists" /> if any
        /// </summary>
        /// <param name="codeLists">
        ///     The code lists.
        /// </param>
        /// <returns>
        ///     The first codelist from <paramref name="codeLists" />; otherwise null
        /// </returns>
        public static ICodelistMutableObject GetFirstCodeList(ISet<ICodelistMutableObject> codeLists)
        {
            if (codeLists == null)
            {
                return null;
            }

            ICodelistMutableObject firstCL = codeLists.FirstOrDefault();

            if (firstCL != null)
            {
                firstCL.IsPartial = true;
            }

            return codeLists.Count != 0 ? firstCL : null;
        }
    }
}