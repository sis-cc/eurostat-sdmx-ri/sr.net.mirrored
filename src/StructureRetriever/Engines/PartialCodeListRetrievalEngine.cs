// -----------------------------------------------------------------------
// <copyright file="PartialCodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines
{
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.Mapping.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    ///     This <see cref="PartialCodeListRetrievalEngine" /> retrieves partial codelists from DDB and Mapping store.
    /// </summary>
    internal class PartialCodeListRetrievalEngine : ICodeListRetrievalEngine
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Retrieve Codelist
        /// </summary>
        /// <param name="info">
        ///     The current StructureRetrieval state
        /// </param>
        /// <returns>
        ///     A <see cref="ICodelistMutableObject" />
        /// </returns>
        public ICodelistMutableObject GetCodeList(StructureRetrievalInfo info)
        {
            if (info.RequestedComponentInfo == null)
            {
                return null;
            }

            IComponentMappingBuilder cmap = info.RequestedComponentInfo.ComponentMapping;
            var codesSet = DdbQueryEngine.RetrieveLocalValues(info, cmap);

            if (codesSet.Count > 0)
            {
                var subset = codesSet.ToArray();
                MaintainableRefObjectImpl codelistReference = new MaintainableRefObjectImpl(
                      info.CodelistRef.AgencyId,
                      info.CodelistRef.MaintainableId,
                      info.CodelistRef.Version);
                return GetPartialCodelist(info, codelistReference, subset);
            }

            return null;
        }

        public static ICodelistMutableObject GetPartialCodelist(StructureRetrievalInfo info, IMaintainableRefObject codelistReference, IList<string> subset)
        {
            ISet<ICodelistMutableObject> codeLists =
                info.MastoreAccess.GetMutableCodelistObjects(
                codelistReference,
                 subset);

            return CodeListHelper.GetFirstCodeList(codeLists);
        }

        #endregion
    }
}