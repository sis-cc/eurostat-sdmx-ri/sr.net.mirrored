// -----------------------------------------------------------------------
// <copyright file="SpecificResolver.cs" company="EUROSTAT">
//   Date Created : 2013-09-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Sdmx.Util.Sdmx;

namespace Estat.Nsi.StructureRetriever.Engines.Resolver
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

    /// <summary>
    ///     The specific resolver.
    /// </summary>
    internal class SpecificResolver : ResolverBase, IResolver
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="retrievalEngineContainer">Contains all the engines to retrieve any artefacts.</param>
        public SpecificResolver(IRetrievalEngineContainer retrievalEngineContainer)
            : base(retrievalEngineContainer)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Resolves the references of the specified mutable objects.
        /// </summary>
        /// <param name="mutableObjects">
        ///     The mutable objects.
        /// </param>
        /// <param name="queryDetail">
        ///     The <see cref="StructureQueryDetail" /> which controls if the output will include details or not.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed dataflows.
        /// </param>
        /// <param name="specificItems">
        ///     The optional specific item filter.
        /// </param>
        [Obsolete("Use the other overload with the ICommonStructureQuery parameter.")]
        public void ResolveReferences(IMutableObjects mutableObjects, StructureQueryDetail queryDetail,
            IList<IMaintainableRefObject> allowedDataflows, ISet<string> specificItems = null)
        {
            //ResolveSpecific(mutableObjects, this._structureTypes, returnStub, this.CrossReferenceManager,
            //    allowedDataflows, specificItems);
            throw new InvalidOperationException();
        }

        /// <summary>
        /// Fills the <paramref name="mutableObjects"/> with the specific items requested
        /// </summary>
        /// <param name="mutableObjects"></param>
        /// <param name="structureQuery"></param>
        /// <param name="specificItems"></param>
        public void ResolveReferences(
            IMutableObjects mutableObjects, ICommonStructureQuery structureQuery, IList<IMaintainableMutableObject> specificItems)
        {
            ResolveSpecific(mutableObjects, structureQuery, specificItems);
        }

        #endregion

        #region Methods

        private void ResolveSpecific(
            IMutableObjects mutableObjects, ICommonStructureQuery structureQuery, IList<IMaintainableMutableObject> specificItems)
        {
            Func<IMaintainableMutableObject, IList<IMaintainableMutableObject>> reference = maintainableMutableObject =>
            {
                var referencedStructures = new List<IMaintainableMutableObject>();
                foreach (var specificStructureType in structureQuery.SpecificReferences)
                {
                    referencedStructures.AddRange(GetSpecificObjects(
                        maintainableMutableObject,
                        specificStructureType,
                        structureQuery,
                        specificItems));
                }

                return referencedStructures;
            };

            Resolve(mutableObjects, reference);
        }

        private IEnumerable<IMaintainableMutableObject> GetSpecificObjects(
            IMaintainableMutableObject maintainable,
            SdmxStructureType specificStructureType,
            ICommonStructureQuery structureQuery,
            IList<IMaintainableMutableObject> specificItems)
        {
            var specificObjects = new List<IMaintainableMutableObject>();
            //var sdmxStructureRelation = SdmxMaintainableReferenceTree.GetSpecificPath(maintainable.StructureType,
            //    SdmxStructureType.GetFromEnum(specificStructureType));
            SdmxStructureRelation sdmxStructureRelation =
                    SdmxMaintainableReferenceTree.GetSpecificPath(structureQuery.MaintainableTarget, specificStructureType);
            bool returnStub = structureQuery.ReferencedDetail.EnumType == ComplexMaintainableQueryDetailEnumType.Stub;
            AddSpecific(maintainable, structureQuery, specificStructureType, returnStub, specificObjects, sdmxStructureRelation.Descendants);
            AddSpecific(maintainable, structureQuery, specificStructureType, returnStub, specificObjects, sdmxStructureRelation.ParentsAndSiblings, specificItems);
            return specificObjects;
        }

        private void AddSpecific(
            IMaintainableMutableObject maintainable,
            ICommonStructureQuery structureQuery,
            SdmxStructureType specificStructureType, bool returnStub,
            List<IMaintainableMutableObject> specificObjects,
            IReadOnlyList<IReadOnlyList<StructureAndRelation>> relationChains,
            IList<IMaintainableMutableObject> specificItems = null)
        {
            foreach (var relationChain in relationChains)
            {
                var input = new List<IMaintainableMutableObject>()
                {
                    maintainable
                };
                var output = new List<IMaintainableMutableObject>();
                for (var i = 0; i < relationChain.Count - 1; i++)
                {
                    var destinationType = relationChain[i + 1];
                    // TODO replace bool isStub with StructureQueryDetail in order to support CompleteStub
                    var isStub = destinationType.StructureType != specificStructureType || returnStub;
                    var retrievalEngine = RetrievalEngineContainer.GetEngine(destinationType.StructureType);
                    if (retrievalEngine == null)
                    {
                        // not supported
                        continue;
                    }
                    foreach (var maintainableMutableObject in input)
                    {
                        if (destinationType.Relation == StructureRelation.Parent)
                        {
                            ICommonStructureQuery inputQuery = BuildSpecificQueryForParent(structureQuery, destinationType, isStub, maintainableMutableObject);
                            output.AddRange(retrievalEngine.RetrieveAsParents(inputQuery));
                        }
                        else
                        {
                            ICommonStructureQuery inputQuery = BuildStructureQuery(maintainableMutableObject, isStub, structureQuery.ReferencedDetail, null, StructureReferenceDetailEnumType.Children);
                            output.AddRange(retrievalEngine.RetrieveAsChildren(inputQuery));
                        }
                    }

                    if (destinationType.StructureType != specificStructureType)
                    {
                        input.Clear();
                        input.AddRange(output);
                        output.Clear();
                    }
                }

                specificObjects.AddRange(output);
            }
        }

        /// <summary>
        /// Build a common structure query from a maintainable artefact used for getting specific items
        /// </summary>
        /// <param name="maintainable"></param>
        /// <param name="isStub"></param>
        /// <param name="referencedDetail"></param>
        /// <param name="specificItems"></param>
        /// <param name="referenceDetail"></param>
        /// <returns></returns>
        private ICommonStructureQuery BuildStructureQuery(
            IMaintainableMutableObject maintainable, bool isStub, ComplexMaintainableQueryDetail referencedDetail,
            IList<IComplexIdentifiableReferenceObject> specificItems, StructureReferenceDetailEnumType referenceDetail)
        {
            // TODO replace bool isStub with StructureQueryDetail in order to support CompleteStub
            // TODO specific items
            // TODO to fix in SDMXRI-1920 support referencepartial
            // TODO low priority, fix builder hasSpecific* to work without the need to specify REST and have a default constructor
            // TODO test if the requestedDetail needs to be set

            var builder = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.REST, StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument))
                .SetStructureIdentification(maintainable)
                .SetRequestedDetail(isStub ? ComplexStructureQueryDetailEnumType.Stub : ComplexStructureQueryDetailEnumType.Full)
                .SetReferencedDetail(isStub ? ComplexMaintainableQueryDetail.GetFromEnum(ComplexMaintainableQueryDetailEnumType.Stub) : referencedDetail)
                .SetReferences(referenceDetail);

            if (specificItems != null)
            {
                builder.SetChildReferences(specificItems.ToArray());
            }
            return builder.Build();
        }

        private ICommonStructureQuery BuildSpecificQueryForParent(ICommonStructureQuery structureQuery, StructureAndRelation destinationType, bool isStub, IMaintainableMutableObject maintainable)
        {
            // special case  category scheme with specific items -> categorisation
            // we want to limit the number of parent categorisations returned based on the item selection
            IList<IComplexIdentifiableReferenceObject> specificItems = null;
            if (destinationType.StructureType.EnumType == SdmxStructureEnumType.Categorisation &&
                structureQuery.MaintainableTarget.EnumType == SdmxStructureEnumType.CategoryScheme)
            {
                specificItems = structureQuery.SpecificItems;
            }
            return BuildStructureQuery(maintainable, isStub, structureQuery.ReferencedDetail, specificItems, StructureReferenceDetailEnumType.Parents); ;
        }

        #endregion
    }
}
