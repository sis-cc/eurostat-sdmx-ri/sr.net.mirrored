// -----------------------------------------------------------------------
// <copyright file="AllResolver.cs" company="EUROSTAT">
//   Date Created : 2013-09-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines.Resolver
{
    using System;
    using System.Collections.Generic;
    using Estat.Sdmxsource.Extension.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    /// <summary>
    ///     The all resolver.
    /// </summary>
    internal class AllResolver : ResolverBase, IResolver
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AllResolver" /> class.
        /// </summary>
        /// <param name="crossReferenceManager">
        ///     The cross reference manager.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="crossReferenceManager" /> is null.
        /// </exception>
        public AllResolver(IAuthCrossReferenceMutableRetrievalManager crossReferenceManager)
            : base(crossReferenceManager)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Resolves the references of the specified mutable objects.
        /// </summary>
        /// <param name="mutableObjects">The mutable objects.</param>
        /// <param name="queryDetail">
        ///     The <see cref="StructureQueryDetail" /> which controls if the output will include details or not.
        /// </param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <param name="specificItems">The optional specific item filter.</param>
        public void ResolveReferences(IMutableObjects mutableObjects, StructureQueryDetail queryDetail, IList<IMaintainableRefObject> allowedDataflows, ISet<string> specificItems = null)
        {
            ResolveAll(mutableObjects, queryDetail, this.CrossReferenceManager, allowedDataflows);
        }

        public void ResolveReferences(IMutableObjects mutableObjects, ICommonStructureQuery structureQuery, IList<IMaintainableMutableObject> requestedArtefacts)
        {
            throw new NotImplementedException(); //TODO: SDMXRI-1839: implement AllResolver.ResolveReferences ?
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Resolve children of the specified <paramref name="mutableObjects" /> using the specified parameters
        /// </summary>
        /// <param name="mutableObjects">
        ///     The mutable objects.
        /// </param>
        /// <param name="queryDetail">
        ///     The <see cref="StructureQueryDetail" /> which controls if the output will include details or not.
        /// </param>
        /// <param name="crossReferenceMutableRetrievalManager">
        ///     The cross reference mutable retrieval engine
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        internal static void ResolveAll(
            IMutableObjects mutableObjects,
            StructureQueryDetail queryDetail, 
            IAuthCrossReferenceMutableRetrievalManager crossReferenceMutableRetrievalManager, 
            IList<IMaintainableRefObject> allowedDataflows)
        {
            Func<IMaintainableMutableObject, IList<IMaintainableMutableObject>> reference = maintainableMutableObject =>
            {
                var all = new List<IMaintainableMutableObject>();

                // add descendants
                all.AddRange(GetDescendantsReference(
                                            queryDetail, 
                                            crossReferenceMutableRetrievalManager, 
                                            allowedDataflows, 
                                            maintainableMutableObject));

                // add parents and siblings
                all.AddRange(GetParentsAndSiblingsReference(
                                    queryDetail, 
                                    crossReferenceMutableRetrievalManager, 
                                    allowedDataflows, 
                                    maintainableMutableObject));
                return all;
            };

            Resolve(mutableObjects, reference);
        }

        #endregion
    }
}