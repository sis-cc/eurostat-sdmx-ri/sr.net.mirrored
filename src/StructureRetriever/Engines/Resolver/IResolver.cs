// -----------------------------------------------------------------------
// <copyright file="IResolver.cs" company="EUROSTAT">
//   Date Created : 2013-09-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines.Resolver
{
    using System;
    using System.Collections.Generic;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    /// <summary>
    ///     The Resolver interface.
    /// </summary>
    public interface IResolver
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Resolves the references of the specified mutable objects.
        /// </summary>
        /// <param name="mutableObjects">
        ///     The mutable objects.
        /// </param>
        /// <param name="queryDetail">
        ///     The <see cref="StructureQueryDetail" /> which controls if the output will include details or not.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed dataflows.
        /// </param>
        /// <param name="specificItems">
        ///     The optional specific item filter.
        /// </param>
        [Obsolete("Since MSDB 7.0; use the other overload instead.")]
        void ResolveReferences(IMutableObjects mutableObjects, StructureQueryDetail queryDetail, IList<IMaintainableRefObject> allowedDataflows, ISet<string> specificItems = null);

        /// <summary>
        /// Fills the <paramref name="mutableObjects"/> with the requested artefacts.
        /// </summary>
        /// <param name="mutableObjects">The object to fill with data.</param>
        /// <param name="structureQuery">The query for the structure.</param>
        /// <param name="requestedArtefacts">The specific artefacts requested.</param>
        void ResolveReferences(IMutableObjects mutableObjects, ICommonStructureQuery structureQuery, IList<IMaintainableMutableObject> requestedArtefacts);

        #endregion
    }
}