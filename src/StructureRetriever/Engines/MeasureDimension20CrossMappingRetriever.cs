// -----------------------------------------------------------------------
// <copyright file="MeasureDimension20CrossMappingRetriever.cs" company="EUROSTAT">
//   Date Created : 2018-5-28
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Nsi.StructureRetriever.Engines
{
    using System.Linq;
    using Estat.Nsi.StructureRetriever.Model;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Util.Extensions;

    internal class MeasureDimension20CrossMappingRetriever : IAvailableDataRetrievalEngine
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(MeasureDimension20CrossMappingRetriever));

        public IMutableObjects GetPartialStructure(StructureRetrievalInfo info, IComponent component)
        {
            if (info.MappingSet == null)
            {
                _log.Warn("|-- Warning: Constraints not retrieved. Couldn't find a mapping set.");
                return null;
            }

            _log.InfoFormat("Starting dynamic constraint retriever for dataflow {0}  component {1}", info.Dataflow, component.Id);

            // set the codelist ref
            if (string.Equals(component.Id, info.MeasureComponent))
            {
                var crossDsd = info.DataStructure as ICrossSectionalDataStructureObject;
                IMutableObjects mutableObjects = new MutableObjectsImpl();
                if (crossDsd == null)
                {
                    throw new SdmxException("Measure dimension not mapped but DSD is not SDMX v2.0 cross sectional");
                }

                var codelistRef = crossDsd.GetCodelistForMeasureDimension(component.Id);
                // measure dimension codelist request if measure dimension is not mapped
                // get the entire codelist for measure dimension
                // This can be only XS since for SDMX v2.1 we must map the measure dimension
                var crossSectionalMeasureDimensionCodelist = SimpleCodeListRetrievalEngine.GetCodeList(info, codelistRef);

                mutableObjects.AddContentConstraint(CodeListHelper.ConvertToCubeRegion(component, crossSectionalMeasureDimensionCodelist));
                if (info.DynamicQuery.SpecificStructureReference != null && info.DynamicQuery.SpecificStructureReference.EnumType.IsOneOf(SdmxStructureEnumType.CodeList, SdmxStructureEnumType.Any))
                {
                    mutableObjects.AddCodelist(crossSectionalMeasureDimensionCodelist);
                }

                return mutableObjects;
            }

            return null;
        }
    }
}