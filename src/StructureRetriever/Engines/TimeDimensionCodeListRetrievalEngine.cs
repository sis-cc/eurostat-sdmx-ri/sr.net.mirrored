// -----------------------------------------------------------------------
// <copyright file="TimeDimensionCodeListRetrievalEngine.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Engines
{
    using System;
    using System.Data;
    using System.Data.Common;

    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sdmxsource.Extension.Extension;

    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Sri.MappingStoreRetrieval.Extensions;

    /// <summary>
    ///     Time Dimension start and optionally end code list retrieval engine
    /// </summary>
    internal class TimeDimensionCodeListRetrievalEngine : ICodeListRetrievalEngine
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Retrieve Codelist
        /// </summary>
        /// <param name="info">
        ///     The current StructureRetrieval state
        /// </param>
        /// <returns>
        ///     A <see cref="ICodelistMutableObject" />
        /// </returns>
        public ICodelistMutableObject GetCodeList(StructureRetrievalInfo info)
        {
            var tuple = RetrieveStartEnd(info);
            var minPeriod = tuple.Item1;
            var maxPeriod = tuple.Item2;

            return TimeDimensionCodeListHelper.BuildTimeCodelist(minPeriod, maxPeriod);
        }

        /// <summary>
        /// Retrieves the start end.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <returns>A tuple with min and max period.</returns>
        public static Tuple<ISdmxDate, ISdmxDate> RetrieveStartEnd(StructureRetrievalInfo info)
        {
            // TODO time range support
            ISdmxDate minPeriod = new SdmxDateCore(CustomCodelistConstants.TimePeriodMax);
            ISdmxDate maxPeriod = new SdmxDateCore(CustomCodelistConstants.TimePeriodMin);
            var frequencyMapping = info.FrequencyInfo != null ? info.FrequencyInfo.ComponentMapping : null;
            var timeTranscoder = info.TimeTranscoder;
            using (DbConnection ddbConnection = info.ConnectionBuilder.Build(info))
            using (DbCommand cmd = ddbConnection.CreateCommand(info.SqlQuery, info.Parameters))
            {
                cmd.CommandTimeout = 0;
                using (IDataReader reader = cmd.ExecuteReaderAndLog())
                {
                    while (reader.Read())
                    {
                        string frequency = frequencyMapping != null ? frequencyMapping.MapComponent(reader) : null;
                        string period = timeTranscoder.MapComponent(reader, frequency);
                        if (!string.IsNullOrEmpty(period))
                        {
                            ISdmxDate currentPeriod = new SdmxDateCore(period);
                            if (currentPeriod.StartsBefore(minPeriod))
                            {
                                minPeriod = currentPeriod;
                            }

                            if (currentPeriod.EndsAfter(maxPeriod))
                            {
                                maxPeriod = currentPeriod;
                            }
                        }
                    }
                }
            }

            return Tuple.Create(minPeriod, maxPeriod);
        }

        #endregion
    }
}