// -----------------------------------------------------------------------
// <copyright file="CachedAvailableConstraintRetriever.cs" company="EUROSTAT">
//   Date Created : 2018-5-28
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Nsi.StructureRetriever.Engines
{
    using System;
    using System.Globalization;
    using System.Linq;
    using Dapper;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    internal class CachedAvailableConstraintRetriever : IAvailableDataRetrievalEngine
    {
        private const string  _sqlQuery = "";
        public IMutableObjects GetPartialStructure(StructureRetrievalInfo info, IComponent component)
        {
            // TODO in case of no criteria we can use caching
            if (info.Criteria.Count == 0 || info.Criteria.All(x => x.Values.Count == 0))
            {
                ComponentMappingEntity mapping;
                if (info.MappingSet.ComponentMappings.TryGetValue(component.Id, out mapping))
                {
                    IContentConstraintMutableObject constraint = new ContentConstraintMutableCore();
                    if (mapping.HasTranscoding())
                    {
                        var database = new Database(info.Settings.ConnectionStringSettings);
                        var queryStatement = string.Format(CultureInfo.InvariantCulture,
                             "SELECT DISTINCT(SDMX_VALUE) FROM N_VALUE_MAPPING_RULES WHERE MAP_ID = {0} ", database.BuildParameterName("MAP_ID"));
                        var constraintsValues = database.Query<string>(queryStatement, new DynamicParameters(new { MAP_ID = mapping.EntityId }));
                        var keyValues = new KeyValuesMutableImpl() { Id = component.Id };
                        foreach (var item in constraintsValues)
                        {
                            keyValues.AddValue(item);
                        }
                        constraint.IncludedCubeRegion = new CubeRegionMutableCore();
                        constraint.IncludedCubeRegion.AddKeyValue(keyValues);
                    }
                    

                    if (constraint != null && constraint.IncludedCubeRegion != null && constraint.IncludedCubeRegion.KeyValues.Any(k => k.KeyValues.Count > 0))
                    {
                        var mutable = new MutableObjectsImpl();
                        mutable.AddContentConstraint(constraint);
                        return mutable;
                    }
                }
            }

            return null;
        }
    }
}