// -----------------------------------------------------------------------
// <copyright file="GlobalSuppressions.cs" company="EUROSTAT">
//   Date Created : 2016-03-24
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "It is OK. Queries are parameterised", Scope = "member", Target = "Estat.Nsi.StructureRetriever.Engines.TimeDimensionCodeListRetrievalEngine.#GetCodeList(Estat.Nsi.StructureRetriever.Model.StructureRetrievalInfo)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "It is OK. Queries are parameterised", Scope = "member", Target = "Estat.Nsi.StructureRetriever.Engines.PartialCodeListRetrievalEngine.#GetCodeList(Estat.Nsi.StructureRetriever.Model.StructureRetrievalInfo)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "It is OK. Queries are parameterised", Scope = "member", Target = "Estat.Nsi.StructureRetriever.Engines.CountCodeListRetrievalEngine.#ExecuteSql(Estat.Nsi.StructureRetriever.Model.StructureRetrievalInfo)")]
