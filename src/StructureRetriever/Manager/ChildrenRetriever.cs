// -----------------------------------------------------------------------
// <copyright file="ChildrenRetriever.cs" company="EUROSTAT">
//   Date Created : 2023-02-01
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.StructureRetriever.Manager
{
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    internal class ChildrenRetriever : ChildrenRetrieverBase
    {
        internal ChildrenRetriever(IRetrievalEngineContainer retrievalEngineContainer) 
            : base(retrievalEngineContainer)
        {
        }

        protected override ICommonStructureQuery BuildQuery(ICommonStructureQuery structureQuery, SdmxStructureType childType)
        {
            return structureQuery;
        }

        protected override IRetrievalEngine<IMaintainableMutableObject> GetRetrievalEngine(SdmxStructureType childType)
        {
            return RetrievalEngineContainer.GetEngine(childType);
        }

        protected override bool ShouldRetrieveChildren(ICommonStructureQuery structureQuery, SdmxStructureType childType)
        {
            return true;
        }
    }
}
