// -----------------------------------------------------------------------
// <copyright file="AuthMutableStructureSearchManager.cs" company="EUROSTAT">
//   Date Created : 2013-06-17
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Manager
{
    using System;
    using System.Configuration;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    using Sdmxsource.Extension.Manager;

    using Sri.MappingStoreRetrieval.Factory;

    /// <summary>
    ///     The mutable structure search manager implementation.
    /// </summary>
    [Obsolete("MSDB 7.0: After migrating to msdb 7")]
    public class AuthMutableStructureSearchManager : AuthMutableStructureSearchManagerBase
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthMutableStructureSearchManager" /> class.
        /// </summary>
        /// <param name="mutableRetrievalManagerFactory">
        ///     The mutable retrieval manager factory.
        /// </param>
        /// <param name="crossReferenceManager">
        ///     The cross reference manager.
        /// </param>
        /// <param name="connectionStringSettings">
        ///     The connection string settings.
        /// </param>
        public AuthMutableStructureSearchManager(
            IAuthAdvancedSdmxMutableObjectRetrievalManager mutableRetrievalManagerFactory, 
            IAuthCrossRetrievalManagerFactory crossReferenceManager, 
            ConnectionStringSettings connectionStringSettings)
            : base(mutableRetrievalManagerFactory, crossReferenceManager, connectionStringSettings)
        {
            throw new InvalidOperationException("AuthMutableStructureSearchManager class is obsolete");
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthMutableStructureSearchManager" /> class.
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The connection string settings.
        /// </param>
        public AuthMutableStructureSearchManager(ConnectionStringSettings connectionStringSettings)
            : base(connectionStringSettings)
        {
            throw new InvalidOperationException("AuthMutableStructureSearchManager class is obsolete");
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthMutableStructureSearchManager" /> class.
        /// </summary>
        /// <param name="fullRetrievalManager">
        ///     The full retrieval manager. Used for <see cref="StructureQueryDetailEnumType.Full" /> or
        ///     <see cref="StructureQueryDetailEnumType.ReferencedStubs" />
        /// </param>
        /// <param name="crossReferenceManager">
        ///     The cross reference manager. Set it to be able to retrieve cross references. Used for
        ///     <see cref="StructureQueryDetailEnumType.Full" />
        ///     .
        /// </param>
        public AuthMutableStructureSearchManager(
            IAuthSdmxMutableObjectRetrievalManager fullRetrievalManager, 
            IAuthCrossRetrievalManagerFactory crossReferenceManager)
            : base(fullRetrievalManager, crossReferenceManager)
        {
            throw new InvalidOperationException("AuthMutableStructureSearchManager class is obsolete");
        }

        #endregion
    }
}