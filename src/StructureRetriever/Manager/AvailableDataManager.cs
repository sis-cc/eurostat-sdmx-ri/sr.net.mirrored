// -----------------------------------------------------------------------
// <copyright file="AvailableDataManager.cs" company="EUROSTAT">
//   Date Created : 2018-04-18
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Nsi.StructureRetriever.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Estat.Nsi.StructureRetriever.Builders;
    using Estat.Nsi.StructureRetriever.Engines;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sdmxsource.Extension.Manager;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    /// Class AvailableDataManager.
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Manager.IAvailableDataManager" />
    /// <inheritdoc />
    public class AvailableDataManager : IAvailableDataManager
    {
        /// <summary>
        /// The log
        /// </summary>
        private static ILog _log = LogManager.GetLogger(typeof(AvailableDataManager));

        /// <summary>
        /// The reference period retrieval engine
        /// </summary>
        private readonly ReferencePeriodRetrievalEngine _referencePeriodRetrievalEngine = new ReferencePeriodRetrievalEngine();

        /// <summary>
        /// The available data retrieval engine
        /// </summary>
        private readonly IAvailableDataRetrievalEngine[] _availableDataRetrievalEngines = { new MeasureDimension20CrossMappingRetriever(), new CachedAvailableConstraintRetriever(), new DynamicConstraintRetriever() };

        /// <summary>
        /// SQL Code builder for getting the COUNT
        /// </summary>
        private readonly ISqlBuilder _countSqlBuilder = new CountSqlBuilder();

        /// <summary>
        /// The information builder
        /// </summary>
        private readonly StructureRetrievalInfoBuilder _infoBuilder = new StructureRetrievalInfoBuilder();

        /// <summary>
        /// The settings
        /// </summary>
        private readonly StructureRetrieverSettings _settings;

        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableDataManager" /> class.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <exception cref="ArgumentNullException">settings is <c>null</c></exception>
        public AvailableDataManager(StructureRetrieverSettings settings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            this._settings = settings;
        }

        /// <inheritdoc />
        public IMutableObjects Retrieve(IAvailableConstraintQuery query, IMutableObjects objects = null, bool multipleReferences = false)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }
            StructureRetrievalInfo info = BuildInfo(query);

            var metrics = GetCount(info, false);

            objects = objects ?? new MutableObjectsImpl();
            IContentConstraintMutableObject constraintMutableObject = new ContentConstraintMutableCore();

            string constraintId = "CC";
            if (multipleReferences)
            {
                constraintId += "_" + query.Dataflow.Id;
            }
            constraintMutableObject.Id = constraintId;
            constraintMutableObject.AgencyId = "SDMX";
            constraintMutableObject.Version = "1.0";
            constraintMutableObject.IsDefiningActualDataPresent = true;
            constraintMutableObject.AddName("en", "Autogenerated content constraint");

            constraintMutableObject.ConstraintAttachment = new ContentConstraintAttachmentMutableCore();
            constraintMutableObject.ConstraintAttachment.AddStructureReference(query.Dataflow.AsReference);
            constraintMutableObject.IncludedCubeRegion = new CubeRegionMutableCore();
            var metricAnnotation = new AnnotationMutableCore();
            metricAnnotation.Type = "sdmx_metrics";
            metricAnnotation.Id = "obs_count";
            metricAnnotation.Title = metrics.ToString(CultureInfo.InvariantCulture);
            constraintMutableObject.AddAnnotation(metricAnnotation);

            QueryComponent(query, info, (MutableObjectsImpl)objects, constraintMutableObject);

            objects.AddContentConstraint(constraintMutableObject);

            if (query.SpecificStructureReference != null)
            {
                switch (query.SpecificStructureReference.EnumType)
                {
                    case SdmxStructureEnumType.Any:
                        objects.AddDataflow(query.Dataflow.MutableInstance);
                        objects.AddDataStructure(query.DataStructure.MutableInstance);
                        objects.ConceptSchemes.UnionWith(GetConceptIdentities(query, info));
                        break;

                    case SdmxStructureEnumType.Dataflow:
                        objects.AddDataflow(query.Dataflow.MutableInstance);
                        break;

                    case SdmxStructureEnumType.Dsd:
                        objects.AddDataStructure(query.DataStructure.MutableInstance);
                        break;

                    case SdmxStructureEnumType.ConceptScheme:
                        objects.ConceptSchemes.UnionWith(GetConceptIdentities(query, info));
                        break;
                }
            }

            return objects;
        }

        /// <summary>
        /// Gets the concept identities.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="info">The information.</param>
        /// <returns>The <see cref="IEnumerable{IConceptSchemeMutableObject}" />.</returns>
        private IEnumerable<IConceptSchemeMutableObject> GetConceptIdentities(
            IAvailableConstraintQuery query,
            StructureRetrievalInfo info)
        {
            ILookup<IMaintainableRefObject, ICrossReference> conceptReferences =
                query.Components.Select(c => c.ConceptRef).ToLookup(c => c.MaintainableReference);
            foreach (var conceptReference in conceptReferences)
            {
                var builder = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.REST, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                    .SetRequestedDetail(ComplexStructureQueryDetailEnumType.Full)
                    .SetReferencedDetail(ComplexMaintainableQueryDetailEnumType.Full)
                    .SetMaintainableTarget(SdmxStructureEnumType.ConceptScheme);

                if (conceptReference.Key.HasVersion())
                {
                    var versionRequests = conceptReference.Key.Version.Split(',').Select(v => new VersionRequestCore(v)).Cast<IVersionRequest>().ToArray();
                    builder.SetVersionRequests(versionRequests);
                }
                if (conceptReference.Key.HasMaintainableId())
                {
                    builder.SetMaintainableIds(conceptReference.Key.MaintainableId.Split(','));
                }
                if (conceptReference.Key.HasAgencyId())
                {
                    builder.SetAgencyIds(conceptReference.Key.AgencyId.Split(','));
                }
                builder.Build();
                ICommonStructureQuery commonStructureQuery = builder.Build();

                var sdmxObjects = info.RetrievalManager.GetMaintainables(commonStructureQuery);
                IConceptSchemeMutableObject conceptScheme = sdmxObjects.GetAllMaintainables().FirstOrDefault().MutableInstance as IConceptSchemeMutableObject;

                var conceptIds = new HashSet<string>(conceptReference.Select(i => i.ChildReference.Id), StringComparer.Ordinal);
                int count = conceptScheme.Items.Count;
                for (int i = count - 1; i >= 0; i--)
                {
                    var concept = conceptScheme.Items[i];
                    if (!conceptIds.Contains(concept.Id))
                    {
                        conceptScheme.RemoveItem(concept.Id);
                    }
                }
                conceptScheme.IsPartial = conceptScheme.Items.Count < count;
                yield return conceptScheme;
            }
        }

        /// <summary>
        /// Queries the component.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="info">The information.</param>
        /// <param name="objects">The objects.</param>
        /// <param name="constraintMutableObject">The constraint mutable object.</param>
        /// <exception cref="SdmxNotImplementedException">mode=available is not implemented</exception>
        private void QueryComponent(IAvailableConstraintQuery query, StructureRetrievalInfo info, MutableObjectsImpl objects, IContentConstraintMutableObject constraintMutableObject)
        {
            foreach (var component in query.Components)
            {
                if (component.Id.Equals(DimensionObject.TimeDimensionFixedId))
                {
                    var key = new KeyValuesMutableImpl() { Id = DimensionObject.TimeDimensionFixedId };
                    var referencePeriod = _referencePeriodRetrievalEngine.GetReferencePeriod(info);
                    var startPeriod = referencePeriod.StartTime;
                    var endPeriod = referencePeriod.EndTime;
                    if (startPeriod.HasValue || endPeriod.HasValue)
                    {
                        key.TimeRange = new TimeRangeMutableCore();
                        if (startPeriod.HasValue && !endPeriod.HasValue)
                        {
                            key.TimeRange.StartDate = new SdmxDateCore(startPeriod);
                            key.TimeRange.IsStartInclusive = true;
                        }
                        else if (!startPeriod.HasValue && endPeriod.HasValue)
                        {
                            key.TimeRange.EndDate = new SdmxDateCore(endPeriod);
                            key.TimeRange.IsEndInclusive = true;
                        }
                        else
                        {
                            key.TimeRange.StartDate = new SdmxDateCore(startPeriod);
                            key.TimeRange.IsStartInclusive = true;
                            key.TimeRange.EndDate = new SdmxDateCore(endPeriod);
                            key.TimeRange.IsEndInclusive = true;
                            key.TimeRange.IsRange = true;
                        }
                    }
                    else
                    {
                        key.AddValue(DimensionObject.TimeDimensionFixedId);
                    }
                    constraintMutableObject.IncludedCubeRegion.AddKeyValue(key);
                }
                else
                {
                    var mutable = this._availableDataRetrievalEngines.Select(x => new AvailableConstraintReferencesRetriever(x).GetPartialStructure(info, component)).FirstOrDefault(x => x != null);
                    if (mutable != null)
                    {
                        // two components may have the same codelist but using different subset
                        // the standard doesn't say anything on this matter so we merge the different subsets if this is the case
                        if (component.HasCodedRepresentation())
                        {
                            // Normally it should at one codelist
                            foreach (ICodelistMutableObject codelist in mutable.Codelists)
                            {
                                ICodelistMutableObject existingCodelist = Find(objects, codelist);
                                if (existingCodelist == null)
                                {
                                    objects.AddCodelist(codelist);
                                }
                                else
                                {
                                    MergeItemsToExisting(codelist, existingCodelist);
                                }
                            }
                        }

                        objects.AddIdentifiables(mutable.ConceptSchemes);
                        foreach (var result in mutable.ContentConstraints.SelectMany(x => x.IncludedCubeRegion.KeyValues))
                        {
                            constraintMutableObject.IncludedCubeRegion.AddKeyValue(result);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the maximum possible observation count for the specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="includeHistory"></param>
        /// <returns>The number of observations</returns>
        /// <exception cref="ArgumentNullException">query</exception>
        public long GetCount(IAvailableConstraintQuery query,bool includeHistory)
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query));
            }
            StructureRetrievalInfo info = BuildInfo(query);
            return GetCount(info, includeHistory);
        }

        /// <summary>
        /// Gets the maximum possible observation count for the specified query.
        /// </summary>
        /// <param name="info">The query.</param>
        /// <param name="includeHistory"></param>
        /// <returns>The number of observations</returns>
        private long GetCount(StructureRetrievalInfo info, bool includeHistory)
        {
            IObservationCountEngine countCodeListRetrieval = new CountCodeListRetrievalEngine();

            // TODO get rid of info.SqlQuery. It is still used in old SR code needed for NSI Client
            info.SqlQuery = _countSqlBuilder.GenerateSql(info, includeHistory);

            _log.Info("|-- SQL for dissemination database generated:\n" + info.SqlQuery);
            return countCodeListRetrieval.GetCount(info);
        }

        /// <summary>
        /// Merges the items from <paramref name="codelist"/> to the <paramref name="existingCodelist"/>.
        /// </summary>
        /// <param name="codelist">The codelist.</param>
        /// <param name="existingCodelist">The existing codelist.</param>
        private static void MergeItemsToExisting(ICodelistMutableObject codelist, ICodelistMutableObject existingCodelist)
        {
            var existingCodes = new HashSet<string>(StringComparer.Ordinal);
            existingCodes.UnionWith(existingCodelist.Items.Select(x => x.Id));
            foreach (var newCode in codelist.Items)
            {
                if (!existingCodes.Contains(newCode.Id))
                {
                    existingCodelist.AddItem(newCode);
                }
            }
        }

        /// <summary>
        /// Finds the specified artefact
        /// </summary>
        /// <param name="objects">The objects.</param>
        /// <param name="representation">The representation.</param>
        /// <returns>The <see cref="ICodelistMutableObject" />.</returns>
        private static ICodelistMutableObject Find(MutableObjectsImpl objects, IMaintainableMutableObject representation)
        {
            return objects.Codelists.FirstOrDefault(x => x.Id.Equals(representation.Id) && x.AgencyId.Equals(representation.AgencyId) && x.Version.Equals(representation.Version));
        }

        /// <summary>
        /// Builds the information.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>StructureRetrievalInfo.</returns>
        private StructureRetrievalInfo BuildInfo(IAvailableConstraintQuery query)
        {
            return _infoBuilder.Build(query, _settings);
        }
    }
}