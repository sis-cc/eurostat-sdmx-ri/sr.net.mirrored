// -----------------------------------------------------------------------
// <copyright file="RetrievalUtils.cs" company="EUROSTAT">
//   Date Created : 2023-02-01
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.StructureRetriever.Manager
{
    using System.Linq;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

    internal static class RetrievalUtils
    {
        /// <summary>
        /// Helper method for creating a query to be used for retrieving a relative of the artefact targeted by the provided structureQuery, 
        /// with the relation taken from referenceDetail
        /// </summary>
        /// <param name="structureQuery">query that retrieves the original artefact</param>
        /// <param name="relative">the artefact that was retrieved by the structureQuery</param>
        /// <param name="referenceDetail">the relation of the artefact that was retrieved by the structureQuery with the one that we wish to retrieve</param>
        /// <returns>the relative of the artefact that was retrieved by the structureQuery that we wish to retrieve</returns>
        internal static ICommonStructureQuery GetCommonStructureQueryForRetrievingRelated(
            ICommonStructureQuery structureQuery, IMaintainableMutableObject relative, StructureReferenceDetailEnumType referenceDetail)
        {
            IVersionRequest[] versionRequests = relative.Version?.Split(',').Select(v => new VersionRequestCore(v)).ToArray();
            string[] maintainableIds = relative.Id?.Split(',');
            string[] agencyIds = relative.AgencyId?.Split(',');

            return CommonStructureQueryCore.Builder
                .NewQuery(structureQuery.QueryType, structureQuery.StructureOutputFormat)
                .SetRequestedDetail(structureQuery.RequestedDetail)
                .SetReferencedDetail(structureQuery.ReferencedDetail)
                .SetReferences(StructureReferenceDetail.GetFromEnum(referenceDetail))
                .SetMaintainableTarget(relative.StructureType)
                .SetVersionRequests(versionRequests)
                .SetMaintainableIds(maintainableIds)
                .SetAgencyIds(agencyIds)
                .Build();
        }

        /// <summary>
        /// Check if a query targets on one and only one artefact
        /// </summary>
        /// <param name="structureQuery">the query to check</param>
        /// <returns>true if the query targets exactly one artefact. False if it targets many or no artefacts</returns>
        internal static bool IsOnlyOneArtefactSpecified(ICommonStructureQuery structureQuery)
        {
            bool result = true;

            if (!structureQuery.HasSpecificAgencyId)
            {
                return false;
            }
            if (structureQuery.AgencyIds.Count > 1)
            {
                return false;
            }
            if (!structureQuery.HasSpecificMaintainableId)
            {
                return false;
            }
            if (structureQuery.MaintainableIds.Count > 1)
            {
                return false;
            }
            if (structureQuery.VersionRequests.Count > 1)
            {
                return false;
            }
            if (structureQuery.VersionRequests.Any(v => !v.SpecifiesVersion))
            {
                return false;
            }
            if (structureQuery.VersionRequests.Any(v => v.WildCard != null))
            {
                return false;
            }

            return result;
        }
    }
}
