// -----------------------------------------------------------------------
// <copyright file="ChildrenRetrieverBase.cs" company="EUROSTAT">
//   Date Created : 2023-02-01
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------


namespace Estat.Sri.StructureRetriever.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.StructureRetriever.Cache;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using Org.Sdmxsource.Util.Extensions;

    internal abstract class ChildrenRetrieverBase : IChildrenRetriever
    {
        protected readonly IRetrievalEngineContainer RetrievalEngineContainer;

        protected ChildrenRetrieverBase(IRetrievalEngineContainer retrievalEngineContainer)
        {
            this.RetrievalEngineContainer = retrievalEngineContainer;
        }

        protected abstract IRetrievalEngine<IMaintainableMutableObject> GetRetrievalEngine(SdmxStructureType childType);

        /// <summary>
        /// Build a CommonStructureQuery that will be used by the engine returned by getRetrievalEngine(SDMX_STRUCTURE_TYPE) method to retrieve children.
        /// The object built may be a subclass of CommonStructureQuery and have additional properties populated that can only be used by the relevant engine.
        /// e.g. an instance of a subclass of CommonStructureQuery holding information related to referencepartial functionality may be built here 
        /// and used by an instance of a partial engine returned by getRetrievalEngine
        /// </summary>
        /// <param name="structureQuery">the original query that returned the artefact whose children we wish to retrieve</param>
        /// <param name="childType">the type of the children we wish to retrieve</param>
        /// <returns>the query to be used for retrieving the children</returns>
        protected abstract ICommonStructureQuery BuildQuery(ICommonStructureQuery structureQuery, SdmxStructureType childType);

        /// <summary>
        /// Checks if the retriever should return children for the provided structure type
        /// </summary>
        /// <param name="structureQuery">the original query that returned the artefact whose children we wish to retrieve</param>
        /// <param name="childType">the type of the children we wish to retrieve</param>
        /// <returns>true if this retriever should return children for the provided type</returns>
        protected abstract bool ShouldRetrieveChildren(ICommonStructureQuery structureQuery, SdmxStructureType childType);

        public ICollection<IMaintainableMutableObject> RetrieveChildren(ICommonStructureQuery structureQuery, HashSet<StructureQueryCacheKey> cache)
        {
            IList<IMaintainableMutableObject> allChildren = new List<IMaintainableMutableObject>();
            StructureQueryCacheKey childrenQueryKey = CacheUtils.GetKeyFromQuery(structureQuery);
            foreach (SdmxStructureType childType in SdmxMaintainableReferenceTree.GetChildReferences(structureQuery.MaintainableTarget))
            {
                IRetrievalEngine<IMaintainableMutableObject> childEngine = GetRetrievalEngine(childType);
                if (childEngine == null)
                {
                    continue;// not supported
                }
                if (!ShouldRetrieveChildren(structureQuery, childType))
                {
                    continue;
                }

                ICommonStructureQuery useQuery = BuildQuery(structureQuery, childType);
                
                IEnumerable<IMaintainableMutableObject> childrenWithCurrentChildType = childEngine.RetrieveAsChildren(useQuery,cache);
                
                allChildren.AddAll(childrenWithCurrentChildType);
            }
            cache.Add(childrenQueryKey);
            return allChildren;
        }

        public ICollection<IMaintainableMutableObject> RetrieveDescendants(ICommonStructureQuery structureQuery,HashSet<StructureQueryCacheKey> cache)
        {
            StructureQueryCacheKey childrenQueryKey = CacheUtils.GetKeyFromQuery(structureQuery);
            if (cache.Contains(childrenQueryKey))
            {
                return new List<IMaintainableMutableObject>();
            }
            ICollection<IMaintainableMutableObject> children = RetrieveChildren(structureQuery,cache);
            cache.Add(childrenQueryKey);

            var descendants = new List<IMaintainableMutableObject>(children);
            foreach (IIdentifiableMutableObject child in children)
            {
                if (child is IMaintainableMutableObject maintainableChild)
                {
                   
                    ICommonStructureQuery query = RetrievalUtils.GetCommonStructureQueryForRetrievingRelated(
                        structureQuery, maintainableChild, StructureReferenceDetailEnumType.Descendants);
                    descendants.AddAll(RetrieveDescendants(query, cache));
                }
            }
            return descendants;
        }
    }
}
