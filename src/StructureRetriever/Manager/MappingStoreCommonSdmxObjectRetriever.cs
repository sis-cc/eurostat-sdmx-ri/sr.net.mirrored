// -----------------------------------------------------------------------
// <copyright file="MappingStoreCommonSdmxObjectRetriever.cs" company="EUROSTAT">
//   Date Created : 2022-05-25
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Nsi.StructureRetriever.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Nsi.StructureRetriever.Engines.Resolver;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.StructureRetriever.Cache;
    using Estat.Sri.StructureRetriever.Manager;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Sdmx;
    using Org.Sdmxsource.Util.Extensions;

    public class MappingStoreCommonSdmxObjectRetriever : ICommonSdmxObjectRetrievalManager
    {
        private readonly IRetrievalEngineContainer _retrievalEngineContainer;
        private readonly SpecificResolver _specificObjectsResolver;
        private readonly StructureRetrieverSettings _settings;

        public MappingStoreCommonSdmxObjectRetriever(IRetrievalEngineContainer retrievalEngineContainer)
        {
            if (retrievalEngineContainer == null)
            {
                throw new ArgumentNullException(nameof(retrievalEngineContainer));
            }
            this._retrievalEngineContainer = retrievalEngineContainer;
            this._specificObjectsResolver = new SpecificResolver(retrievalEngineContainer);
            this._settings = new StructureRetrieverSettings();
        }

        public MappingStoreCommonSdmxObjectRetriever(Database mappingStore, StructureRetrieverSettings settings)
            : this(new RetrievalEngineContainer(mappingStore))
        {
            this._settings = settings ?? new StructureRetrieverSettings();
        }

        public ISdmxObjects GetMaintainables(ICommonStructureQuery structureQuery)
        {
            if (structureQuery == null)
            {
                throw new ArgumentNullException(nameof(structureQuery));
            }

            bool isPartial = structureQuery.ReferencedDetail.EnumType == ComplexMaintainableQueryDetailEnumType.ReferencePartial;
            if (isPartial && !(
                structureQuery.MaintainableTarget.EnumType == SdmxStructureEnumType.Dataflow ||
                structureQuery.MaintainableTarget.EnumType == SdmxStructureEnumType.Dsd))
            {
                throw new SdmxNotImplementedException("referencepartial option for detail is only available for dataflows and datastructures.");
            }
            if (isPartial && !RetrievalUtils.IsOnlyOneArtefactSpecified(structureQuery))
            {
                throw new SdmxNotImplementedException("referencepartial optionfor detail is only available for a single dataflow or datastructure artefact.");
            }

            IRetrievalEngine<IMaintainableMutableObject> engine = _retrievalEngineContainer.GetEngine(structureQuery.MaintainableTarget);
            if (engine == null)
            {
                // not supported
                return new SdmxObjectsImpl();
            }

            // get the 1st level of artefacts
            IList<IMaintainableMutableObject> requestedArtefacts = engine.Retrieve(structureQuery).ToList();

            IMutableObjects mutableObjects = new MutableObjectsImpl();
            mutableObjects.AddIdentifiables(requestedArtefacts);

            // check if we need something else
            IChildrenRetriever childrenRetriever;
            switch (structureQuery.References.EnumType)
            {
                case StructureReferenceDetailEnumType.None:
//                    ApplyCrossSectionalAnnotations(structureQuery, mutableObjects);
//                    return mutableObjects.ImmutableObjects;
                    break;
                case StructureReferenceDetailEnumType.Parents:
                    mutableObjects.AddIdentifiables(RetrieveParents(structureQuery));
                    break;
                    //return mutableObjects.ImmutableObjects;
                case StructureReferenceDetailEnumType.ParentsSiblings:
                    ICollection<IIdentifiableMutableObject> artefactParents = RetrieveParents(structureQuery);
                    mutableObjects.AddIdentifiables(artefactParents);
                    if (isPartial)
                    {
                        childrenRetriever = new PartialChildrenRetriever(_retrievalEngineContainer, mutableObjects);
                    }
                    else
                    {
                        childrenRetriever = new ChildrenRetriever(_retrievalEngineContainer);
                    }
                    var cache = new HashSet<StructureQueryCacheKey>();
                    foreach (IIdentifiableMutableObject parent in artefactParents)
                    {
                        if (parent is IMaintainableMutableObject maintainableParent)
                        {
                            ICommonStructureQuery childrenQuery = RetrievalUtils.GetCommonStructureQueryForRetrievingRelated(
                                structureQuery, maintainableParent, StructureReferenceDetailEnumType.None);
                            mutableObjects.AddIdentifiables(childrenRetriever.RetrieveDescendants(childrenQuery, cache));
                        }
                    }
                    break;
                case StructureReferenceDetailEnumType.Children:
                    // TODO SDMXRI-1839 Optimise so not to query for duplicates (Although duplicate mutable beans will eventually be removed when put in a
                    // set of immutable beans so result will be correct, carrying duplicates in mutable beans is slower.)
                    childrenRetriever = new ChildrenRetriever(_retrievalEngineContainer);
                    mutableObjects.AddIdentifiables(childrenRetriever.RetrieveChildren(structureQuery, new HashSet<StructureQueryCacheKey>()));
                    break;
                case StructureReferenceDetailEnumType.Descendants:
                    if (isPartial)
                    {
                        // the parents are needed to see which items are used or not,
                        // but since the reference detail choice here is descendants, the parents should not be included in the response.
                        // So a new IMutableObjects is created to feed the method of retrieving partial children.
                        IMutableObjects parents = new MutableObjectsImpl(mutableObjects.AllMaintainables);
                        artefactParents = RetrieveParents(structureQuery);
                        parents.AddIdentifiables(artefactParents);
                        childrenRetriever = new PartialChildrenRetriever(_retrievalEngineContainer, parents);
                    }
                    else
                    {
                        childrenRetriever = new ChildrenRetriever(_retrievalEngineContainer);
                    }
                    mutableObjects.AddIdentifiables(childrenRetriever.RetrieveDescendants(structureQuery, new HashSet<StructureQueryCacheKey>()));
                    break;
                case StructureReferenceDetailEnumType.All:
                    artefactParents = RetrieveParents(structureQuery);
                    mutableObjects.AddIdentifiables(artefactParents);
                    if (isPartial)
                    {
                        childrenRetriever = new PartialChildrenRetriever(_retrievalEngineContainer, mutableObjects);
                    }
                    else
                    {
                        childrenRetriever = new ChildrenRetriever(_retrievalEngineContainer);
                    }
                    foreach (IIdentifiableMutableObject parent in artefactParents)
                    {
                        if (parent is IMaintainableMutableObject maintainableParent)
                        {
                            ICommonStructureQuery childrenQuery = RetrievalUtils.GetCommonStructureQueryForRetrievingRelated(
                                structureQuery, maintainableParent, StructureReferenceDetailEnumType.Children);
                            mutableObjects.AddIdentifiables(childrenRetriever.RetrieveChildren(childrenQuery, new HashSet<StructureQueryCacheKey>()));
                        }
                    }
                    mutableObjects.AddIdentifiables(childrenRetriever.RetrieveDescendants(structureQuery, new HashSet<StructureQueryCacheKey>()));
                    break;
                case StructureReferenceDetailEnumType.Specific:
                    this._specificObjectsResolver.ResolveReferences(mutableObjects, structureQuery, requestedArtefacts);
                    break;
                case StructureReferenceDetailEnumType.Ancestors:
                    AddAncestors(mutableObjects, structureQuery);
                    break;
            }
            ApplyCrossSectionalAnnotations(structureQuery, mutableObjects);

            return mutableObjects.ImmutableObjects;
        }

        private void ApplyCrossSectionalAnnotations(ICommonStructureQuery structureQuery, IMutableObjects mutableObjects)
        {
            // calling programmatically we don't want to change anything
            if (structureQuery.QueryType == CommonStructureQueryType.Other)
            {
                return;
            }

            if (ShouldTransformDataStructures(structureQuery))
            {
                var dsds = mutableObjects.DataStructures.TransformCrossSectional().ToArray();
                mutableObjects.DataStructures.Clear();
                mutableObjects.AddIdentifiables(dsds);
            }
            else if (ShouldNormalizeDataStructures(structureQuery))
            {
                mutableObjects.DataStructures.NormalizeSdmxv20DataStructures();
            }
        }

        /// <summary>
        /// Check if we need to transform any SDMX 2.0 Cross DSD to be visible in 2.1+ formats
        /// </summary>
        /// <param name="structureQuery"></param>
        /// <returns></returns>
        private bool ShouldTransformDataStructures(ICommonStructureQuery structureQuery)
        {
            if (structureQuery.StructureOutputFormat == null)
            {
                return _settings.AllowSdmx21CrossSectional;
            }
            
            switch(structureQuery.StructureOutputFormat.OutputVersion.EnumType)
            {
                case SdmxSchemaEnumType.VersionOne:
                case SdmxSchemaEnumType.VersionTwo:
                    return false;
                case SdmxSchemaEnumType.Json:
                case SdmxSchemaEnumType.JsonV20: // TODO this format JSON 2.0.0 supports multiple measures so needs different transformation
                    return true;
                default: // TODO 3.0.0 format supports multiple measures so needs different transformation
                    return _settings.AllowSdmx21CrossSectional;
            }
        }

        private static bool ShouldNormalizeDataStructures(ICommonStructureQuery structureQuery)
        {
            //we need to normalize for REST requests if XML 2.1 or later is requested
            //or for SOAP requests that are not v2.0
            return structureQuery.StructureOutputFormat != null && structureQuery.StructureOutputFormat.OutputVersion != SdmxSchemaEnumType.VersionTwo;
        }

        private void AddAncestors(IMutableObjects mutableObjects, ICommonStructureQuery structureQuery)
        {
            var artefactParents = RetrieveParents(structureQuery);
            if (artefactParents != null || artefactParents.Count > 0)
            {
                foreach (IMaintainableMutableObject parent in artefactParents)
                {
                    if (mutableObjects.GetMaintainables(parent.StructureType).FirstOrDefault(
                        x => x.Id == parent.Id && x.AgencyId == parent.AgencyId && x.Version == parent.Version) == null)
                    {
                        AddAncestors(mutableObjects, RetrievalUtils.GetCommonStructureQueryForRetrievingRelated(
                            structureQuery, parent, StructureReferenceDetailEnumType.Ancestors));
                    }
                }
            }
            mutableObjects.AddIdentifiables(artefactParents);
        }

        private IList<IIdentifiableMutableObject> RetrieveParents(ICommonStructureQuery structureQuery)
        {
            IList<IIdentifiableMutableObject> allParents = new List<IIdentifiableMutableObject>();

            foreach (SdmxStructureType parentType in SdmxMaintainableReferenceTree.GetParentReferences(structureQuery.MaintainableTarget))
            {
                IRetrievalEngine<IMaintainableMutableObject> parentEngine = _retrievalEngineContainer.GetEngine(parentType);
                if (parentEngine == null)
                {
                    continue; // not supported
                }
                IList<IMaintainableMutableObject> parentsWithCurrentParentType = parentEngine.RetrieveAsParents(structureQuery).ToList();
                allParents.AddAll(parentsWithCurrentParentType);
            }
            return allParents;
        }
    }
}
