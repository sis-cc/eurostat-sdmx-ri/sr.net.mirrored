// -----------------------------------------------------------------------
// <copyright file="AuthStructureRetrieverAvailableDataV20.cs" company="EUROSTAT">
//   Date Created : 2010-10-21
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;

    using Builders;

    using Estat.Sdmxsource.Extension.Extension;

    using log4net;

    using Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Util.Extensions;

    using Sdmxsource.Extension.Manager;

    using Sri.CustomRequests.Model;

    /// <summary>
    ///     This is an implementation of <see cref="IAuthMutableStructureSearchManager" /> interface that can retrieve
    ///     available data from DDB and dataflows with complete mapping set.
    /// </summary>
    /// <example>
    ///     Code example for C#
    ///     <code source="ReUsingExamples\StructureRetriever\ReUsingStructureRetriever.cs" lang="cs">
    /// </code>
    /// </example>
    /// <remarks>
    ///     It's main job is to retrieve structural metadata from Mapping Store. It can be used with any v3.0 or higher
    ///     "Mapping Store" complying with the database design specified there. This implementation supports the following
    ///     special case in order to retrieve a subset of codes for a dimension that can be constrained by the values of other
    ///     dimensions: If the <c>QueryStructureRequest</c> contains a <c>CodelistRef</c> and <c>DataflowRef</c> with
    ///     constrains with one <c>Member</c> without <c>MemberValue</c> and optionally any number <c>Member</c> with
    ///     <c>MemberValue</c> then the this implementation will retrieve the subset of the requested codelist that is used by
    ///     the dimension referenced in the member without member value, used by the specified dataflow and constrained by the
    ///     dimension values defined with Member and MemberValues.
    /// </remarks>
    public class AuthStructureRetrieverAvailableDataV20 : AuthCategorisationV20MutableStructureSearchManager
    {
        #region Constants and Fields

        /// <summary>
        ///     The logger object which will be used to log information,warning and error messages
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(StructureRetrieverAvailableData));

        /// <summary>
        ///     This field holds the <see cref="SpecialRequestManager" /> which manages all special requests
        /// </summary>
        private static readonly ISpecialRequestManager _specialRequestManager = new SpecialRequestManager();

        /// <summary>
        ///     This field holds the builder for <see cref="StructureRetrievalInfo" />
        /// </summary>
        private static readonly IStructureRetrievalInfoBuilder _structureRetrievalInfoBuilder =
            new StructureRetrievalInfoBuilder();

        /// <summary>
        /// The settings
        /// </summary>
        private readonly StructureRetrieverSettings _settings;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthStructureRetrieverAvailableDataV20" /> class.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <exception cref="System.ArgumentNullException">defaultHeader is null</exception>
        /// <exception cref="System.ArgumentNullException">defaultHeader is null</exception>
        /// <exception cref="SdmxException">Could not establish a connection to the mapping store DB</exception>
        /// <exception cref="ArgumentNullException">defaultHeader or connectionStringSettings is null</exception>
        public AuthStructureRetrieverAvailableDataV20(StructureRetrieverSettings settings)
            : base(settings.PassNoNull(nameof(settings)).ConnectionStringSettings)
        {
            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            _settings = settings;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Retrieve the <paramref name="queries" /> and populate the <paramref name="mutableObjects" />
        /// </summary>
        /// <param name="retrievalManager">
        ///     The retrieval manager.
        /// </param>
        /// <param name="mutableObjects">
        ///     The mutable objects.
        /// </param>
        /// <param name="queries">
        ///     The structure queries
        /// </param>
        /// <param name="returnLatest">
        ///     Set to <c>true</c> to retrieve the latest; otherwise set to <c>false</c> to retrieve all versions
        /// </param>
        /// <param name="queryDetail">
        ///     The <see cref="StructureQueryDetail" /> which controls if the output will include details or not.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed dataflows.
        /// </param>
        /// <param name="crossReferenceMutableRetrievalManager">
        ///     The cross-reference manager
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="queries"/> is <see langword="null" />.</exception>
        protected override void PopulateMutables(
            IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalManager, 
            IMutableObjects mutableObjects, 
            IList<IStructureReference> queries, 
            bool returnLatest, 
            StructureQueryDetail queryDetail,
            IList<IMaintainableRefObject> allowedDataflows, 
            IAuthCrossReferenceMutableRetrievalManager crossReferenceMutableRetrievalManager)
        {
            if (queries == null)
            {
                throw new ArgumentNullException("queries");
            }

            try
            {
                // try get the codelist reference
                var codelistReference = GetCodelistReference(queries);

                // try get the dataflow ref
                var dataflowRef = GetDataflowReference(queries);

                // check if it is special
                if (IsSpecialRequest(queries, codelistReference, dataflowRef))
                {
                    if (retrievalManager == null)
                    {
                        throw new ArgumentNullException(nameof(retrievalManager));
                    }

                    // get the dataflow
                    base.PopulateMutables(
                        retrievalManager, 
                        mutableObjects, 
                        new IStructureReference[] { dataflowRef }, 
                        returnLatest, 
                        queryDetail, 
                        allowedDataflows, 
                        crossReferenceMutableRetrievalManager);

                    var dataflowMutable = mutableObjects.GetMaintainables(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)).FirstOrDefault() as IDataflowMutableObject;
                    if (dataflowMutable == null)
                    {
                        throw new SdmxNoResultsException("No results not found");
                    }

                    var dsd =
                    retrievalManager.GetMutableDataStructure(
                        dataflowMutable.DataStructureRef.ToComplex(),
                        ComplexStructureQueryDetail.GetFromEnum(ComplexStructureQueryDetailEnumType.Full));
                    if (dsd == null)
                    {
                        throw new SdmxNoResultsException("No results not found");
                    }

                    // get the partial codelist
                    this.GetPartialCodelist(mutableObjects, allowedDataflows, dataflowRef, codelistReference, dataflowMutable, dsd);
                }
                else
                {
                    // not special requests
                    base.PopulateMutables(
                        retrievalManager, 
                        mutableObjects, 
                        queries, 
                        returnLatest, 
                        queryDetail, 
                        allowedDataflows, 
                        crossReferenceMutableRetrievalManager);
                }
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException e)
            {
                string mesage = "Mapping Store connection error." + e.Message;
                _logger.Error(mesage, e);
                throw new SdmxInternalServerException(mesage);
            }
            catch (NotImplementedException e)
            {
                string mesage = e.Message;
                _logger.Error(mesage, e);
                throw new SdmxNotImplementedException(e);
            }
            catch (Exception e)
            {
                string mesage = e.Message;
                _logger.Error(mesage, e);
                throw new SdmxInternalServerException(mesage);
            }
        }

        /// <summary>
        /// Determines whether the request is special
        /// </summary>
        /// <param name="queries">The queries.</param>
        /// <param name="codelistReference">The codelist reference.</param>
        /// <param name="dataflowRef">The dataflow reference.</param>
        /// <returns>True if the request is special otherwise false.</returns>
        private static bool IsSpecialRequest(IList<IStructureReference> queries, IMaintainableRefObject codelistReference, IConstrainableStructureReference dataflowRef)
        {
            return codelistReference != null && dataflowRef != null && dataflowRef.ConstraintObject != null &&
                   queries.Count == 2;
        }

        /// <summary>
        /// Gets the dataflow reference.
        /// </summary>
        /// <param name="queries">
        /// The queries.
        /// </param>
        /// <returns>
        /// The <see cref="IConstrainableStructureReference"/>.
        /// </returns>
        private static IConstrainableStructureReference GetDataflowReference(IEnumerable<IStructureReference> queries)
        {
            var dataflowRef =
                queries.FirstOrDefault(
                    structureReference => structureReference.TargetReference.EnumType == SdmxStructureEnumType.Dataflow) as
                IConstrainableStructureReference;
            return dataflowRef;
        }

        /// <summary>
        /// Gets the codelist reference.
        /// </summary>
        /// <param name="queries">
        /// The queries.
        /// </param>
        /// <returns>
        /// The <see cref="IMaintainableRefObject"/>.
        /// </returns>
        private static IMaintainableRefObject GetCodelistReference(IEnumerable<IStructureReference> queries)
        {
            IMaintainableRefObject codelistReference =
                queries.Where(
                    structureReference => structureReference.TargetReference.EnumType == SdmxStructureEnumType.CodeList)
                    .Select(structureReference => structureReference.MaintainableReference)
                    .FirstOrDefault();
            return codelistReference;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Process the <paramref name="current" /> and populate the <paramref name="mutableObjects" />
        /// </summary>
        /// <param name="av">
        ///     The current Structure Retrieval state
        /// </param>
        /// <param name="current">
        ///     The <see cref="IMaintainableRefObject" /> codelist request
        /// </param>
        /// <param name="mutableObjects">The output mutable objects</param>
        /// <returns>
        ///     <c>true</c> if the partial codelist is found found is larger than 0. Else <c>false</c>
        /// </returns>
        private static bool ProcessReference(
            StructureRetrievalInfo av, 
            IMaintainableRefObject current, 
            IMutableObjects mutableObjects)
        {
            ICodelistMutableObject availableData = _specialRequestManager.RetrieveAvailableData(current, av);
            if (availableData != null)
            {
                mutableObjects.AddCodelist(availableData);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the partial codelist.
        /// </summary>
        /// <param name="mutableObjects">The mutable objects.</param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <param name="dataflowRef">The dataflow reference.</param>
        /// <param name="codelistReference">The codelist reference.</param>
        /// <param name="dataflowMutable">The dataflow mutable.</param>
        /// <param name="dsd">The DSD.</param>
        /// <exception cref="SdmxNoResultsException">No codes found for <paramref name="codelistReference" /></exception>
        private void GetPartialCodelist(IMutableObjects mutableObjects, IList<IMaintainableRefObject> allowedDataflows, IConstrainableStructureReference dataflowRef, IMaintainableRefObject codelistReference, IDataflowMutableObject dataflowMutable, IDataStructureMutableObject dsd)
        {
            StructureRetrievalInfo structureRetrievalInfo = _structureRetrievalInfoBuilder.Build(
                dataflowRef,
                allowedDataflows,
                this._settings,
                dataflowMutable,
                dsd);
            if (!ProcessReference(structureRetrievalInfo, codelistReference, mutableObjects))
            {
                string message = string.Format(CultureInfo.InvariantCulture, "No codes found for {0}", codelistReference);
                _logger.Error(message);
                throw new SdmxNoResultsException(message);
            }
        }

        #endregion
    }
}