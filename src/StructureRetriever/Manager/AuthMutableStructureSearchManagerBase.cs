// -----------------------------------------------------------------------
// <copyright file="AuthMutableStructureSearchManagerBase.cs" company="EUROSTAT">
//   Date Created : 2013-06-17
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;

    using Factory;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    using Sdmxsource.Extension.Builder;
    using Sdmxsource.Extension.Extension;
    using Sdmxsource.Extension.Manager;

    using Sri.MappingStoreRetrieval.Factory;
    using Sri.MappingStoreRetrieval.Manager;

    /// <summary>
    ///     The AUTH mutable structure search manager base class.
    /// </summary>
    [Obsolete("MSDB 7.0: After migrating to msdb 7, MappingStoreCommonSdmxObjectRetriever should be used instead")]
    public abstract class AuthMutableStructureSearchManagerBase : AuthAdvancedMutableStructureSearchManagerBase, 
        IAuthMutableStructureSearchManager
    {
        #region Static Fields

        /// <summary>
        ///     The _complex query builder
        /// </summary>
        private static readonly StructureQuery2ComplexQueryBuilder _complexQueryBuilder =
            new StructureQuery2ComplexQueryBuilder();

        /// <summary>
        ///     The log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(AuthMutableStructureSearchManagerBase));

        /// <summary>
        ///     The _resolver factory
        /// </summary>
        private static readonly IResolverFactory _resolverFactory;

        #endregion

        #region Fields

        /// <summary>
        ///     The advanced retrieval advanced manager
        /// </summary>
        private readonly IAuthAdvancedSdmxMutableObjectRetrievalManager _retrievalAdvancedManager;

        /// <summary>
        ///     The cross reference manager.
        /// </summary>
        private readonly IAuthCrossRetrievalManagerFactory _crossReferenceManager;

        /// <summary>
        ///     The _database.
        /// </summary>
        private readonly Database _database;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes static members of the <see cref="AuthMutableStructureSearchManagerBase" /> class.
        /// </summary>
        static AuthMutableStructureSearchManagerBase()
        {
            _resolverFactory = new ResolverFactory();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthMutableStructureSearchManagerBase" /> class.
        /// </summary>
        /// <param name="mutableRetrievalManagerFactory">
        ///     The mutable retrieval manager factory.
        /// </param>
        /// <param name="crossReferenceManager">
        ///     The cross reference manager.
        /// </param>
        /// <param name="connectionStringSettings">
        ///     The connection string settings.
        /// </param>
        protected AuthMutableStructureSearchManagerBase(
            IAuthAdvancedSdmxMutableObjectRetrievalManager mutableRetrievalManagerFactory, 
            IAuthCrossRetrievalManagerFactory crossReferenceManager, 
            ConnectionStringSettings connectionStringSettings)
        {
            //this._crossReferenceManager = crossReferenceManager ?? new AuthCrossMutableRetrievalManagerFactory();
            //var database = new Database(connectionStringSettings);
            //this._database = database;
            //this._retrievalAdvancedManager = mutableRetrievalManagerFactory ?? new Sri.MappingStoreRetrieval.Manager.AuthAdvancedStructureRetriever(database);
            throw new InvalidOperationException("AuthMutableStructureSearchManagerBase class is obsolete");
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthMutableStructureSearchManagerBase" /> class.
        /// </summary>
        /// <param name="connectionStringSettings">
        ///     The connection string settings.
        /// </param>
        protected AuthMutableStructureSearchManagerBase(ConnectionStringSettings connectionStringSettings)
        {
            //this._database = new Database(connectionStringSettings);
            //this._crossReferenceManager = new AuthCrossMutableRetrievalManagerFactory();

            //// advanced
            //this._retrievalAdvancedManager = new Sri.MappingStoreRetrieval.Manager.AuthAdvancedStructureRetriever(_database);
            throw new InvalidOperationException("AuthMutableStructureSearchManagerBase class is obsolete");
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthMutableStructureSearchManagerBase" /> class.
        /// </summary>
        /// <param name="fullRetrievalManager">
        ///     The full retrieval manager. Used for <see cref="StructureQueryDetailEnumType.Full" /> or
        ///     <see cref="StructureQueryDetailEnumType.ReferencedStubs" />
        /// </param>
        /// <param name="crossReferenceManager">
        ///     The cross reference manager. Set it to be able to retrieve cross references. Used for
        ///     <see cref="StructureQueryDetailEnumType.Full" />
        ///     .
        /// </param>
        protected AuthMutableStructureSearchManagerBase(
            IAuthSdmxMutableObjectRetrievalManager fullRetrievalManager, 
            IAuthCrossRetrievalManagerFactory crossReferenceManager)
        {
            //if (fullRetrievalManager == null)
            //{
            //    throw new ArgumentNullException("fullRetrievalManager");
            //}

            //this._crossReferenceManager = crossReferenceManager;
            throw new InvalidOperationException("AuthMutableStructureSearchManagerBase class is obsolete");
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Returns the latest version of the maintainable for the given maintainable input
        /// </summary>
        /// <param name="maintainableObject">
        ///     The maintainable Object.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        /// <returns>
        ///     The <see cref="IMaintainableMutableObject" />.
        /// </returns>
        public virtual IMaintainableMutableObject GetLatest(
            IMaintainableMutableObject maintainableObject, 
            IList<IMaintainableRefObject> allowedDataflows)
        {
            //if (maintainableObject == null)
            //{
            //    return null;
            //}

            //// Create a reference *without* the version, because we want the latest.
            //IStructureReference reference = new StructureReferenceImpl(maintainableObject.AgencyId, maintainableObject.Id, null, maintainableObject.StructureType);
            //IMutableObjects objects = new MutableObjectsImpl();
            //this.PopulateMutables(
            //    objects, 
            //    new[] { reference }, 
            //    true, 
            //    StructureQueryDetailEnumType.Full, 
            //    allowedDataflows);
            //var maintainable = objects.GetMaintainables(maintainableObject.StructureType).FirstOrDefault();

            //if (maintainable == null)
            //{
            //    throw new SdmxNoResultsException("No structures found for the specific query");
            //}

            //return maintainable;
            throw new InvalidOperationException("AuthMutableStructureSearchManagerBase class is obsolete");
        }

        /// <summary>
        ///     Returns a set of maintainable that match the given query parameters
        /// </summary>
        /// <param name="structureQuery">
        ///     The structure Query.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="structureQuery" /> is null
        /// </exception>
        /// <returns>
        ///     The <see cref="IMutableObjects" />.
        /// </returns>
        public virtual IMutableObjects GetMaintainables(
            IRestStructureQuery structureQuery, 
            IList<IMaintainableRefObject> allowedDataflows)
        {
            //_log.InfoFormat(CultureInfo.InvariantCulture, "Query for maintainable artefact(s): {0}", structureQuery);
            //if (structureQuery == null)
            //{
            //    throw new ArgumentNullException("structureQuery");
            //}

            //IMutableObjects mutableObjects = new MutableObjectsImpl();

            //var cachedRetrievalManager = new AuthCachedAdvancedStructureRetriever(this._retrievalAdvancedManager);
            //var crossReferenceMutableRetrievalManager = this._crossReferenceManager.GetCrossRetrievalManager(this._database, cachedRetrievalManager);
            //var complexStructureQuery = _complexQueryBuilder.Build(structureQuery);

            //this.PopulateMutables(cachedRetrievalManager, mutableObjects, complexStructureQuery, allowedDataflows, crossReferenceMutableRetrievalManager);
            //StructureQueryDetailEnumType returnDetail = structureQuery.StructureQueryMetadata.StructureQueryDetail.EnumType;
            //if (returnDetail == StructureQueryDetailEnumType.ReferencePartial)
            //{
            //    // if detail=referencesPartial, the resolution of references and the processing of constraints 
            //    // will be performed from the CrossConstrainedReferenceRetrievalManager.
            //    new CrossConstrainedReferenceRetrievalManager(this._database, cachedRetrievalManager, this._crossReferenceManager)
            //        .ProcessConstraints(structureQuery, mutableObjects, allowedDataflows);
            //} else
            //{
            //    GetDetails(structureQuery, mutableObjects, crossReferenceMutableRetrievalManager, allowedDataflows);
            //}

            //if (mutableObjects.AllMaintainables.Count == 0)
            //{
            //    throw new SdmxNoResultsException("No structures found for the specific query");
            //}

            //return mutableObjects;
            throw new InvalidOperationException("AuthMutableStructureSearchManagerBase class is obsolete");
        }

        /// <summary>
        ///     Retrieves all structures that match the given query parameters in the list of query objects.  The list
        ///     must contain at least one StructureQueryObject.
        /// </summary>
        /// <param name="queries">
        ///     The queries.
        /// </param>
        /// <param name="resolveReferences">
        ///     - if set to true then any cross referenced structures will also be available in the SdmxObjects container
        /// </param>
        /// <param name="returnStub">
        ///     - if set to true then only stubs of the returned objects will be returned.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        /// <returns>
        ///     The <see cref="IMutableObjects" />.
        /// </returns>
        public virtual IMutableObjects RetrieveStructures(
            IList<IStructureReference> queries, 
            bool resolveReferences,
            StructureQueryDetail queryDetail, 
            IList<IMaintainableRefObject> allowedDataflows)
        {
            //IMutableObjects mutableObjects = new MutableObjectsImpl();

            //var cachedRetrievalManager = new AuthCachedAdvancedStructureRetriever(this._retrievalAdvancedManager);

            //var crossReferenceMutableRetrievalManager = this._crossReferenceManager.GetCrossRetrievalManager(this._database, cachedRetrievalManager);
            //this.PopulateMutables(
            //    cachedRetrievalManager, 
            //    mutableObjects, 
            //    queries, 
            //    false, 
            //    returnStub.GetStructureQueryDetail(), 
            //    allowedDataflows, 
            //    crossReferenceMutableRetrievalManager);

            //if (resolveReferences)
            //{
            //    var resolver =
            //        _resolverFactory.GetResolver(
            //            StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.Children), 
            //            crossReferenceMutableRetrievalManager);
            //    resolver.ResolveReferences(mutableObjects, returnStub, allowedDataflows);
            //}

            //if (mutableObjects.AllMaintainables.Count == 0)
            //{
            //    throw new SdmxNoResultsException("No structures found for the specific query");
            //}

            //return mutableObjects;
            throw new InvalidOperationException("AuthMutableStructureSearchManagerBase class is obsolete");
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Retrieve the structures referenced by <paramref name="queries" /> and populate the
        ///     <paramref name="mutableObjects" />
        /// </summary>
        /// <param name="mutableObjects">
        ///     The mutable objects.
        /// </param>
        /// <param name="queries">
        ///     The queries.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        protected virtual void PopulateMutables(
            IMutableObjects mutableObjects, 
            IList<IStructureReference> queries, 
            bool returnLatest, 
            StructureQueryDetailEnumType returnStub, 
            IList<IMaintainableRefObject> allowedDataflows)
        {
            //var cachedRetrievalManager = new AuthCachedAdvancedStructureRetriever(this._retrievalAdvancedManager);

            //var crossReferenceMutableRetrievalManager = this._crossReferenceManager.GetCrossRetrievalManager(this._database, cachedRetrievalManager);
            //this.PopulateMutables(
            //    cachedRetrievalManager, 
            //    mutableObjects, 
            //    queries, 
            //    returnLatest, 
            //    returnStub, 
            //    allowedDataflows, 
            //    crossReferenceMutableRetrievalManager);
            throw new InvalidOperationException("AuthMutableStructureSearchManagerBase class is obsolete");
        }

        /// <summary>
        ///     Retrieve the structures referenced by <paramref name="queries" /> and populate the
        ///     <paramref name="mutableObjects" />
        /// </summary>
        /// <param name="retrievalManager">
        ///     The retrieval manager.
        /// </param>
        /// <param name="mutableObjects">
        ///     The mutable objects.
        /// </param>
        /// <param name="queries">
        ///     The queries.
        /// </param>
        /// <param name="returnLatest">
        ///     The return Latest.
        /// </param>
        /// <param name="returnStub">
        ///     The return Stub.
        /// </param>
        /// <param name="allowedDataflows">
        ///     The allowed Dataflows.
        /// </param>
        /// <param name="crossReferenceMutableRetrievalManager">
        ///     The cross reference mutable retrieval manager.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="queries"/> is <see langword="null" />.</exception>
        protected virtual void PopulateMutables(
            IAuthAdvancedSdmxMutableObjectRetrievalManager retrievalManager, 
            IMutableObjects mutableObjects, 
            IList<IStructureReference> queries, 
            bool returnLatest, 
            StructureQueryDetail queryDetail, 
            IList<IMaintainableRefObject> allowedDataflows, 
            IAuthCrossReferenceMutableRetrievalManager crossReferenceMutableRetrievalManager)
        {
            //if (queries == null)
            //{
            //    throw new ArgumentNullException("queries");
            //}

            ////// changes here might also apply to AuthAdvancedMutableStructureSearchManagerBase 
            //for (int i = 0; i < queries.Count; i++)
            //{
            //    var structureReference = queries[i];
            //    IRestStructureQuery restStructureQuery = new RESTStructureQueryCore(
            //        StructureQueryDetail.GetFromEnum(returnStub), 
            //        StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.None), 
            //        null, 
            //        structureReference, 
            //        returnLatest);
            //    var complexStructureQuery = _complexQueryBuilder.Build(restStructureQuery);
            //    this.PopulateMutables(retrievalManager, mutableObjects, complexStructureQuery, allowedDataflows, crossReferenceMutableRetrievalManager);
            //}
            throw new InvalidOperationException("AuthMutableStructureSearchManagerBase class is obsolete");
        }

        /// <summary>
        ///     Get details specified in <paramref name="structureQuery" /> of the specified <paramref name="mutableObjects" />
        /// </summary>
        /// <param name="structureQuery">
        ///     The structure query.
        /// </param>
        /// <param name="mutableObjects">
        ///     The mutable objects.
        /// </param>
        /// <param name="crossReferenceMutableRetrievalManager">
        ///     The cross Reference Mutable Retrieval Manager.
        /// </param>
        /// <param name="allowedDataflow">
        ///     The allowed Dataflow.
        /// </param>
        /// <exception cref="NotImplementedException">
        ///     Not implemented value at <see cref="StructureReferenceDetail" /> of <paramref name="structureQuery" />
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     Invalid value at <see cref="StructureReferenceDetail" /> of <paramref name="structureQuery" />
        /// </exception>
        private static void GetDetails(
            IRestStructureQuery structureQuery, 
            IMutableObjects mutableObjects, 
            IAuthCrossReferenceMutableRetrievalManager crossReferenceMutableRetrievalManager, 
            IList<IMaintainableRefObject> allowedDataflow)
        {
            //_log.InfoFormat("Reference detail: {0}", structureQuery.StructureQueryMetadata.StructureReferenceDetail);

            //bool returnStub = structureQuery.StructureQueryMetadata.StructureQueryDetail.EnumType !=
            //                  StructureQueryDetailEnumType.Full;
            //var resolver = _resolverFactory.GetResolver(
            //    structureQuery.StructureQueryMetadata.StructureReferenceDetail,
            //    crossReferenceMutableRetrievalManager,
            //    structureQuery.StructureQueryMetadata.SpecificStructureReference);
            //resolver.ResolveReferences(mutableObjects, returnStub, allowedDataflow, structureQuery.SpecificItems);
            throw new InvalidOperationException("AuthMutableStructureSearchManagerBase class is obsolete");
        }

        #endregion
    }
}