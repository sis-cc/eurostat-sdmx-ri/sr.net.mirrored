// -----------------------------------------------------------------------
// <copyright file="PartialChildrenRetriever.cs" company="EUROSTAT">
//   Date Created : 2023-02-01
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------


namespace Estat.Sri.StructureRetriever.Manager
{
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Sdmx.MappingStore.Retrieve.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    /// <summary>
    /// Implementation of <see cref="IChildrenRetriever"/> that retrieves partial children. 
    /// For retrieving all children use <see cref="ChildrenRetriever"/>
    /// </summary>
    /// <remarks>
    /// Currently retrieving partial children is supported only when retrieving Dataflow or DSD with referencepartial=true
    /// </remarks>
    internal class PartialChildrenRetriever : ChildrenRetrieverBase
    {
        private readonly IMutableObjects _parents;

        internal PartialChildrenRetriever(IRetrievalEngineContainer retrievalEngineContainer, IMutableObjects parents) 
            : base(retrievalEngineContainer)
        {
            this._parents = parents;
        }

        protected override ICommonStructureQuery BuildQuery(ICommonStructureQuery structureQuery, SdmxStructureType childType)
        {
            if (childType.EnumType == SdmxStructureEnumType.CategoryScheme &&
                structureQuery.MaintainableTarget.EnumType == SdmxStructureEnumType.Categorisation)
            {
                var partialQuery = new PartialCategorySchemeStructureQuery(structureQuery);
                partialQuery.AddCategorisations(_parents.Categorisations.ToArray());
                return partialQuery;
            }
            else if (childType.EnumType == SdmxStructureEnumType.CodeList &&
                structureQuery.MaintainableTarget.EnumType == SdmxStructureEnumType.Dsd)
            {
                var partialQuery = new PartialCodelistStructureQuery(structureQuery);
                partialQuery.AddContentConstraints(_parents.ContentConstraints.ToArray());
                // retrieve also Content Constraints for DSD
                IEnumerable<IContentConstraintMutableObject> dsdConstraints =
                    RetrievalEngineContainer.ContentConstraintRetrievalEngine.RetrieveAsParents(structureQuery);
                partialQuery.AddContentConstraints(dsdConstraints.ToArray());
                // this will fill up the partialQuery with the CodelistToComponents map
                RetrievalEngineContainer.DSDRetrievalEngine.Retrieve(partialQuery);
                return partialQuery;
            }
            else
            {
                return structureQuery;
            }
        }

        protected override IRetrievalEngine<IMaintainableMutableObject> GetRetrievalEngine(SdmxStructureType childType)
        {
            return RetrievalEngineContainer.GetPartialEngine(childType);
        }

        protected override bool ShouldRetrieveChildren(ICommonStructureQuery structureQuery, SdmxStructureType childType)
        {
            //NOTE: if this case was executed, it would replace the partial codelists, taken from DSD parent, with the full ones.
            //So we don't want to retrieve children for this scenario
            return !(childType.EnumType == SdmxStructureEnumType.CodeList &&
                structureQuery.MaintainableTarget.EnumType == SdmxStructureEnumType.ConceptScheme);
        }
    }
}
