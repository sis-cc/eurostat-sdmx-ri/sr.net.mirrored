// -----------------------------------------------------------------------
// <copyright file="MappingStoreCommonSdmxObjectRetrieverSoap20.cs" company="EUROSTAT">
//   Date Created : 2022-05-25
//   Copyright (c) 2012, 2016 by the European Union. 
//     All rights reserved. This program and the accompanying materials are made
//    available under the terms of the GNU Lesser General Public License v 3.0
//    which accompanies this distribution, and is available at
//    http://www.gnu.org/licenses/lgpl.html
//     This file is part of SdmxAPI.
//     SdmxAPI is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Lesser General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//     SdmxAPI is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Lesser General Public License for more details.
//     You should have received a copy of the GNU Lesser General Public License
//     along with SdmxAPI.  If not, see <http://www.gnu.org/licenses/>.
//     Contributors:
//     Metadata Technology - initial API and implementation
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Nsi.StructureRetriever.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security.Principal;
    using System.Text;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sri.CustomRequests.Model;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using log4net;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.CategoryScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Estat.Nsi.StructureRetriever.Builders;
    using DryIoc.ImTools;
    using Estat.Nsi.StructureRetriever.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Dapper;
    using Estat.Sri.MappingStore.Store.Extension;
    using Estat.Sri.MappingStoreRetrieval.Extensions;
    using Estat.Sdmxsource.Extension.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Util.Extensions;
    using System.Diagnostics;
    using Estat.Nsi.StructureRetriever.Engines;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using log4net.Repository.Hierarchy;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Common;
    using Estat.Sri.MappingStoreRetrieval.Engine;

    public class MappingStoreCommonSdmxObjectRetrieverSoap20 : ICommonSdmxObjectRetrievalManagerSoap20
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(MappingStoreCommonSdmxObjectRetrieverSoap20));

        private readonly MappingStoreCommonSdmxObjectRetriever _mappingStoreCommonSdmxObjectRetriever;
        private readonly Database _database;
        /// <summary>
        /// The available data retrieval engine
        /// </summary>
        private readonly IAvailableDataRetrievalEngine[] _availableDataRetrievalEngines = { new MeasureDimension20CrossMappingRetriever(), new CachedAvailableConstraintRetriever(), new DynamicConstraintRetriever() };
        /// <summary>
        ///     This field holds the <see cref="SpecialRequestManager" /> which manages all special requests
        /// </summary>
        private static readonly ISpecialRequestManager _specialRequestManager = new SpecialRequestManager();

        /// <summary>
        ///     This field holds the builder for <see cref="StructureRetrievalInfo" />
        /// </summary>
        private static readonly IStructureRetrievalInfoBuilder _structureRetrievalInfoBuilder =
            new StructureRetrievalInfoBuilder();

        /// <summary>
        /// The settings
        /// </summary>
        private readonly StructureRetrieverSettings _settings;


        /// <summary>
        /// The reference period retrieval engine
        /// </summary>
        private readonly ReferencePeriodRetrievalEngine _referencePeriodRetrievalEngine = new ReferencePeriodRetrievalEngine();
        /// <summary>
        /// Initializes a new instance of the class
        /// </summary>
        /// <param name="mappingStoreDb"></param>
        public MappingStoreCommonSdmxObjectRetrieverSoap20(Database mappingStoreDb, StructureRetrieverSettings settings)
        {
            _mappingStoreCommonSdmxObjectRetriever = new MappingStoreCommonSdmxObjectRetriever(mappingStoreDb, settings);
            _database = mappingStoreDb;
            _settings = settings;
        }

        /// <summary>
        /// Retrieves all structures that match the given query parameters.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="principal">not used from this implementation.</param>
        /// <returns></returns>
        /// <exception cref="SdmxNoResultsException"></exception>
        public ISdmxObjects RetrieveStructures(ISoap20CommonStructureQuery query, IPrincipal principal)
        {
            ISdmxObjects returnObjects = new SdmxObjectsImpl();

            // build the common query with the metadata for all structure references.
            var commonQueryBuilder = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.SOAP, StructureOutputFormatEnumType.SdmxV2StructureDocument)
                .SetRequestedDetail(query.ReturnStub ? ComplexStructureQueryDetailEnumType.Stub : ComplexStructureQueryDetailEnumType.Full)
                .SetReferences(query.ResolveReferences ? StructureReferenceDetailEnumType.Children : StructureReferenceDetailEnumType.None)
                .SetReferencedDetail(query.ReturnStub ? ComplexMaintainableQueryDetailEnumType.Stub : ComplexMaintainableQueryDetailEnumType.Full);

            var dataflowRef =
                query.SimpleStructureQueries.FirstOrDefault(
                    structureReference => structureReference.TargetReference.EnumType == SdmxStructureEnumType.Dataflow) as
                IConstrainableStructureReference;
            var codelistReference = query.SimpleStructureQueries.FirstOrDefault(x => x.MaintainableStructureEnumType == SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList));
            // check if it is special
            if (IsSpecialRequest(query.SimpleStructureQueries, codelistReference, dataflowRef))
            {
                
                commonQueryBuilder.SetStructureIdentification(dataflowRef);
                //get dataflow and dsd
                commonQueryBuilder.SetReferences(StructureReferenceDetailEnumType.Children);
                ICommonStructureQuery commonStructureQuery = commonQueryBuilder.Build();
                

                // get the dataflow
                ISdmxObjects sdmxObjects = _mappingStoreCommonSdmxObjectRetriever.GetMaintainables(commonStructureQuery);
                var dataflowMutable = sdmxObjects.GetMaintainables(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow)).FirstOrDefault() as IDataflowObject;
                if (dataflowMutable == null)
                {
                    throw new SdmxNoResultsException("No results not found");
                }

                var dsd = sdmxObjects.GetMaintainables(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd)).FirstOrDefault() as IDataStructureObject; ;
                if (dsd == null)
                {
                    throw new SdmxNoResultsException("No results not found");
                }
                IMutableObjects mutableObjects = new MutableObjectsImpl();

                var info = _structureRetrievalInfoBuilder.Build(
                dataflowRef,
                null,
                this._settings,
                dataflowMutable.MutableInstance,
                dsd.MutableInstance);
                var codelistRef = new MaintainableRefObjectImpl()
                {
                    AgencyId = codelistReference.AgencyId,
                    MaintainableId = codelistReference.MaintainableId,
                    Version = codelistReference.Version,
                };

                if (info.RequestedComponent == null)
                {
                    if (CustomCodelistConstants.IsCountRequest(codelistRef))
                    {
                        var sqlBuilder = new CountSqlBuilder();
                        info.SqlQuery = sqlBuilder.GenerateSql(info,false);
                        returnObjects.AddCodelist(new CountCodeListRetrievalEngine().GetCodeList(info).ImmutableInstance);
                    }
                }
                else
                {
                    // get the partial codelist
                    this.GetPartialCodelist(mutableObjects, info, dsd.MutableInstance);
                }
                // get the partial codelist
                
                foreach (var maintainable in mutableObjects.AllMaintainables)
                { 
                    AddMaintainableInResultSet(returnObjects, maintainable.ImmutableInstance); 
                }

                //add categorisation
                commonQueryBuilder.SetSpecificReferences(SdmxStructureEnumType.Categorisation);
                commonQueryBuilder.SetReferences(StructureReferenceDetailEnumType.Specific);
                commonQueryBuilder.SetStructureIdentification(dataflowRef);
                commonStructureQuery = commonQueryBuilder.Build();
                sdmxObjects.Merge(_mappingStoreCommonSdmxObjectRetriever.GetMaintainables(commonStructureQuery));
                sdmxObjects.RemoveDataStructure(dsd);
                returnObjects.Merge(sdmxObjects);
            }
            else
            {
                ISet<IStructureReference> crossStructRef = new HashSet<IStructureReference>();

                foreach (IStructureReference sRef in query.SimpleStructureQueries)
                {
                    _log.Info("|--- trying to retrieve " + sRef.MaintainableStructureEnumType);

                    // build rest of common query for the specific structure reference
                    commonQueryBuilder.SetStructureIdentification(sRef);

                    ICommonStructureQuery commonStructureQuery = commonQueryBuilder.Build();

                    ISdmxObjects sdmxObjects = _mappingStoreCommonSdmxObjectRetriever.GetMaintainables(commonStructureQuery);

                    // SOAP 2.0 requests for dataflows or category schemes need to also retrieve the relevant categorisations
                    // only when requested detail is FULL                 
                    if (ShouldRetrieveCategorisations(query, sRef))
                    {                                            
                        CommonStructureQueryCore.Builder categorizationQueryBuilder = CommonStructureQueryCore.Builder.NewQuery(CommonStructureQueryType.SOAP,
                            StructureOutputFormatEnumType.SdmxV2StructureDocument)
                            .SetRequestedDetail(ComplexStructureQueryDetailEnumType.Full)
                            .SetReferences(StructureReferenceDetailEnumType.Specific)
                            .SetSpecificReferences(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation))
                            .SetStructureIdentification(sRef);

                        sdmxObjects.Merge(_mappingStoreCommonSdmxObjectRetriever.GetMaintainables(categorizationQueryBuilder.Build()));
                    }

                    foreach (IMaintainableObject maintainable in sdmxObjects.GetAllMaintainables())
                    {
                        IStructureReference retrievedRefObject = new StructureReferenceImpl(maintainable.AgencyId, maintainable.Id, maintainable.Version, maintainable.StructureType);
                        //assure uniqueness in the results retrieved
                        if (!crossStructRef.Contains(retrievedRefObject))
                        {
                            crossStructRef.Add(retrievedRefObject);
                            AddMaintainableInResultSet(returnObjects, maintainable);
                        }
                    }
                }
            }
            /*check for empty list of returnObjects, if so return an Exception of NO_RESULTS_FOUND*/
            if (returnObjects.GetAllMaintainables().Count == 0)
            {
                throw new SdmxNoResultsException("No structures found for the specific query");
            }
            return returnObjects;
        }

        private bool ShouldRetrieveCategorisations(ISoap20CommonStructureQuery query, IStructureReference sRef)
        {
            return (sRef.MaintainableStructureEnumType == SdmxStructureEnumType.Dataflow ||
                    sRef.MaintainableStructureEnumType == SdmxStructureEnumType.CategoryScheme) &&
                    !query.ReturnStub;
        }

        private void AddMaintainableInResultSet(ISdmxObjects returnObjects, IMaintainableObject maintanable)
        {
            switch (maintanable.StructureType.EnumType)
            {
                case SdmxStructureEnumType.CodeList:
                    returnObjects.AddCodelist((ICodelistObject)maintanable);
                    break;
                case SdmxStructureEnumType.ConceptScheme:
                    returnObjects.AddConceptScheme((IConceptSchemeObject)maintanable);
                    break;
                case SdmxStructureEnumType.HierarchicalCodelist:
                    returnObjects.AddHierarchicalCodelist((IHierarchicalCodelistObject)maintanable);
                    break;
                case SdmxStructureEnumType.Dsd:
                    returnObjects.AddDataStructure((IDataStructureObject)maintanable);
                    break;
                case SdmxStructureEnumType.Dataflow:
                    returnObjects.AddDataflow((IDataflowObject)maintanable);
                    break;
                case SdmxStructureEnumType.CategoryScheme:
                    returnObjects.AddCategoryScheme((ICategorySchemeObject)maintanable);
                    break;
                case SdmxStructureEnumType.Categorisation:
                    returnObjects.AddCategorisation((ICategorisationObject)maintanable);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Determines whether the request is special
        /// </summary>
        /// <param name="queries">The queries.</param>
        /// <param name="codelistReference">The codelist reference.</param>
        /// <param name="dataflowRef">The dataflow reference.</param>
        /// <returns>True if the request is special otherwise false.</returns>
        private static bool IsSpecialRequest(IList<IStructureReference> queries, IMaintainableRefObject codelistReference, IConstrainableStructureReference dataflowRef)
        {
            return codelistReference != null && dataflowRef != null && dataflowRef.ConstraintObject != null &&
                   queries.Count == 2;
        }

        /// <summary>
        /// Gets the partial codelist.
        /// </summary>
        /// <param name="mutableObjects">The mutable objects.</param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <param name="dataflowRef">The dataflow reference.</param>
        /// <param name="codelistReference">The codelist reference.</param>
        /// <param name="dataflowMutable">The dataflow mutable.</param>
        /// <param name="dsd">The DSD.</param>
        /// <exception cref="SdmxNoResultsException">No codes found for <paramref name="codelistReference" /></exception>
        private void GetPartialCodelist(IMutableObjects mutableObjects,StructureRetrievalInfo structureRetrievalInfo , IDataStructureMutableObject dsd)
        {
            
            var component = dsd.ImmutableInstance.GetComponent(structureRetrievalInfo.RequestedComponent);
            if (component.Id.Equals(DimensionObject.TimeDimensionFixedId))
            {
                var key = new KeyValuesMutableImpl() { Id = DimensionObject.TimeDimensionFixedId };
                var referencePeriod = _referencePeriodRetrievalEngine.GetReferencePeriod(structureRetrievalInfo);
                ICodelistMutableObject timeCodeList = new CodelistMutableCore();
                var startDate = new SdmxDateCore(referencePeriod.StartTime).FormatAsDateString(true);
                ICodeMutableObject startCode = new CodeMutableCore
                {
                    Id = startDate
                };
                var endDate = new SdmxDateCore(referencePeriod.EndTime).FormatAsDateString(false);
                ICodeMutableObject endCode = !startDate.Equals(endDate) ? new CodeMutableCore
                {
                    Id = endDate
                }
                : null;
                SetupTimeCodelist(startCode, endCode, timeCodeList);
                mutableObjects.AddCodelist(timeCodeList);
            }
            else
            {
                var mutable = this._availableDataRetrievalEngines.Select(x => new AvailableConstraintReferencesRetriever(x).GetPartialStructure(structureRetrievalInfo, component)).FirstOrDefault(x => x != null);
                mutableObjects.AddIdentifiable(mutable.Codelists.First());
            }
           
        }

        public static void SetupTimeCodelist(ICodeMutableObject startCode, ICodeMutableObject endCode, ICodelistMutableObject timeCodeList)
        {
            timeCodeList.Id = CustomCodelistConstants.TimePeriodCodeList;
            timeCodeList.AgencyId = CustomCodelistConstants.Agency;
            timeCodeList.Version = CustomCodelistConstants.Version;
            timeCodeList.Names.Add(
                new TextTypeWrapperMutableCore
                {
                    Locale = CustomCodelistConstants.Lang,
                    Value = CustomCodelistConstants.TimePeriodCodeListName
                });
            startCode.Names.Add(
                new TextTypeWrapperMutableCore
                {
                    Locale = CustomCodelistConstants.Lang,
                    Value = CustomCodelistConstants.TimePeriodStartDescription
                });
            timeCodeList.AddItem(startCode);

            if (endCode != null)
            {
                endCode.Names.Add(
                    new TextTypeWrapperMutableCore
                    {
                        Locale = CustomCodelistConstants.Lang,
                        Value = CustomCodelistConstants.TimePeriodEndDescription
                    });

                timeCodeList.AddItem(endCode);
            }
        }
    }
}
