// -----------------------------------------------------------------------
// <copyright file="StructureRetrievalInfo.cs" company="EUROSTAT">
//   Date Created : 2012-03-29
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Model
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;

    using Estat.Nsi.StructureRetriever.Builders;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    ///     This class holds the current StructureRetrieval state
    /// </summary>
    internal class StructureRetrievalInfo
    {
        #region Constants and Fields

        /// <summary>
        ///     Get or Set the collection of allowed dataflows
        /// </summary>
        private readonly IList<IMaintainableRefObject> _allowedDataflows;

        // DataflowRefBean = IMaintainableMutableObject

        /// <summary>
        ///     This field holds the dataflowRef constrains
        /// </summary>
        private readonly List<IKeyValues> _criteria = new List<IKeyValues>();

        /// <summary>
        ///     The component filters
        /// </summary>
        private readonly List<ComponentFilter> _componentFilters = new List<ComponentFilter>();

        /// <summary>
        ///     The list of XS measures at the constraint or all XS measures.
        /// </summary>
        private readonly ISet<string> _xsMeasureDimensionConstraints =
            new HashSet<string>(StringComparer.Ordinal);

        /// <summary>
        ///     This field holds the mapping between the component name and the ComponentInfo
        /// </summary>
        private readonly Dictionary<string, ComponentInfo> _componentMapping = new Dictionary<string, ComponentInfo>();

        /// <summary>
        /// The parameters
        /// </summary>
        private readonly IList<DbParameter> _parameters = new List<DbParameter>();

        /// <summary>
        ///     Gets or sets the requested codelist reference.
        /// </summary>
        private IMaintainableRefObject _codelistRef;

        /// <summary>
        ///     The dataset SQL Query
        /// </summary>
        private string _innerSqlQuery;

        /// <summary>
        ///     The Structure access object used to get structural metadata from Mapping Store
        /// </summary>
        private ISpecialMutableObjectRetrievalManager _mastoreAccess;

        /// <summary>
        ///     This field holds the measure component name in case it is not mapped. Otherwise it is null.
        /// </summary>
        private string _measureComponent;

        /// <summary>
        ///     This field holds the ReferencePeriod
        /// </summary>
        private IReferencePeriodMutableObject _referencePeriod; // ReferencePeriodBean = IReferencePeriodMutableObject

        /// <summary>
        ///     The field contains the requested component
        /// </summary>
        private string _requestedComponent;

        /// <summary>
        ///     The <see cref="_sqlQuery" /> to execute on DDB
        /// </summary>
        private string _sqlQuery;

        /// <summary>
        ///     This field holds the name of the TimeDimension
        /// </summary>
        private string _timeDimension;

        /// <summary>
        ///     The requested component <see cref="ComponentInfo" />
        /// </summary>
        private ComponentInfo _requestedComponentInfo;

        /// <summary>
        ///     Gets or sets the frequency mapping
        /// </summary>
        private ComponentInfo _frequencyInfo;

        /// <summary>
        ///     This field holds the Retrieval Manager
        /// </summary>
        private readonly ICommonSdmxObjectRetrievalManager _retrievalManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureRetrievalInfo" /> class.
        /// Initializes a new instance of the <see cref="StructureRetrievalInfo" /> class. Initializes a new instance of the
        /// <see cref="StructureRetrievalInfo" /> class.
        /// </summary>
        /// <param name="allowedDataflows">The allowed Dataflows.</param>
        /// <param name="settings">The settings.</param>
        public StructureRetrievalInfo(IList<IMaintainableRefObject> allowedDataflows,  StructureRetrieverSettings settings)
        {
            this._allowedDataflows = allowedDataflows;
            Settings = settings;
            ConnectionBuilder = new DDbConnectionBuilder(Settings.ConnectionBuilder);

            Database database = new Database(Settings.ConnectionStringSettings);
            _retrievalManager = new MappingStoreCommonSdmxObjectRetriever(database,settings);
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the collection of allowed dataflows
        /// </summary>
        public IList<IMaintainableRefObject> AllowedDataflows
        {
            get { return this._allowedDataflows; }
        }

        /// <summary>
        ///     Gets the mapping between the component name and the ComponentInfo
        /// </summary>
        public IDictionary<string, ComponentInfo> ComponentMapping
        {
            get { return this._componentMapping; }
        }

        /// <summary>
        ///     Gets or sets the requested codelist reference.
        /// </summary>
        public IMaintainableRefObject CodelistRef
        {
            get { return this._codelistRef; }

            set { this._codelistRef = value; }
        }

        /// <summary>
        ///     Gets the dataflowRef constrains
        /// </summary>
        public List<IKeyValues> Criteria
        {
            get { return this._criteria; }
        }

        /// <summary>
        ///     Gets the component filters
        /// </summary>
        public List<ComponentFilter> ComponentFilters
        {
            get { return this._componentFilters; }
        }

        /// <summary>
        ///     Gets or sets the dataset SQL Query
        /// </summary>
        public string InnerSqlQuery
        {
            get { return this._innerSqlQuery; }

            set { this._innerSqlQuery = value; }
        }

        /// <summary>
        ///     Gets or sets the mappingSet of the dataflow specified in the constructor
        /// </summary>
        public IComponentMappingContainer MappingSet { get; set; }

        /// <summary>
        ///     Gets or sets the Structure access object used to get structural metadata from Mapping Store
        /// </summary>
        public ISpecialMutableObjectRetrievalManager MastoreAccess
        {
            get { return this._mastoreAccess; }

            set { this._mastoreAccess = value; }
        }

        /// <summary>
        ///     Gets or sets the measure component name in case it is not mapped. Otherwise it is null.
        /// </summary>
        public string MeasureComponent
        {
            get { return this._measureComponent; }

            set { this._measureComponent = value; }
        }

        /// <summary>
        ///     Gets or sets the ReferencePeriod
        /// </summary>
        public IReferencePeriodMutableObject ReferencePeriod
        {
            get { return this._referencePeriod; }

            set { this._referencePeriod = value; }
        }

        /// <summary>
        ///     Gets or sets the requested component
        /// </summary>
        public string RequestedComponent
        {
            get { return this._requestedComponent; }

            set { this._requestedComponent = value; }
        }

        /// <summary>
        ///     Gets or sets the <see cref="_sqlQuery" /> to execute on DDB
        /// </summary>
        public string SqlQuery
        {
            get { return this._sqlQuery; }

            set { this._sqlQuery = value; }
        }

        /// <summary>
        ///     Gets or sets the name of the TimeDimension
        /// </summary>
        public string TimeDimension
        {
            get { return this._timeDimension; }

            set { this._timeDimension = value; }
        }

        /// <summary>
        ///     Gets or sets the mapping used by the time dimension
        /// </summary>
        public TimeDimensionMappingEntity TimeMapping { get; set; }

        /// <summary>
        ///     Gets or sets the transcoder used by the time dimension
        /// </summary>
        public ITimeDimensionFrequencyMappingBuilder TimeTranscoder { get; set; }

        /// <summary>
        ///     Gets the list of XS measures at the constraint or all XS measures.
        /// </summary>
        public ISet<string> XSMeasureDimensionConstraints
        {
            get { return this._xsMeasureDimensionConstraints; }
        }

        /// <summary>
        ///     Gets or sets the Requested Component <see cref="ComponentInfo" />.
        /// </summary>
        public ComponentInfo RequestedComponentInfo
        {
            get { return this._requestedComponentInfo; }

            set { this._requestedComponentInfo = value; }
        }

        /// <summary>
        ///     Gets or sets the frequency mapping
        /// </summary>
        public ComponentInfo FrequencyInfo
        {
            get { return this._frequencyInfo; }

            set { this._frequencyInfo = value; }
        }

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        public IList<DbParameter> Parameters
        {
            get
            {
                return _parameters;
            }
        }

        /// <summary>
        /// Gets the connection builder.
        /// </summary>
        /// <value>
        /// The connection builder.
        /// </value>
        public IBuilder<DbConnection, StructureRetrievalInfo> ConnectionBuilder { get; private set; }

        /// <summary>
        /// Gets the settings.
        /// </summary>
        /// <value>
        /// The settings.
        /// </value>
        public StructureRetrieverSettings Settings { get; }

        /// <summary>
        /// Gets or sets the dataflow.
        /// </summary>
        /// <value>
        /// The dataflow.
        /// </value>
        public IDataflowObject Dataflow { get; set; }

        /// <summary>
        /// Gets or sets the data structure.
        /// </summary>
        /// <value>The data structure.</value>
        public IDataStructureObject DataStructure { get; set; }
        
        /// <summary>
        /// Gets or sets the start period.
        /// </summary>
        /// <value>The start period.</value>
        public ISdmxDate StartPeriod { get; internal set; }

        /// <summary>
        /// Gets or sets the end period.
        /// </summary>
        /// <value>The end period.</value>
        public ISdmxDate EndPeriod { get; internal set; }

        /// <summary>
        /// Gets or sets the dynamic query.
        /// </summary>
        /// <value>The dynamic query.</value>
        public IAvailableConstraintQuery DynamicQuery { get; internal set; }

        /// <summary>
        /// Gets the retrieval manager.
        /// </summary>
        /// <value>The retrieval manager.</value>        
        public ICommonSdmxObjectRetrievalManager RetrievalManager { get { return this._retrievalManager; } }

        #endregion
    }
}