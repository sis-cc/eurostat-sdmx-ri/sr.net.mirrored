// -----------------------------------------------------------------------
// <copyright file="StructureRetrieverSettings.cs" company="EUROSTAT">
//   Date Created : 2017-09-15
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Model
{
    using System.Configuration;
    using System.Data.Common;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The structure retriever settings.
    /// </summary>
    public class StructureRetrieverSettings
    {
        /// <summary>
        /// Gets or sets the mapping manager.
        /// </summary>
        /// <value>
        /// The mapping manager.
        /// </value>
        public IComponentMappingManager MappingManager { get; set; }

        /// <summary>
        /// Gets or sets the SDMX schema version.
        /// </summary>
        /// <value>
        /// The SDMX schema version.
        /// </value>
        public SdmxSchemaEnumType SdmxSchemaVersion { get; set;  }

        /// <summary>
        /// Gets or sets the connection string settings.
        /// </summary>
        /// <value>
        /// The connection string settings.
        /// </value>
        public ConnectionStringSettings ConnectionStringSettings { get; set; }

        /// <summary>
        ///     Gets or sets the Structure access object used to get structural metadata from Mapping Store
        /// </summary>
        public ISpecialMutableObjectRetrievalManager SpecialRetrievalManager { get; set; }

        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>
        /// The store identifier.
        /// </value>
        public string StoreId { get; set; }

        /// <summary>
        /// Gets or sets the component mapping validation manager.
        /// </summary>
        /// <value>
        /// The component mapping validation manager.
        /// </value>
        public IComponentMappingValidationManager ComponentMappingValidationManager { get; set; }

        /// <summary>
        /// Gets or sets the connection builder
        /// </summary>
        /// <value>
        /// The connection builder.
        /// </value>
        public IBuilder<DbConnection, DdbConnectionEntity> ConnectionBuilder { get; set; }

        public bool AllowSdmx21CrossSectional { get; set; } 
    }
}