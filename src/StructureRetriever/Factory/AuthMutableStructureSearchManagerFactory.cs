// -----------------------------------------------------------------------
// <copyright file="AuthMutableStructureSearchManagerFactory.cs" company="EUROSTAT">
//   Date Created : 2013-06-17
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sdmxsource.Extension.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;

    /// <summary>
    ///     The authorization mutable structure search manager factory.
    /// </summary>
    [Obsolete("MSDB 7.0: After migrating to msdb 7, MappingStoreCommonSdmxObjectRetriever should be used instead")]
    public class AuthMutableStructureSearchManagerFactory :
        StructureSearchManagerFactoryBase<IAuthMutableStructureSearchManager>, IAuthMutableStructureSearchManagerFactory
    {
        #region Static Fields

        /// <summary>
        ///     The default factory methods.
        /// </summary>
        //private static readonly IDictionary<Type, Func<object, SdmxSchema, IAuthMutableStructureSearchManager>> _factoryMethods =
        //        new Dictionary<Type, Func<object, SdmxSchema, IAuthMutableStructureSearchManager>>
        //        {
        //            {
        //                typeof(ConnectionStringSettings), OnConnectionStringSettings
        //            },
        //            {
        //                typeof(StructureRetrieverSettings), OnStructureRetrievalSettings
        //            }
        //        };

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthMutableStructureSearchManagerFactory" /> class.
        /// </summary>
        /// <param name="factoryMethod">
        ///     The factory method.
        /// </param>
        public AuthMutableStructureSearchManagerFactory(Func<object, IAuthMutableStructureSearchManager> factoryMethod)
            : base(factoryMethod)
        {
            throw new InvalidOperationException("AuthMutableStructureSearchManagerFactory class is obsolete");
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AuthMutableStructureSearchManagerFactory" /> class.
        /// </summary>
        public AuthMutableStructureSearchManagerFactory() : this(null)
        {
            throw new InvalidOperationException("AuthMutableStructureSearchManagerFactory class is obsolete");
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the factories.
        /// </summary>
        /// <returns>The type based dictionary.</returns>
        protected override IDictionary<Type, Func<object, SdmxSchema, IAuthMutableStructureSearchManager>> GetFactories()
        {
            //return _factoryMethods;
            throw new InvalidOperationException("AuthMutableStructureSearchManagerFactory class is obsolete");
        }

        /// <summary>
        ///     The on connection string settings factory method.
        /// </summary>
        /// <param name="settings">
        ///     The settings.
        /// </param>
        /// <param name="sdmxSchema">
        ///     The SDMX schema.
        /// </param>
        /// <returns>
        ///     The <see cref="IMutableStructureSearchManager" />.
        /// </returns>
        //private static IAuthMutableStructureSearchManager OnConnectionStringSettings(object settings, SdmxSchema sdmxSchema)
        //{
        //    var connectionStringSettings = settings as ConnectionStringSettings;
        //    if (connectionStringSettings == null)
        //    {
        //        return null;
        //    }

        //    SdmxSchemaEnumType sdmxSchemaEnumType = sdmxSchema != null ? sdmxSchema.EnumType : SdmxSchemaEnumType.Null;
        //    switch (sdmxSchemaEnumType)
        //    {
        //        case SdmxSchemaEnumType.VersionTwo:
        //            throw new SdmxNotImplementedException("Use StructureRetrieverSettings");
        //        case SdmxSchemaEnumType.VersionTwoPointOne:
        //            return new AuthStructureRetrieverV21(connectionStringSettings);
        //        default:
        //            return new AuthMutableStructureSearchManager(connectionStringSettings);
        //    }
        //}

        /// <summary>
        ///     The on connection string settings factory method.
        /// </summary>
        /// <param name="settings">
        ///     The settings.
        /// </param>
        /// <param name="sdmxSchema">
        ///     The SDMX schema.
        /// </param>
        /// <returns>
        ///     The <see cref="IMutableStructureSearchManager" />.
        /// </returns>
        //private static IAuthMutableStructureSearchManager OnStructureRetrievalSettings(object settings, SdmxSchema sdmxSchema)
        //{
        //    var structureRetrieverSettings = settings as StructureRetrieverSettings;
        //    if (structureRetrieverSettings == null)
        //    {
        //        return null;
        //    }

        //    SdmxSchemaEnumType sdmxSchemaEnumType = sdmxSchema != null ? sdmxSchema.EnumType : SdmxSchemaEnumType.Null;
        //    switch (sdmxSchemaEnumType)
        //    {
        //        case SdmxSchemaEnumType.VersionTwo:
        //            return new AuthStructureRetrieverAvailableDataV20(structureRetrieverSettings);
        //        case SdmxSchemaEnumType.VersionTwoPointOne:
        //            if (!structureRetrieverSettings.AllowSdmx21CrossSectional)
        //            { 
        //                return new AuthStructureRetrieverV21(structureRetrieverSettings.ConnectionStringSettings); 
        //            }
        //            else
        //            {
        //                return new AuthMutableStructureSearchWithCrossManager(structureRetrieverSettings.ConnectionStringSettings);
        //            }
        //        case SdmxSchemaEnumType.Json:
        //            return new AuthMutableStructureSearchWithCrossManager(structureRetrieverSettings.ConnectionStringSettings);
        //        default:
        //            return new AuthMutableStructureSearchManager(structureRetrieverSettings.ConnectionStringSettings);
        //    }
        //}

        #endregion
    }
}