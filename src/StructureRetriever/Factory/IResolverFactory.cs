// -----------------------------------------------------------------------
// <copyright file="IResolverFactory.cs" company="EUROSTAT">
//   Date Created : 2013-09-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Factory
{
    using System;
    using Engines.Resolver;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    using Sdmxsource.Extension.Manager;

    /// <summary>
    ///     The <see cref="IResolver" /> Factory interface.
    /// </summary>
    public interface IResolverFactory
    {
        /// <summary>
        ///     Returns the <see cref="IResolver" /> for the specified <paramref name="referenceDetailType" />.
        /// </summary>
        /// <param name="referenceDetailType">Type of the reference detail.</param>
        /// <param name="crossReferenceManager">The cross reference manager.</param>
        /// <param name="specificStructureTypes">The specific object structure types.</param>
        /// <returns>The <see cref="IResolver" /> for the specified <paramref name="referenceDetailType" />; otherwise null</returns>
        [Obsolete("Since MSDB 7.0; use the other overload instead.")]
        IResolver GetResolver(
            StructureReferenceDetail referenceDetailType, 
            IAuthCrossReferenceMutableRetrievalManager crossReferenceManager, 
            params SdmxStructureType[] specificStructureTypes);

        /// <summary>
        /// Returns the <see cref="IResolver"/> for the <paramref name="referenceDetailType"/>.
        /// </summary>
        /// <param name="retrievalEngineContainer"></param>
        /// <returns></returns>
        IResolver GetResolver(StructureReferenceDetail referenceDetailType, IRetrievalEngineContainer retrievalEngineContainer);
    }
}