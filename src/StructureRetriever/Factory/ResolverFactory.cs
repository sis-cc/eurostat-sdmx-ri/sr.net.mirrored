// -----------------------------------------------------------------------
// <copyright file="ResolverFactory.cs" company="EUROSTAT">
//   Date Created : 2013-09-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.StructureRetriever.Factory
{
    using System;

    using Engines.Resolver;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    using Sdmxsource.Extension.Manager;

    /// <summary>
    ///     The resolver factory.
    /// </summary>
    public class ResolverFactory : IResolverFactory
    {
        /// <summary>
        ///     Returns the <see cref="IResolver" /> for the specified <paramref name="referenceDetailType" />.
        /// </summary>
        /// <param name="referenceDetailType">Type of the reference detail.</param>
        /// <param name="crossReferenceManager">The cross reference manager.</param>
        /// <param name="specificStructureTypes">The specific object structure types.</param>
        /// <returns>The <see cref="IResolver" /> for the specified <paramref name="referenceDetailType" />; otherwise null</returns>
        /// <exception cref="ArgumentNullException"><paramref name="referenceDetailType"/> is <see langword="null" />.</exception>
        public IResolver GetResolver(
            StructureReferenceDetail referenceDetailType, 
            IAuthCrossReferenceMutableRetrievalManager crossReferenceManager, 
            params SdmxStructureType[] specificStructureTypes)
        {
            if (referenceDetailType == null)
            {
                throw new ArgumentNullException("referenceDetailType");
            }

            switch (referenceDetailType.EnumType)
            {
                case StructureReferenceDetailEnumType.Null:
                case StructureReferenceDetailEnumType.None:
                    return new NoneResolver();
                case StructureReferenceDetailEnumType.Parents:
                    return new ParentsResolver(crossReferenceManager);
                case StructureReferenceDetailEnumType.ParentsSiblings:
                    return new ParentsAndSiblingsResolver(crossReferenceManager);
                case StructureReferenceDetailEnumType.Children:
                    return new ChildrenResolver(crossReferenceManager);
                case StructureReferenceDetailEnumType.Descendants:
                    return new DescendantsResolver(crossReferenceManager);
                case StructureReferenceDetailEnumType.All:
                    return new AllResolver(crossReferenceManager);
                case StructureReferenceDetailEnumType.Specific:
                    throw new InvalidOperationException("SpecificResolver constructor expects IRetrievalEngineContainer not IAuthCrossReferenceMutableRetrievalManager.");
                default:
                    throw new ArgumentOutOfRangeException(referenceDetailType.EnumType.ToString());
            }
        }

        /// <inheritdoc/>
        public IResolver GetResolver(StructureReferenceDetail referenceDetailType, IRetrievalEngineContainer retrievalEngineContainer)
        {
            if (referenceDetailType == null)
            {
                throw new ArgumentNullException("referenceDetailType");
            }
            //TODO SDMXRI-1839: change the constructors when made available
            switch (referenceDetailType.EnumType)
            {
                case StructureReferenceDetailEnumType.Null:
                case StructureReferenceDetailEnumType.None:
                    return new NoneResolver();
                case StructureReferenceDetailEnumType.Parents:
                    //return new ParentsResolver(crossReferenceManager);
                    throw new NotImplementedException("ParentsResolver new constructor is not yet available.");
                case StructureReferenceDetailEnumType.ParentsSiblings:
                    //return new ParentsAndSiblingsResolver(crossReferenceManager);
                    throw new NotImplementedException("ParentsAndSiblingsResolver new constructor is not yet available.");
                case StructureReferenceDetailEnumType.Children:
                    //return new ChildrenResolver(crossReferenceManager);
                    throw new NotImplementedException("ChildrenResolver new constructor is not yet available.");
                case StructureReferenceDetailEnumType.Descendants:
                    //return new DescendantsResolver(crossReferenceManager);
                    throw new NotImplementedException("DescendantsResolver new constructor is not yet available.");
                case StructureReferenceDetailEnumType.All:
                    //return new AllResolver(crossReferenceManager);
                    throw new NotImplementedException("AllResolver new constructor is not yet available.");
                case StructureReferenceDetailEnumType.Specific:
                    return new SpecificResolver(retrievalEngineContainer);
                default:
                    throw new ArgumentOutOfRangeException(referenceDetailType.EnumType.ToString());
            }
        }
    }
}