// -----------------------------------------------------------------------
// <copyright file="TestReferencePartialCodelists.cs" company="EUROSTAT">
//   Date Created : 2022-12-07
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace StructureRetriever.Tests
{
    using System;
    using System.Linq;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    [TestFixture("sqlserver_scratch")]
    public class TestReferencePartialCodelists : RetrieverTestBase
    {
        public TestReferencePartialCodelists(string storeId) 
            : base(storeId)
        {
        }

        [OneTimeSetUp]
        public void SetUp()
        {
            InitializeMappingStore();

            InsertStructures("tests/DSD_withConstraints.xml", SdmxSchemaEnumType.VersionTwoPointOne);
        }

        /// <summary>
        /// Test the detail=referencepartial for the codelists.
        /// </summary>
        /// <param name="queryPath"></param>
        /// <param name="codelistId">The Id of the codelist to check</param>
        /// <param name="isPartial">defines whether is supposed to be partial (if not all items returned)</param>
        /// <param name="expectedCodesNum">The expected number of items to be returned for the given codelist.</param>
        [TestCase("dataflow/OECD/STES@BTS/1.0?detail=full&references=all", "CL_SUBJECT", false, 121)]
        [TestCase("dataflow/OECD/STES@BTS/1.0?detail=referencepartial&references=all", "CL_SUBJECT", true, 20)] // contained in two content constraints
        [TestCase("dataflow/OECD/STES@BTS/1.0?detail=referencepartial&references=descendants", "CL_SUBJECT", true, 20)]
        [TestCase("dataflow/OECD/STES@BTS/1.0?detail=full&references=all", "CL_COICOP", false, 144)] 
        [TestCase("dataflow/OECD/STES@BTS/1.0?detail=referencepartial&references=all", "CL_COICOP", true, 143)] // excluded cube region for _X code
        [TestCase("dataflow/OECD/STES@BTS/1.0?detail=referencepartial&references=descendants", "CL_COICOP", true, 143)]
        public void TestCodelists_noCascade(string queryPath, string codelistId, bool isPartial, int expectedCodesNum)
        {
            ICommonStructureQuery structureQuery = BuildStructureQuery(queryPath);
            using (DataflowFilterContext context = new DataflowFilterContext(Estat.Sri.MappingStoreRetrieval.Constants.DataflowFilter.Any))
            {
                ISdmxObjects sdmxObjects = RetrievalManager.GetMaintainables(structureQuery);


                var codelist = sdmxObjects.Codelists.SingleOrDefault(s => s.Id.Equals(codelistId, StringComparison.Ordinal));
                Assert.IsNotNull(codelist, $"Codelist {codelistId} not returned");
                Assert.AreEqual(isPartial, codelist.Partial, $"Codelist {codelistId} Partial");
                Assert.AreEqual(expectedCodesNum, codelist.Items.Count, $"Codelist {codelistId} items");
            }
        }
    }
}
