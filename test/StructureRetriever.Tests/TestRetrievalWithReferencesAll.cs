// -----------------------------------------------------------------------
// <copyright file="TestRetrievalWithReferencesAll.cs" company="EUROSTAT">
//   Date Created : 2013-09-17
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace StructureRetriever.Tests
{
    using System.Linq;

    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Test unit for <see cref="IMutableStructureSearchManager"/>.
    /// Port from SR Java tests.
    /// </summary>
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class TestRetrievalWithReferencesAll : TestBase
    {
        private DataflowFilterContext _context;
        /// <summary>
        /// Initializes a new instance of the <see cref="TestBase"/> class.
        /// </summary>
        public TestRetrievalWithReferencesAll(string storeId)
            : base(storeId)
        {
        }

        [OneTimeSetUp] public void OneTimeSetUp()
        {
            _context = new DataflowFilterContext(Estat.Sri.MappingStoreRetrieval.Constants.DataflowFilter.Any);
        }

        [OneTimeTearDown] public void OneTimeTearDown()
        {
            _context.Dispose();

        }

        /// <summary>
        /// Test unit for <see cref="IMutableStructureSearchManager.GetMaintainables"/> 
        /// </summary>
        [Test]
        public void TestGetAllCategorisations()
        {
            var mutableStructureSearchManager = GetStructureSearchManager();
            var catRef = new StructureReferenceImpl(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation));
            var detailLevel = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.ReferencedStubs);
            var referenceLevel = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.All);

            var structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                .SetStructureIdentification(catRef)
                .SetRequestedDetail(detailLevel.ToComplex())
                .SetReferencedDetail(detailLevel.ToComplexReference())
                .SetReferences(referenceLevel)
                .Build();


            ISdmxObjects mutableObjects = mutableStructureSearchManager.GetMaintainables(structureQuery);
            Assert.IsTrue(mutableObjects.Dataflows.Count > 0);
            Assert.IsTrue(mutableObjects.DataStructures.Count > 0);
            Assert.IsTrue(mutableObjects.CategorySchemes.Count > 0);
            Assert.IsTrue(mutableObjects.Categorisations.Count > 0);
            Assert.IsTrue(mutableObjects.Codelists.Count > 0);
            Assert.IsTrue(mutableObjects.ConceptSchemes.Count > 0);
            Assert.IsTrue(mutableObjects.HierarchicalCodelists.Count == 0);
        }

        /// <summary>
        /// Test unit for <see cref="IMutableStructureSearchManager.GetMaintainables" />
        /// </summary>
        [Test]
        public void TestGetOneCategorisations()
        {
            var mutableStructureSearchManager = GetStructureSearchManager();
            var detailLevel = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full);
            var referenceLevel = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.All);

            IStructureReference catRef = new StructureReferenceImpl(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation));
            var getFirst = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                .SetStructureIdentification(catRef)
                .Build();
            var categorisation = mutableStructureSearchManager.GetMaintainables(getFirst).Categorisations.First();

            var referenceForOne = new StructureReferenceImpl(categorisation.AgencyId, categorisation.Id,categorisation.Version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Categorisation));
            var structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                .SetStructureIdentification(referenceForOne)
                .SetRequestedDetail(detailLevel.ToComplex())
                .SetReferencedDetail(detailLevel.ToComplexReference())
                .SetReferences(referenceLevel)
                .Build();

            var mutableObjects = mutableStructureSearchManager.GetMaintainables(structureQuery);

            Assert.IsTrue(mutableObjects.Dataflows.Count > 0);
            Assert.IsTrue(mutableObjects.DataStructures.Count > 0);
            Assert.IsTrue(mutableObjects.CategorySchemes.Count > 0);
            Assert.IsTrue(mutableObjects.Categorisations.Count > 0);
            Assert.IsTrue(mutableObjects.Codelists.Count > 0);
            Assert.IsTrue(mutableObjects.ConceptSchemes.Count > 0);
            Assert.IsTrue(mutableObjects.HierarchicalCodelists.Count == 0);
        }

        /// <summary>
        /// Test unit for <see cref="MappingStoreCommonSdmxObjectRetriever.GetMaintainables" />
        /// </summary>
        [Test]
        public void TestGetOneCatScheme()
        {
            var mutableStructureSearchManager = GetStructureSearchManager();
            var detailLevel = StructureQueryDetail.GetFromEnum(StructureQueryDetailEnumType.Full);
            var referenceLevel = StructureReferenceDetail.GetFromEnum(StructureReferenceDetailEnumType.All);

            IStructureReference getAllReference = new StructureReferenceImpl(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme));
            var allQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                .SetStructureIdentification(getAllReference)
                .Build();

            var objectToSearch = mutableStructureSearchManager.GetMaintainables(allQuery).CategorySchemes.First();

            var referenceForOne = new StructureReferenceImpl(objectToSearch.AgencyId, objectToSearch.Id, objectToSearch.Version, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CategoryScheme));
            var structureQuery = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                .SetStructureIdentification(referenceForOne)
                .SetRequestedDetail(detailLevel.ToComplex())
                .SetReferencedDetail(detailLevel.ToComplexReference())
                .SetReferences(referenceLevel)
                .Build();

            var mutableObjects = mutableStructureSearchManager.GetMaintainables(structureQuery);

            Assert.IsNotEmpty(mutableObjects.Dataflows, referenceForOne.ToString());
            // FIXME This should not fail
            Assert.IsEmpty(mutableObjects.DataStructures, referenceForOne.ToString());
            Assert.IsNotEmpty(mutableObjects.CategorySchemes, referenceForOne.ToString());
            Assert.IsNotEmpty(mutableObjects.Categorisations, referenceForOne.ToString());
            Assert.IsEmpty(mutableObjects.Codelists, referenceForOne.ToString());
            Assert.IsEmpty(mutableObjects.ConceptSchemes, referenceForOne.ToString());
            Assert.IsTrue(mutableObjects.HierarchicalCodelists.Count == 0, referenceForOne.ToString());
        }

        /// <summary>
        /// Gets the structure search manager.
        /// </summary>
        /// <returns></returns>
        private ICommonSdmxObjectRetrievalManager GetStructureSearchManager()
        {
            var settings = this.CreateSettings(SdmxSchemaEnumType.VersionTwoPointOne);
            return new MappingStoreCommonSdmxObjectRetriever(new Database(ConnectionStringSettings), settings);
        }
    }
}