// -----------------------------------------------------------------------
// <copyright file="AvailabilityTests.cs" company="EUROSTAT">
//   Date Created : 2023-04-28
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace StructureRetriever.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class AvailabilityTests : TestBase
    {
        private readonly SdmxObjectRetrievalManagerWrapper _sdmxObjectRetrievalManager;
        private readonly IAvailableDataManager _availableDataManager;

        public AvailabilityTests(string storeId) 
            : base(storeId)
        {
            StructureRetrieverSettings setings = CreateSettings(SdmxSchemaEnumType.VersionTwoPointOne);
            ICommonSdmxObjectRetrievalManager decorated = new MappingStoreCommonSdmxObjectRetriever(new Database(ConnectionStringSettings), setings);

            _sdmxObjectRetrievalManager = new SdmxObjectRetrievalManagerWrapper(decorated);
            _availableDataManager = new AvailableDataManager(setings);
        }

        // test context
        [TestCase("availability/dataflow/ESTAT/NAMAIN_IDC_N/+", 1)]
        [TestCase("availability/datastructure/ESTAT/NA_MAIN/1.9", 0)]
        [TestCase("availability/provisionagreement/ESTAT/no_name/+", 0, Ignore = "true", IgnoreReason = "not available structure for provision agreement")]
        // test wildcard
        [TestCase("availability/*/IT1/*/*", 6)]
        [TestCase("availability/dataflow/ESTAT/*/*", 28)]
        [TestCase("availability/datastructure/ESTAT/*/*", 0)]
        [TestCase("availability/provisionagreement/ESTAT/no_name/+", 0, Ignore = "true", IgnoreReason = "not available structure for provision agreement")]
        // multiple values
        [TestCase("availability/*/*/NAMAIN_IDC_N,NAMAIN_IDC_N_GROUP/+", 2)]
        public void TestAvailabilityFeatures(string restUrl, int ccCount)
        {
            var sdmxObjects = RetrieveAllAvailables(restUrl);

            Assert.IsNotNull(sdmxObjects);
            Assert.AreEqual(ccCount, sdmxObjects.ContentConstraintObjects.Count);
        }

        // these test cases are performed on mastore_test docker image database
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*.*.*.*.*.B1GQ?c[CONF_STATUS]=F", 3)]//test c-parameter combined old filter 
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[TIME_PERIOD]=ge:1994-Q3+le:1996-M09", 34)]// test time range special case
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[TIME_PERIOD]=ge:1994-Q3+le:1996-M09&c[CONF_STATUS]=F", 17)] // test time range with another c-parameter
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*.*.*.*.*.B1GQ?c[TIME_PERIOD]=ge:1994-Q3+le:1996-M09&c[CONF_STATUS]=F", 1)]// test all together
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[ACTIVITY]=BTE,_T", 8)]// test OR
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[OBS_VALUE]=ge:300+le:302", 34)]// ranged; note that actually string values
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[STO]=sw:D21", 8)]// test operator sw
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[STO]=ew:1", 16)]// test operator ew
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[STO]=ne:D21", 64)]// test operator ne
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[ACTIVITY]=co:T", 20)]// test operator co
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[ACTIVITY]=nc:T", 48)]// test operator nc
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[STO]=sw:D+co:3", 8)]// starts with D and contains 3
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[ACTIVITY]=sw:K,L", 8)]// starts either with K or L
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[ACTIVITY]=sw:K,ew:Z", 24)]// starts with K or ends with Z
        [TestCase("availability/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[STO]=B1GQ&c[CONF_STATUS]=F,BM", 3)]// two parameters
        public void TestParameter_c(string query, int numObsExpected)
        {
            var sdmxObjects = RetrieveAllAvailables(query);

            Assert.IsNotNull(sdmxObjects);
            // expecting one cc because there is one dataflow
            Assert.AreEqual(1, sdmxObjects.ContentConstraintObjects.Count);
            var cc = sdmxObjects.ContentConstraintObjects.Single();
            Assert.IsNotNull(cc);
            // num of observations are put in the metrics annotation
            int obsCount = Convert.ToInt32(cc.GetAnnotationsByType("sdmx_metrics").Single().Title);
            Assert.AreEqual(numObsExpected, obsCount);
        }


        private ISdmxObjects RetrieveAllAvailables(string restUrl)
        {
            var availabilityQuery = BuildRestAvailabilityQuery(restUrl);

            IMutableObjects mutableObjects = new MutableObjectsImpl();
            var flowRefIterator = availabilityQuery.StartFlowRefIterator();
            bool hasMultipleReferences = flowRefIterator.Count > 1;

            while (flowRefIterator.MoveNext())
            {
                try
                {
                    var dynamiccQuery = new AvailableConstraintQueryV2(availabilityQuery, _sdmxObjectRetrievalManager);
                    var wildcardIterator = dynamiccQuery.StartFlowRefIterator();
                    hasMultipleReferences |= wildcardIterator.Count > 1;
                    while (wildcardIterator.MoveNext())
                    {
                        try
                        {
                            mutableObjects = _availableDataManager.Retrieve(dynamiccQuery, mutableObjects, hasMultipleReferences);
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                    }
                }
                catch (SdmxNoResultsException)
                {
                    // wait for other structure references to end
                }
            }

            return mutableObjects.ImmutableObjects;
        }

        protected IRestAvailabilityQuery BuildRestAvailabilityQuery(string queryPath)
        {
            var parts = queryPath.Split('?');
            IDictionary<string, string> paramsDict = new Dictionary<string, string>();

            if (parts.Length == 2)
            {
                var queryParameters = parts[1].Split('&');
                foreach (string parameter in queryParameters)
                {
                    var pair = parameter.Split('=');
                    paramsDict.Add(pair[0], pair[1]);
                }
            }

            var query = new RestAvailabilityQuery(parts[0], paramsDict);
            return query;
        }
    }
}
