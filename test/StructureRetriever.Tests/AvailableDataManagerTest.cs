// -----------------------------------------------------------------------
// <copyright file="AvailableDataManagerTest.cs" company="EUROSTAT">
//   Date Created : 2018-04-24
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace StructureRetriever.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class AvailableDataManagerTest : TestBase
    {
        /// <summary>
        ///     The _from mutable.
        /// </summary>
        private static readonly StructureReferenceFromMutableBuilder _fromMutable = new StructureReferenceFromMutableBuilder();

        private readonly SdmxObjectRetrievalManagerWrapper _sdmxObjectRetrievalManager;
        private readonly IAvailableDataManager _availableDataManager;

        private static readonly ISdmxObjects _cacheSdmxObjects = new SdmxObjectsImpl();

        public AvailableDataManagerTest(string storeId)
            : base(storeId)
        {
            StructureRetrieverSettings setings = CreateSettings(SdmxSchemaEnumType.VersionTwoPointOne);

            ICommonSdmxObjectRetrievalManager decorated = new MappingStoreCommonSdmxObjectRetriever(new Database(ConnectionStringSettings), setings);
            _sdmxObjectRetrievalManager = new SdmxObjectRetrievalManagerWrapper(decorated);

            _availableDataManager = new AvailableDataManager(setings);
        }

        [TestCaseSource(nameof(GetDataflows))]
        public void GetPartialDataTest(string dataflowUrn)
        {
            var restQuery = BuildRestQuery(dataflowUrn);
            var query = new AvailableConstraintQuery(new RestAvailableConstraintQuery(restQuery, null), _sdmxObjectRetrievalManager);

            var result = _availableDataManager.Retrieve(query).ImmutableObjects;
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GetAllMaintainables().Count, Is.EqualTo(1));

            var dataflowObject = query.Dataflow;
            var dsd = query.DataStructure;
          
            AssetContentConstraint(query, result);
        }

        [TestCaseSource(nameof(GetDataflowsWithTime))]
        public void GetPartialDataTestTimePeriod(string dataflowUrn)
        {
            var restQuery = BuildRestQuery(dataflowUrn);
            var path = restQuery.Split('/');
            path[path.Length - 1] = "TIME_PERIOD";
            RestAvailableConstraintQuery restAvailableConstraintQuery = new RestAvailableConstraintQuery(string.Join("/", path), null);
            var query = new AvailableConstraintQuery(restAvailableConstraintQuery, _sdmxObjectRetrievalManager);

            var result = _availableDataManager.Retrieve(query).ImmutableObjects;
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GetAllMaintainables().Count, Is.EqualTo(1));

            var dataflowObject = query.Dataflow;
            AssetContentConstraint(query, result, 1);
        }

        private static void AssetContentConstraint(AvailableConstraintQuery query, ISdmxObjects result, int? numberOfExpectedKeyValues = null)
        {
            Assert.That(result.ContentConstraintObjects, Is.Not.Empty);
            Assert.That(result.ContentConstraintObjects.Count, Is.EqualTo(1));
            var contentContraint = result.GetContentConstraintObjects(new MaintainableRefObjectImpl(
                 "SDMX",
                 "CC",
                 "1.0")).Single();
            Assert.That(contentContraint.IncludedCubeRegion.KeyValues, Is.Not.Empty);
            if (numberOfExpectedKeyValues.HasValue)
            {
                Assert.That(contentContraint.IncludedCubeRegion.KeyValues.Count, Is.EqualTo(numberOfExpectedKeyValues.Value));
            }

            foreach (var component in query.Components)
            {
                var dimension = (IDimension)component;


                Assert.That(contentContraint.IsDefiningActualDataPresent, Is.True);
                IKeyValues keyValues = contentContraint.IncludedCubeRegion.KeyValues.FirstOrDefault(kv => kv.Id.Equals(dimension.Id, StringComparison.Ordinal));
                Assert.That(keyValues, Is.Not.Null);
                Assert.That(keyValues.CascadeValues, Is.Empty);
                if (dimension.TimeDimension)
                {
                    Assert.That(keyValues.TimeRange, Is.Not.Null);
                    Assert.That(keyValues.TimeRange.StartDate, Is.Not.Null);
                    Assert.That(keyValues.TimeRange.EndDate, Is.Not.Null);

                }
                else
                {
                    Assert.That(keyValues.TimeRange, Is.Null);
                    Assert.That(keyValues.Values, Is.Not.Empty);
                }
            }
        }

        [TestCaseSource(nameof(GetDataflowsWithTime))]
        public void GetPartialDataTestPeriod(string dataflowUrn)
        {
            var restQuery = BuildRestQuery(dataflowUrn);
            var queryParameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            queryParameters.Add("startPeriod", "1900");
            queryParameters.Add("endPeriod", "2100");
            RestAvailableConstraintQuery restQuery1 = new RestAvailableConstraintQuery(restQuery, queryParameters);
            var query = new AvailableConstraintQuery(restQuery1, _sdmxObjectRetrievalManager);

            var result = _availableDataManager.Retrieve(query).ImmutableObjects;
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GetAllMaintainables().Count, Is.EqualTo(1));

            var dataflowObject = query.Dataflow;
            var dsd = query.DataStructure;
            if (dsd.TimeDimension == null)
            {
                Assert.Ignore("DSD doesn't have a time dimension");
            }

            AssetContentConstraint(query, result);
        }

        private static IEnumerable<string> GetDataflows()
        {
            // TODO improve test cases with extra expected info
            yield return "ESTAT:CENSUSHUB_Q_XS1(1.0)";
            yield return "ESTAT:DEMOGRAPHY(2.3)";
            yield return "ESTAT:NAMAIN_IDC_N(1.9)";
            yield return "ESTAT:EGR_CORE_ENT(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_A(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_DT_M(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_DT_Q(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_M(2.0)";
            yield return "ESTAT:SSTSCONS_PROD_QT(2.0)";
            yield return "ESTAT:SSTSCONS_PROD_QT2(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_Q_TRANS(1.0)";
            yield return "ESTAT:SSTSCONS_SDMXRI_137(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_MT(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_MT2(1.0)";
            yield return "ESTAT:STSCONS_UPDATE_CONS_LAST(1.0)";
            yield return "ESTAT:STSCONS_UPDATE_FROM_DDB(1.0)";
            yield return "ESTAT:STSCONS_UPDATE_OFF(1.0)";
            yield return "ESTAT:STSCONS_UPDATE_STATUS_CONS(1.0)";
        }
        private static string BuildRestURL(IDataflowObject dataflow, IDataStructureObject dsd) 
        {
            return string.Format(
                    CultureInfo.InvariantCulture,
                    "{0}/{1},{2},{3}/ALL/ALL/{4}",
                    RestAvailableConstraintQuery.ResourceName,
                    dataflow.AgencyId,
                    dataflow.Id,
                    dataflow.Version,
                    string.Join("+", dsd.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension).Select(c => c.Id)));
        }

        private static IEnumerable<string> GetDataflowsWithTime()
        {
            yield return "ESTAT:DEMOGRAPHY(2.3)";
            yield return "ESTAT:NAMAIN_IDC_N(1.9)";
            yield return "ESTAT:SSTSCONS_PROD_A(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_DT_M(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_DT_Q(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_M(2.0)";
            yield return "ESTAT:SSTSCONS_PROD_QT(2.0)";
            yield return "ESTAT:SSTSCONS_PROD_QT2(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_Q_TRANS(1.0)";
            yield return "ESTAT:SSTSCONS_SDMXRI_137(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_MT(1.0)";
            yield return "ESTAT:SSTSCONS_PROD_MT2(1.0)";
            yield return "ESTAT:STSCONS_UPDATE_CONS_LAST(1.0)";
            yield return "ESTAT:STSCONS_UPDATE_FROM_DDB(1.0)";
            yield return "ESTAT:STSCONS_UPDATE_OFF(1.0)";
            yield return "ESTAT:STSCONS_UPDATE_STATUS_CONS(1.0)";
        }

        private static StructureReferenceImpl GetDataflowStructureReference(string dataflowUrn)
        {
            var structurRef =
                new StructureReferenceImpl(new Uri("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=" + dataflowUrn));
            return structurRef;
        }
        [TestCaseSource(nameof(GetDataflows))]
        public void GetPartialDataTestWithReferences(string dataflowUrn)
        {
            var restQuery = BuildRestQuery(dataflowUrn);
            var queryParameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            queryParameters.Add("references", "all");
            RestAvailableConstraintQuery restQuery1 = new RestAvailableConstraintQuery(restQuery, queryParameters);
            var query = new AvailableConstraintQuery(restQuery1, _sdmxObjectRetrievalManager);

            var result = _availableDataManager.Retrieve(query).ImmutableObjects;
            Assert.That(result, Is.Not.Null);
            Assert.That(result.GetAllMaintainables(), Is.Not.Empty);
            Assert.That(result.ContentConstraintObjects, Is.Not.Empty);
            var contentContraint = result.GetContentConstraintObjects(new MaintainableRefObjectImpl(
     "SDMX",
     "CC",
     "1.0")).First();
            var dataflowObject = query.Dataflow;
            var dsd = query.DataStructure;
            foreach (var component in query.Components)
            {
                var dimension = (IDimension)component;
                if (dimension.HasCodedRepresentation())
                {
                    var representation = dimension.Representation.Representation;

                    var cross = dsd as ICrossSectionalDataStructureObject;
                    if (cross != null)
                    {
                        representation = cross.GetCodelistForMeasureDimension(dimension.Id);
                    }

                    if (dimension.MeasureDimension && cross == null)
                    {
                        Assert.That(result.ConceptSchemes, Is.Not.Empty);
                        var conceptSchemeMutableObject = result.GetConceptSchemes(representation).First();
                        Assert.That(conceptSchemeMutableObject.Items, Is.Not.Empty);
                        Assert.That(conceptSchemeMutableObject.Partial, Is.True);
                    }
                    else
                    {
                        Assert.That(result.Codelists, Is.Not.Empty);
                        var codelistMutableObject = result.GetCodelists(representation).First();
                        Assert.That(codelistMutableObject.Items, Is.Not.Empty, $"Codelist Id: {codelistMutableObject.Id}, Component {component.Id}");
                        // if all codes are used it is not partial
//                        Assert.That(codelistMutableObject.Partial, Is.True);
                    }
                }

                var conceptIdentity = result.GetConceptSchemes(component.ConceptRef).First();
                Assert.That(conceptIdentity.GetItemById(component.ConceptRef.ChildReference.Id), Is.Not.Null);
            }
        }

        private string BuildRestQuery(string dataflowUrn)
        {
            var structureRef = GetDataflowStructureReference(dataflowUrn);
            var dataflow = _sdmxObjectRetrievalManager.GetMaintainableObject<IDataflowObject>(structureRef);
            Assert.That(dataflow, Is.Not.Null);
            var reqDsd = _sdmxObjectRetrievalManager
                .GetMaintainableObject<IDataStructureObject>(dataflow.DataStructureRef);
            Assert.That(reqDsd, Is.Not.Null);
            var restQuery = BuildRestURL(dataflow, reqDsd);
            return restQuery;
        }
    }
}