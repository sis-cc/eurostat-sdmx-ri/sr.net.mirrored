// -----------------------------------------------------------------------
// <copyright file="TestReferencePartial.cs" company="EUROSTAT">
//   Date Created : 2022-12-07
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace StructureRetriever.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;

    [TestFixture("sqlserver")]
    //[TestFixture("odp")]
    //[TestFixture("mysql")]
    public class TestReferencePartial : RetrieverTestBase
    {
        public TestReferencePartial(string storeId) 
            : base(storeId)
        {
        }

        /// <summary>
        /// Test the detail=referencepartial for the concept schemes.
        /// </summary>
        /// <param name="queryPath"></param>
        /// <param name="conceptSchemeId">The Id of the concept scheme to check</param>
        /// <param name="isPartial">defines whether is supposed to be partial (if not all items returned)</param>
        /// <param name="expectedConceptsNum">The expected number of items to be returned for the given concept scheme.</param>
        [TestCase("dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/1.0?detail=full&references=descendants", "CS_NA", false, 42)]
        [TestCase("dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/1.0?detail=referencepartial&references=descendants", "CS_NA", true, 32)]
        [TestCase("dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/1.0?detail=referencepartial&references=all", "CS_NA", true, 32)]
        [TestCase("dataflow/ESTAT/DEMOGRAPHY/2.3?detail=full&references=descendants", "COMPONENT_ROLES", false, 8)]
        [TestCase("dataflow/ESTAT/DEMOGRAPHY/2.3?detail=referencepartial&references=descendants", "COMPONENT_ROLES", true, 1)]
        [TestCase("dataflow/ESTAT/DEMOGRAPHY/2.3?detail=referencepartial&references=all", "COMPONENT_ROLES", true, 1)]
        [TestCase("dataflow/ESTAT/DEMOGRAPHY/2.3?detail=full&references=descendants", "DEMO_CONCEPTS", false, 27)]
        [TestCase("dataflow/ESTAT/DEMOGRAPHY/2.3?detail=referencepartial&references=descendants", "DEMO_CONCEPTS", false, 27)] // all are used
        public void TestConceptSchemes(string queryPath, string conceptSchemeId, bool isPartial, int expectedConceptsNum)
        {
            ICommonStructureQuery structureQuery = BuildStructureQuery(queryPath);
            ISdmxObjects sdmxObjects = RetrievalManager.GetMaintainables(structureQuery);

            var conceptScheme = sdmxObjects.ConceptSchemes.SingleOrDefault(s => s.Id.Equals(conceptSchemeId, StringComparison.Ordinal));
            Assert.IsNotNull(conceptScheme, $"Concept Scheme {conceptSchemeId} not returned");
            Assert.AreEqual(isPartial, conceptScheme.Partial, $"Concept Scheme {conceptSchemeId} Partial");
            Assert.AreEqual(expectedConceptsNum, conceptScheme.Items.Count, $"Concept Scheme {conceptSchemeId} items");
        }

        /// <summary>
        /// Test the detail=referencepartial for the category schemes.
        /// </summary>
        /// <param name="queryPath"></param>
        /// <param name="categorySchemeId">The Id of the category scheme to check</param>
        /// <param name="isPartial">defines whether is supposed to be partial (if not all items returned)</param>
        /// <param name="expectedCategoriesNum">The expected number of items to be returned for the given category scheme.</param>
        [TestCase("dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/1.0?detail=full&references=parentsandsiblings", "DDB", false, 10)]
        [TestCase("dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/1.0?detail=full&references=parentsandsiblings", "ESTAT_DATAFLOWS_SCHEME", false, 6)]
        [TestCase("dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/1.0?detail=referencepartial&references=parentsandsiblings", "DDB", true, 1)]
        [TestCase("dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/1.0?detail=referencepartial&references=parentsandsiblings", "ESTAT_DATAFLOWS_SCHEME", true, 1)]
        [TestCase("dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/1.0?detail=referencepartial&references=all", "DDB", true, 1)]
        [TestCase("dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/1.0?detail=referencepartial&references=all", "ESTAT_DATAFLOWS_SCHEME", true, 1)]
        public void TestCategorySchemes(string queryPath, string categorySchemeId, bool isPartial, int expectedCategoriesNum)
        {
            ICommonStructureQuery structureQuery = BuildStructureQuery(queryPath);
            ISdmxObjects sdmxObjects = RetrievalManager.GetMaintainables(structureQuery);

            var categoryScheme = sdmxObjects.CategorySchemes.SingleOrDefault(s => s.Id.Equals(categorySchemeId, StringComparison.Ordinal));
            Assert.IsNotNull(categoryScheme, $"Category Scheme {categorySchemeId} not returned");
            Assert.AreEqual(isPartial, categoryScheme.Partial, $"Category Scheme {categorySchemeId} Partial");
            Assert.AreEqual(expectedCategoriesNum, categoryScheme.Items.Count, $"Category Scheme {categorySchemeId} items");

            foreach (var categorisation in sdmxObjects.Categorisations)
            {
                if (string.Equals(categorisation.CategoryReference.MaintainableId, categoryScheme.Id, StringComparison.InvariantCulture))
                {
                    string[] identifiableIds = categorisation.CategoryReference.IdentifiableIds.ToArray();
                    Assert.IsNotNull(categoryScheme.GetCategory(identifiableIds), "Missing category : " + string.Join(".", identifiableIds));
                }
            }
        }

        [TestCase("dataflow/*/SSTSCONS_PROD_A/2.0?detail=referencepartial&references=all")]
        [TestCase("dataflow/ESTAT,TEST_AGENCY/SSTSCONS_PROD_A/2.0?detail=referencepartial&references=all")]
        [TestCase("dataflow/ESTAT/*/1.0?detail=referencepartial&references=all")]
        [TestCase("dataflow/ESTAT/NA_ESTAT_MAIN_T0101A,HC06/1.0?detail=referencepartial&references=all")]
        [TestCase("dataflow/ESTAT/DEMOGRAPHY/*?detail=referencepartial&references=all")]
        [TestCase("dataflow/ESTAT/DEMOGRAPHY/1.0,2.3?detail=referencepartial&references=all")]
        [TestCase("dataflow/ESTAT/DEMOGRAPHY/1+.0?detail=referencepartial&references=all")]
        [TestCase("dataflow/ESTAT/DEMOGRAPHY/1.0+.0?detail=referencepartial&references=all")]
        [TestCase("conceptscheme/ESTAT/CS_NA/1.0?detail=referencepartial&references=all")]
        public void TestNotImplemented(string queryPath)
        {
            ICommonStructureQuery structureQuery = BuildStructureQuery(queryPath);
            Assert.Throws<SdmxNotImplementedException>(() => RetrievalManager.GetMaintainables(structureQuery));
        }
    }
}
