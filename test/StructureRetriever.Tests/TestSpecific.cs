// -----------------------------------------------------------------------
// <copyright file="TestSpecific.cs" company="EUROSTAT">
//   Date Created : 2014-06-10
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace StructureRetriever.Tests
{
    using System.Collections.Generic;

    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class TestSpecific : TestBase
    {
        /// <summary>
        ///     The _log.
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(TestSpecific));

        /// <summary>
        /// Initializes a new instance of the <see cref="TestSpecific"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public TestSpecific(string name) : base(name)
        {
        }

        [TestCase(SdmxStructureEnumType.CodeList)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Categorisation)]
        public void TestQueryCodelistUsedByDsd(SdmxStructureEnumType sdmxStructure)
        {
            var structureSearchManager = GetStructureSearchManager();
            var specificStructureReference = SdmxStructureType.GetFromEnum(sdmxStructure);
            foreach (var codelistMutableObject in this.GetCodelistUsedByDsd())
            {
                IStructureReference codelistRef = codelistMutableObject.AsReference;
                var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                    .SetStructureIdentification(codelistRef)
                    .SetRequestedDetail(ComplexStructureQueryDetailEnumType.Full)
                    .SetReferencedDetail(ComplexMaintainableQueryDetailEnumType.Full)
                    .SetReferences(StructureReferenceDetailEnumType.Specific)
                    .SetSpecificReferences(specificStructureReference)
                    .Build();


                var mutableObjects = structureSearchManager.GetMaintainables(structureQuery);
                Assert.IsNotEmpty(mutableObjects.GetMaintainables(specificStructureReference), codelistRef.ToString());
            }
        }

        [TestCase(SdmxStructureEnumType.CodeList)]
        [TestCase(SdmxStructureEnumType.ConceptScheme)]
        [TestCase(SdmxStructureEnumType.Dataflow)]
        [TestCase(SdmxStructureEnumType.Dsd)]
        [TestCase(SdmxStructureEnumType.CategoryScheme)]
        [TestCase(SdmxStructureEnumType.Categorisation)]
        public void TestQueryCodelistUsedByDsdStub(SdmxStructureEnumType sdmxStructure)
        {
            var structureSearchManager = this.GetStructureSearchManager();
            var specificStructureReference = SdmxStructureType.GetFromEnum(sdmxStructure);
            foreach (var codelistMutableObject in this.GetCodelistUsedByDsd())
            {
                IStructureReference codelistRef = codelistMutableObject.AsReference;
                var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                    .SetStructureIdentification(codelistRef)
                    .SetRequestedDetail(ComplexStructureQueryDetailEnumType.Stub)
                    .SetReferencedDetail(ComplexMaintainableQueryDetailEnumType.Stub)
                    .SetReferences(StructureReferenceDetailEnumType.Specific)
                    .SetSpecificReferences(specificStructureReference)
                    .Build();

                var mutableObjects = structureSearchManager.GetMaintainables(structureQuery);
                Assert.IsNotEmpty(mutableObjects.GetMaintainables(specificStructureReference));
            }
        }

        private ISet<ICodelistObject> GetCodelistUsedByDsd()
        {
            var structureSearchManager = this.GetStructureSearchManager();
            var structureQuery = CommonStructureQueryCore.Builder
                    .NewQuery(CommonStructureQueryType.Other, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                    .SetMaintainableTarget(SdmxStructureEnumType.Categorisation)
                    .SetRequestedDetail(ComplexStructureQueryDetailEnumType.Stub)
                    .SetReferencedDetail(ComplexMaintainableQueryDetailEnumType.Stub)
                    .SetReferences(StructureReferenceDetailEnumType.Specific)
                    .SetSpecificReferences(SdmxStructureEnumType.CodeList)
                    .Build();

            var mutableObjects = structureSearchManager.GetMaintainables(structureQuery);
            return mutableObjects.Codelists;
        }

        private ICommonSdmxObjectRetrievalManager GetStructureSearchManager()
        {
            var settings = this.CreateSettings(SdmxSchemaEnumType.VersionTwoPointOne);
            return new MappingStoreCommonSdmxObjectRetriever(new Database(ConnectionStringSettings), settings);
        }
    }
}