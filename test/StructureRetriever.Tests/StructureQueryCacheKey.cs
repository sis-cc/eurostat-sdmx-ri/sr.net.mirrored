using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using Estat.Sri.StructureRetriever.Cache;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;

namespace StructureRetriever.Tests
{
    [TestFixture]
    public class StructureQueryCacheKeyTests
    {

        [Test]
        public void ShouldGenerateTheSameHashCode()
        {
            var first = new StructureQueryCacheKey(new[] { "IT1"},new[] { "Test" }, new[] {new VersionRequestCore("1.1")}, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList));
            var second = new StructureQueryCacheKey(new[] { "IT1" }, new[] { "Test" }, new[] { new VersionRequestCore("1.1") }, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList)); ;

            Assert.AreEqual(first.GetHashCode(), second.GetHashCode()); 
        }

        [Test]
        public void ShouldFindItemWithTheSameHashCode()
        {
            var first = new StructureQueryCacheKey(new[] { "IT1" }, new[] { "Test" }, new[] { new VersionRequestCore("1.1") }, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList));
            var second = new StructureQueryCacheKey(new[] { "IT1" }, new[] { "Test" }, new[] { new VersionRequestCore("1.1") }, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList)); ;

            var hashSet = new HashSet<StructureQueryCacheKey>();
            hashSet.Add(first);
            Assert.IsTrue(hashSet.Contains(second));
        }
    }
}
