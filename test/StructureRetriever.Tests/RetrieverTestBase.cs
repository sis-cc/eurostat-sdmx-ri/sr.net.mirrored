// -----------------------------------------------------------------------
// <copyright file="RetrieverTestBase.cs" company="EUROSTAT">
//   Date Created : 2022-12-07
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace StructureRetriever.Tests
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Model.Error;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Builder.Common;
    using Org.Sdmxsource.XmlHelper;
    using Estat.Sdmxsource.Extension.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;
    using Estat.Sri.MappingStore.Store.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Estat.Nsi.StructureRetriever.Manager;

    public abstract class RetrieverTestBase : TestBase
    {
        protected readonly ICommonSdmxObjectRetrievalManager RetrievalManager;
        private readonly IReadableDataLocationFactory _readableDataLocationFactory = new ReadableDataLocationFactory();
        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();
        private readonly IStructureSubmitterEngine _submitterEngine;

        protected RetrieverTestBase(string storeId) 
            : base(storeId)
        {
            RetrievalManager = new MappingStoreCommonSdmxObjectRetriever(GetRetrievalEngineContainer());
            var submitFactory = new StructureSubmitMappingStoreFactory((storeId) => ConnectionStringSettings);
            _submitterEngine = submitFactory.GetEngine(storeId);
        }

        protected void InsertStructures(string xmlFileName, SdmxSchemaEnumType sdmxSchema)
        {
            ISdmxObjects sdmxObjects;
            FileInfo fileInfo = new FileInfo(xmlFileName);
            using (IReadableDataLocation rdl = _readableDataLocationFactory.GetReadableDataLocation(fileInfo))
            {
                IStructureWorkspace workspace = _parsingManager.ParseStructures(rdl);

                sdmxObjects = workspace.GetStructureObjects(false);

                XMLParser.ValidateXml(rdl, sdmxSchema);
            }
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = _submitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }
        }

        protected static ICommonStructureQuery BuildStructureQuery(string queryPath)
        {
            var parts = queryPath.Split('?');
            var queryString = parts[0].Split('/');
            string structure = queryString[0];
            string agencyId = queryString[1];
            string resourceId = queryString[2];
            string version = queryString[3];

            IDictionary<string, string> paramsDict = new Dictionary<string, string>();
            if (parts.Count() == 2)
            {
                var queryParameters = parts[1].Split('&');
                foreach (string parameter in queryParameters)
                {
                    var pair = parameter.Split('=');
                    paramsDict.Add(pair[0], pair[1]);
                }
            }
            var restStructureQueryParams = new RestStructureQueryParams(null, structure, agencyId, resourceId, version, null, paramsDict);
            return new RestV2CommonStructureQueryBuilder().BuildCommonStructureQuery(restStructureQueryParams);
        }
    }
}
