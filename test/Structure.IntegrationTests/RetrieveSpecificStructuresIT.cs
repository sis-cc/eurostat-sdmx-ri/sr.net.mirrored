// -----------------------------------------------------------------------
// <copyright file="RetrieveSpecificStructuresIT.cs" company="EUROSTAT">
//   Date Created : 2022-06-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Structure.IntegrationTests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    //[TestFixture("odp")]
    //[TestFixture("mysql")]
    [TestFixture("sqlserver")]
    public class RetrieveSpecificStructuresIT : IntegrationTestBase
    {
        public RetrieveSpecificStructuresIT(string storeId)
            : base(storeId)
        {          
        }

        [TestCaseSource(nameof(RetrieveSpecificTestCases))]
        public void TestRetrieveSpecificStructures(
            string filename, SdmxSchemaEnumType sdmxSchema, IStructureReference structureReference, 
            SdmxStructureEnumType specificStructureType, List<IStructureReference> expectedReferences)
        {
            TestInsertRetrieveArtefacts(filename, sdmxSchema, structureReference, expectedReferences, 
                () => GetSpecificMaintainables(structureReference, specificStructureType));
        }

        private static IEnumerable<object> RetrieveSpecificTestCases()
        {
            return new List<object>()
            {
                // DSD related
                new object[] {
                    "tests/v21/dsd_NA_MAIN_1.6.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TEST_AGENCY123:NA_MAIN(1.6)"),
                    SdmxStructureEnumType.ConceptScheme,
                    GetDsdWithConcepts()
                },
                new object[] {
                    "tests/v21/dsd_NA_MAIN_1.6.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=TEST_AGENCY123:CS_NA(1.6)"),
                    SdmxStructureEnumType.Dsd,
                    GetConceptWithDsd()
                },
               new object[] {
                    "tests/v21/dsd_NA_MAIN_1.6.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_STD123:CL_CONF_STATUS(1.0)"),
                    SdmxStructureEnumType.Dsd,
                    GetCodelistsWithDsd()
                },
                new object[] {
                    "tests/v21/dsd_NA_MAIN_1.6.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_STD123:CL_CONF_STATUS(1.0)"),
                    SdmxStructureEnumType.ConceptScheme,
                    GetCodelistsWithConcepts()
                },
                new object[] {
                    "tests/v21/categorisations-and-descendants.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=TEST_AGENCY_3:SSTSCONS_PROD_M(2.0)"),
                    SdmxStructureEnumType.ConceptScheme,
                    GetDataflowWithConcepts()
                },
                new object[] {
                    "tests/v21/categorisations-and-descendants.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=TEST_AGENCY_3:ESTAT_DATAFLOWS_SCHEME(1.1)"),
                    SdmxStructureEnumType.Categorisation,
                    GetCategorisationFromCategoryScheme()
                }
            };
        }

        #region create expected references

        private static List<IStructureReference> GetDsdWithConcepts()
        {
            List<IStructureReference> expected = new List<IStructureReference>();

            // DSD
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TEST_AGENCY123:NA_MAIN(1.6)"));

            // CONCEPT_SCHEME
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=TEST_AGENCY123:CS_NA(1.6)"));

            return expected;
        }

        private static List<IStructureReference> GetConceptWithDsd()
        {
            List<IStructureReference> expected = new List<IStructureReference>();

            // DSD
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TEST_AGENCY123:NA_MAIN(1.6)"));

            // CONCEPT_SCHEME
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=TEST_AGENCY123:CS_NA(1.6)"));

            return expected;
        }

        private static List<IStructureReference> GetCodelistsWithDsd()
        {
            List<IStructureReference> expected = new List<IStructureReference>();

            // DSD
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TEST_AGENCY123:NA_MAIN(1.6)"));


            // CODELISTS
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_STD123:CL_CONF_STATUS(1.0)"));

            return expected;
        }

        private static List<IStructureReference> GetCodelistsWithConcepts()
        {
            List<IStructureReference> expected = new List<IStructureReference>();

            // CONCEPT_SCHEME
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=TEST_AGENCY123:CS_NA(1.6)"));

            // CODELISTS
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_STD123:CL_CONF_STATUS(1.0)"));


            return expected;
        }

        private static List<IStructureReference> GetDataflowWithConcepts()
        {
            List<IStructureReference> expected = new List<IStructureReference>();

            // CONCEPT_SCHEME
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=TEST_AGENCY_3:STS_SCHEME(1.0)"));

            // DATAFLOW
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=TEST_AGENCY_3:SSTSCONS_PROD_M(2.0)"));

            return expected;
        }

        private static List<IStructureReference> GetCategorisationFromCategoryScheme()
        {
            List<IStructureReference> expected = new List<IStructureReference>();

            // CategoryScheme
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=TEST_AGENCY_3:ESTAT_DATAFLOWS_SCHEME(1.1)"));

            // Categorisations
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.categoryscheme.Categorisation=TEST_AGENCY_3:TEST1(1.0)"));
            expected.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.categoryscheme.Categorisation=TEST_AGENCY_3:TEST2(1.0)"));

            return expected;
        }

        #endregion
    }
}
