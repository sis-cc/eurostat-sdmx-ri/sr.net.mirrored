// -----------------------------------------------------------------------
// <copyright file="TestBase.cs" company="EUROSTAT">
//   Date Created : 2022-07-18
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Structure.IntegrationTests
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using DryIoc;

    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Plugin.SqlServer.Builder;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.Util.ResourceBundle;
    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// The test base.
    /// </summary>
    public abstract class TestBase
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(TestBase));
       
        /// <summary>
        /// The container
        /// </summary>
        private readonly Container _container;

        private readonly IMappingStoreManager _mappingStoreManager;

        private readonly IConfigurationStoreManager _connectionStringManager;

        private readonly ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestBase"/> class.
        /// </summary>
        public TestBase(string storeId)
        {
            // this is needed to include a human readable description in SdmxSource exceptions
            SdmxException.SetMessageResolver(new MessageDecoder());
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(ConfigurationManager.AppSettings["MappingStoreRetrieversFactory"]);

            try
            {
                StoreId = storeId;
                _container =
                    new Container(
                        rules =>
                        rules.With(FactoryMethod.ConstructorWithResolvableArguments)
                            .WithoutThrowOnRegisteringDisposableTransient());
                string[] folders = { "Estat.Sri.Plugin.SqlServer", "Estat.Sri.Plugin.Oracle", "Estat.Sri.Plugin.Mysql" };

                var sqlServerPluginAssembly = typeof(SqlServerConnectionBuilder).Assembly;
                var mySqlPluginAssembly = typeof(Estat.Sri.Plugin.MySql.Engine.MySqlDatabaseProviderEngine).Assembly;
                var oraclePluginAssembly = typeof(Estat.Sri.Plugin.Oracle.Builder.OracleSettingsBuilder).Assembly;
                var assemblies = new List<Assembly>()
                {
                    sqlServerPluginAssembly,
                    mySqlPluginAssembly,
                    oraclePluginAssembly
                };

                assemblies.AddRange(new[] { typeof(IDatabaseProviderManager).Assembly, typeof(DatabaseManager).Assembly });
                _container.RegisterMany(assemblies
                    ,
                    type => !typeof(IEntity).IsAssignableFrom(type));
                //_container.Register<IEntityAuthorizationManager, StubAuthorizationManager>();
                _container.Unregister<IEntityAuthorizationManager>();
                MappingStoreIoc.Register<RetrievalEngineContainerFactory>("MappingStoreRetrieversFactory");
                _container.Register<IStructureParsingManager, StructureParsingManager>(
                    made: Made.Of(() => new StructureParsingManager()));
                _container.Register<IStructureWriterFactory, SdmxStructureWriterFactory>();
                _container.Register<IStructureWriterManager, StructureWriterManager>();
                _container.Register<IReadableDataLocationFactory, ReadableDataLocationFactory>();
                _mappingStoreManager = _container.Resolve<IMappingStoreManager>();
                _connectionStringManager = _container.Resolve<IConfigurationStoreManager>();
                _connectionStringSettings = _connectionStringManager.GetSettings<ConnectionStringSettings>().FirstOrDefault(stringSettings => stringSettings.Name == storeId);
            }
            catch (Exception e)
            {
                _log.Error(e);


                Trace.WriteLine(e);
                throw;
            }
        }


        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>
        /// The store identifier.
        /// </value>
        public string StoreId { get; set; }

        /// <summary>
        /// Gets the containers
        /// </summary>
        /// <value>
        /// </value>
        public Container IoCContainer
        {
            get
            {
                return _container;
            }
        }

        public ConnectionStringSettings ConnectionStringSettings
        {
            get
            {
                return _connectionStringSettings;
            }
        }

        /// <summary>
        /// Creates the settings.
        /// </summary>
        /// <param name="sdmxSchemaEnumType">Type of the SDMX schema enum.</param>
        /// <returns></returns>
        protected StructureRetrieverSettings CreateSettings(SdmxSchemaEnumType sdmxSchemaEnumType)
        {
            StructureRetrieverSettings settings = new StructureRetrieverSettings();
            settings.SdmxSchemaVersion = sdmxSchemaEnumType;
            settings.StoreId = StoreId;
            settings.MappingManager = this.IoCContainer.Resolve<IComponentMappingManager>();
           // settings.ComponentMappingValidationManager = this.IoCContainer.Resolve<IComponentMappingValidationManager>();
            settings.SpecialRetrievalManager = new SpecialMutableObjectRetrievalManager(_connectionStringSettings);
            settings.ConnectionBuilder = this.IoCContainer.Resolve<IBuilder<DbConnection, DdbConnectionEntity>>();
            settings.ConnectionStringSettings = _connectionStringSettings;
            return settings;
        }

        protected void InitializeMappingStore()
        {
            var engineByStoreId = this._mappingStoreManager.GetEngineByStoreId(StoreId);
            var actionResult = engineByStoreId.Initialize(new DatabaseIdentificationOptions() { StoreId = StoreId });
            Assert.That(actionResult.Status, Is.EqualTo(StatusType.Success), string.Join(", ", actionResult.Messages));
        }
    }
}