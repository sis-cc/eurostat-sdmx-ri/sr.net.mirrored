// -----------------------------------------------------------------------
// <copyright file="RetrieveParentStructuresIT.cs" company="EUROSTAT">
//   Date Created : 2022-06-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Structure.IntegrationTests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    //[TestFixture("odp")]
    //[TestFixture("mysql")]
    [TestFixture("sqlserver")]
    public class RetrieveParentStructuresIT : IntegrationTestBase
    {
        public RetrieveParentStructuresIT(string storeId)
            : base(storeId)
        {
        }

        [TestCaseSource(nameof(RetrieveParentsTestCases))]
        public void TestRetrieveParents(
            string filename, SdmxSchemaEnumType sdmxSchema, 
            IStructureReference structureReference, List<IStructureReference> expectedReferences)
        {
            TestInsertRetrieveArtefacts(filename, sdmxSchema, structureReference, expectedReferences,
                () => GetMaintainables(StructureReferenceDetailEnumType.Parents, structureReference));
        }

        private static IEnumerable<object> RetrieveParentsTestCases()
        {
            return new List<object>()
            {
                // Codelists
                new object[] {
                    "tests/v21/dsd_NA_MAIN_1.6.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY123:CL_ACTIVITY_TEST_CODELIST(1.3)"),
                    new List<IStructureReference>  { 
                        new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TEST_AGENCY123:NA_MAIN(1.6)"), 
                        new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=TEST_AGENCY123:CS_NA(1.6)") }
                },
                // DSD
                new object[] {
                    "tests/v21/dataflow_ESTAT+STS+2.0.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS_TEST(2.0)"),
                    new List<IStructureReference>  { 
                        new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M_TEST(2.0)") }
                }
            };
        }
    }
}
