// -----------------------------------------------------------------------
// <copyright file="PerformanceImportRetrieveIT.cs" company="EUROSTAT">
//   Date Created : 2022-06-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Structure.IntegrationTests
{
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Estat.Sdmxsource.Extension.Constant;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using System.Diagnostics;

    //[TestFixture("odp")]
    //[TestFixture("mysql")]
    [TestFixture("sqlserver")]
    public class PerformanceImportRetrieveIT : IntegrationTestBase
    {
        private static Dictionary<int, ICodelistObject> testCodelists = new Dictionary<int, ICodelistObject>();

        public PerformanceImportRetrieveIT(string storeId)
            : base(storeId)
        {
        }

        [TestCaseSource(nameof(CodeListTestParams))]
        public void TestPerformanceImportRetrieve(ICodelistObject codelist)
        {
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            sdmxObjects.AddCodelist(codelist);

            // want to measure time taken
            Stopwatch stopwatch = new Stopwatch();

            // delete if already in msdb
            stopwatch.Start();
            Delete(sdmxObjects);
            TestContext.WriteLine("{0} - 1st delete time: {1}", codelist.Id, stopwatch.Elapsed);

            // import
            stopwatch.Restart();
            var result = Import(sdmxObjects);
            foreach (IResponseWithStatusObject response in result)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.Select(m => m.Text))));
            }
            TestContext.WriteLine("{0} - import and assert time: {1}", codelist.Id, stopwatch.Elapsed);

            // retrieve
            stopwatch.Restart();
            ISdmxObjects retrievedObjects = GetMaintainables(StructureReferenceDetailEnumType.None, codelist.AsReference);
            TestContext.WriteLine("{0} - retrieve time: {1}", codelist.Id, stopwatch.Elapsed);

            // validate
            stopwatch.Restart();
            var codelists = retrievedObjects.GetCodelists(codelist.AsReference);
            Assert.AreEqual(1, codelists.Count);
            Assert.IsTrue(codelists.First().Partial); // important for performance see CodelistBeanImpl#validate
            AssertSdmxObjectsAfterWriteRead(sdmxObjects, retrievedObjects, true);
            TestContext.WriteLine("{0} - assert codelist time: {1}", codelist.Id, stopwatch.Elapsed);

            // delete existing codelist
            stopwatch.Restart();
            Delete(retrievedObjects);
            TestContext.WriteLine("{0} - Final delete time: {1}", codelist.Id, stopwatch.Elapsed);

            stopwatch.Stop();
        }

        private static IEnumerable<object> CodeListTestParams()
        {
            testCodelists.Add(1, BuildCodelist(1));
            testCodelists.Add(1_000, BuildCodelist(1_000));
            testCodelists.Add(10_000, BuildCodelist(10_000));
            testCodelists.Add(100_000, BuildCodelist(100_000));
            //testCodelists.Add(1_000_000, BuildCodelist(1_000_000));
            return testCodelists.Values.ToList<object>();
        }

        private static ICodelistObject BuildCodelist(int codeCount)
        {
            ICodelistMutableObject cl = new CodelistMutableCore();
            cl.Id = "CL_PERF" + codeCount;
            cl.AgencyId = "TEST_AGENCY_PER";
            cl.Version = "1." + codeCount;
            cl.IsPartial = true; // TO avoid extra validation
            cl.AddName("en", "Test codelist build from PerformanceImportRetrieveIT");

            for (int i = 0; i < codeCount; i++)
            {
                ICodeMutableObject code = new CodeMutableCore();
                code.Id = "CD" + i;
                if (i > 10 && (i % 4) == 0)
                {
                    code.ParentCode = "CD" + (i - 10);
                }
                code.AddName("en", "Code " + i);
                code.AddName("de", "Kode " + i);
                cl.AddItem(code);
            }

            return cl.ImmutableInstance;
        }
    }
}
