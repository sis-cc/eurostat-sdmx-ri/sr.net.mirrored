// -----------------------------------------------------------------------
// <copyright file="DeadlockStructuresIT.cs" company="EUROSTAT">
//   Date Created : 2022-06-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Structure.IntegrationTests
{
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using Estat.Sdmxsource.Extension.Constant;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    [TestFixture("odp_scratch")]
    //[TestFixture("mysql_scratch")]
    [TestFixture("sqlserver_scratch")]
    public class DeadlockStructuresIT : IntegrationTestBase
    {
        public DeadlockStructuresIT(string storeId)
            : base(storeId)
        {
        }

        [OneTimeSetUp] 
        public void OneTimeSetUp()
        {
            InitializeMappingStore();
        }


        [Parallelizable()]
        [TestCaseSource(nameof(ImportStructuresTestParams))]
        public void ImportStructuresTestCases(string filename, SdmxSchemaEnumType sdmxSchema, IStructureReference structureReference,
            bool testReferences, bool ignoreAgencySchemes)
        {
            TestImportRetrieveDelete(filename, sdmxSchema, structureReference, testReferences, ignoreAgencySchemes);
        }

        private void TestImportRetrieveDelete(
           string filename, SdmxSchemaEnumType sdmxSchema, IStructureReference structureReference,
           bool testReferences, bool ignoreAgencySchemes)
        {
            ISdmxObjects sdmxObjects = ReadStructures(filename, sdmxSchema);

            // delete those already exist in database
            Delete(sdmxObjects);

            // test import
            // it should not exist before importing it
            Assert.AreEqual(((SdmxObjectsImpl)GetMaintainables(StructureReferenceDetailEnumType.None, structureReference)).AllMaintainables.Count, 0);
            var importResults = Import(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                // we allow warnings as we import the same artefact in parallel
                Assert.AreNotEqual(ResponseStatus.Failure, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // test retrieving structures
            ISdmxObjects retrievedObjects;
            if (testReferences)
            {
                // with children/parents
                retrievedObjects = GetMaintainables(StructureReferenceDetailEnumType.All, structureReference);
                //AssertSdmxObjectsAfterWriteRead(sdmxObjects, retrievedObjects, ignoreAgencySchemes);
            }
            else
            {
                // only structure reference
                retrievedObjects = GetMaintainables(StructureReferenceDetailEnumType.None, structureReference);
                IMaintainableObject expectedObject = sdmxObjects.GetAllMaintainables().FirstOrDefault(m => m.Urn.Equals(structureReference.TargetUrn));
                IMaintainableObject retrievedObject = retrievedObjects.GetAllMaintainables().FirstOrDefault(m => m.Urn.Equals(structureReference.TargetUrn));
                Assert.IsTrue(expectedObject.DeepEquals(retrievedObject, true),
                    string.Format("comparison failed between expected {0} and actual {1} ", expectedObject, retrievedObject));
            }

            // test delete
            ISdmxObjects maintainablesToDelete = GetMaintainables(StructureReferenceDetailEnumType.None, structureReference);
            // check it exists before deleting it
            Assert.NotNull(maintainablesToDelete, "maintainable to delete does not exist");
            var deleteResults = Delete(maintainablesToDelete);
            foreach (IResponseWithStatusObject response in deleteResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when deleting structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.Select(m => m.Text))));
            }
            // try to retrieve it
            Assert.AreEqual(((SdmxObjectsImpl)GetMaintainables(StructureReferenceDetailEnumType.None, structureReference)).AllMaintainables.Count, 0);

            // make sure to leave the db clean
            Delete(sdmxObjects);
        }

        private static IEnumerable<object> ImportStructuresTestParams()
        {
            return new List<object>()
            {
                // Codelists
                //TODO add v3.0 sample
                new object[]{ "tests/v21/codelist_ECB_CL_CURRENCY_notfinal.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_CURRENCY(1.0)"), true, true },
                // Concept Schemes
                new object[] { "tests/v20/CS_M_INDEX.xml", SdmxSchemaEnumType.VersionTwo,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=ABS:CS_M_INDEX(1.0.0)"), true, true },
                new object[] {"tests/v21/concept_scheme.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=TEST_AGENCY123:TESTCS_CS_NA(1.6)"), true, true },
                // DSDs
                //TODO we use 0_withoutValidFromTo because there is bug with valid from/to dates:
                //retrieved dates have 2 hours difference from imported ones. Check if it is fixed in sdmx 3
                //TODO check why this test fails even after removing the valid from/to dates
                //The following test case uses update artefact
                //new object [] { "tests/v21/dsd_ESTAT+STS+2.0_withoutValidFromTo.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                //    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS_DSDTEST(2.0)"), false, true },
                new object[] { "tests/v21/dsd_NA_MAIN_1.6_NO_UNUSED_CONCEPTS.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TEST_AGENCY123:NA_MAIN(1.6)"), true, true },
                // Category schemes
                new object[] {"tests/v21/sdmxv2.1-ESTAT+STS+2.0-categories.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=ESTAT:TEST_CATEGORY_SCHEME(1.1)"), true, true },
                // Dataflows
                new object[] {"tests/v21/dataflow_ESTAT+STS+2.0.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M_TEST(2.0)"), false, true },
                // metadatastructure
                //TODO add v3.0 sample
                new object[] { "tests/v21/MSDv21-TEST2_Presentational.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure=TEST:TEST_MSD_1(1.0)"), false, true },
                new object[] { "tests/v21/MSDv21-TEST2.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.metadatastructure.MetadataStructure=TEST:TEST_MSD-MSDB7(1.0)"), false, true },
                // content constraint
                //TODO add v3.0 sample
                new object[] {"tests/v21/contentconstraint_stubs_actual.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("TEST", "CR_ACTUAL_TEST1", "1.7", SdmxStructureEnumType.ContentConstraint), false, true },
                new object[] { "tests/v21/contentconstraint_stubs_allowed.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("TEST", "CR_ALLOWED_TEST1", "1.7", SdmxStructureEnumType.ContentConstraint), false, true },
                // categorisation
                new object[] { "tests/v21/categorisations-and-descendants.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.categoryscheme.Categorisation=TEST_AGENCY_3:TEST1(1.0)"), false, true },
                // provision agreement
                new object[] { "tests/v21/test-pa.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.registry.ProvisionAgreement=TEST:TEST_PA(1.0)"), false, true },
                // data provider
                new object[] { "tests/v21/DP+ESTAT+1.0.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.base.DataProviderScheme=ESTAT:DATA_PROVIDERS(1.0)"), false, true },
                // agency scheme
                new object[] { "tests/v21/AGENCIES+ESTAT+1.0.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.base.AgencyScheme=ESTAT:AGENCIES(1.0)"), false, true },
                // structure set
                new object[] { "tests/v21/dsd_NA_MAIN_1.6_withStructureSet.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.mapping.StructureSet=TEST:TEST_SS(1.2)"), false, true },
                // metadataflow
                new object[] { "tests/v21/MetadataflowWithMSDv21-TEST2.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.metadatastructure.Metadataflow=ESTAT:TEST_MDF(1.5)"), false, true },
                // data consumer
                new object[] { "tests/v21/DC+ESTAT+1.0.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("ESTAT", "DATA_CONSUMERS", "1.0", SdmxStructureEnumType.DataConsumerScheme), false, true }
            };
        }
    }
}
