// -----------------------------------------------------------------------
// <copyright file="MockRetrieverManager.cs" company="EUROSTAT">
//   Date Created : 2023-03-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Structure.IntegrationTests
{
    using System;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;

    /// <summary>
    /// <see cref="IRetrieverManager"/> implementation for testing purposes.
    /// Simplifies the methods of RetrieverManager in nsiws
    /// </summary>
    internal class MockRetrieverManager : IRetrieverManager
    {
        private readonly ICommonSdmxObjectRetrievalManager _structureRetrievalManager;
        private readonly IAvailableDataManager _availableDataManager;
        private readonly ISdmxObjectRetrievalManager _availabletRetrievalManager;

        public MockRetrieverManager(ICommonSdmxObjectRetrievalManager retrievalManager, IAvailableDataManager availableDataManager)
        {
            _structureRetrievalManager = retrievalManager;
            _availableDataManager = availableDataManager;
            _availabletRetrievalManager = new SdmxObjectRetrievalManagerWrapper(retrievalManager);
        }

        #region not implemented

        public IDataResponse<IDataWriterEngine> GetDataResponseComplex(IReadableDataLocation input)
        {
            throw new NotImplementedException();
        }

        public IDataResponse<ICrossSectionalWriterEngine> GetDataResponseCross(IReadableDataLocation input)
        {
            throw new NotImplementedException();
        }

        public IDataResponse<ICrossSectionalWriterEngine> GetDataResponseCross(DataRequest request)
        {
            throw new NotImplementedException();
        }

        public IDataResponse<IDataWriterEngine> GetDataResponseSimple(IReadableDataLocation input)
        {
            throw new NotImplementedException();
        }

        public IDataResponse<IDataWriterEngine> GetDataResponseSimple(DataRequest request)
        {
            throw new NotImplementedException();
        }

        public DataRequest ParseDataRequest(IRestDataQuery dataQuery)
        {
            throw new NotImplementedException();
        }

        public ISdmxObjects ParseRequest(SdmxSchemaEnumType sdmxSchema, IReadableDataLocation input)
        {
            throw new NotImplementedException();
        }

        public ISdmxObjects ParseRequest(SdmxSchema sdmxSchema, IRestStructureQuery query)
        {
            throw new NotImplementedException();
        }

        #endregion

        public ISdmxObjects ParseRequest(ICommonStructureQuery query)
        {
            return _structureRetrievalManager.GetMaintainables(query);
        }

        public ISdmxObjects ParseRequest(SdmxSchema sdmxSchema, IRestAvailableConstraintQuery query)
        {
            var dynamiccQuery = new AvailableConstraintQuery(query, _availabletRetrievalManager);
            var mutableObjects = _availableDataManager.Retrieve(dynamiccQuery);
            return mutableObjects.ImmutableObjects;
        }
    }
}
