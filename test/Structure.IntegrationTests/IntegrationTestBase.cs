// -----------------------------------------------------------------------
// <copyright file="IntegrationTestBase.cs" company="EUROSTAT">
//   Date Created : 2022-08-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Structure.IntegrationTests
{
    using System.Collections.Generic;
    using System.Linq;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Common;
    using System.IO;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.XmlHelper;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Common;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Util.Io;
    using Estat.Sri.MappingStore.Store.Factory;
    using Estat.Sdmxsource.Extension.Engine;
    using log4net;
    using System;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Estat.Sdmxsource.Extension.Constant;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using DryIoc;
    using System.Diagnostics;

    public abstract class IntegrationTestBase : TestBase
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(IntegrationTestBase));

        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();
        private readonly IReadableDataLocationFactory _readableDataLocationFactory = new ReadableDataLocationFactory();
        private readonly IStructureSubmitterEngine _submitterEngine;
        private readonly ICommonSdmxObjectRetrievalManager _retrievalManager;

        protected IntegrationTestBase(string storeId)
            : base(storeId)
        {
            try
            {
                Database database = new Database(ConnectionStringSettings);
                var container = new RetrievalEngineContainer(database);
                IoCContainer.Register<ICommonSdmxObjectRetrievalManager, MappingStoreCommonSdmxObjectRetriever>(
                    Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep,
                    made: Made.Of(() => new MappingStoreCommonSdmxObjectRetriever(container)));

                var submitFactory = new StructureSubmitMappingStoreFactory((storeId) => ConnectionStringSettings);
                _submitterEngine = submitFactory.GetEngine(storeId);
                _retrievalManager = IoCContainer.Resolve<ICommonSdmxObjectRetrievalManager>();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Trace.WriteLine(ex);
                throw;
            }
        }

        /// <summary>
        /// Imports artefacts from file that should contain a structure with references 
        /// and tests if the artefact with its references is retrieved successfully.
        /// The test file may contain other structures too apart from the test structure and its references, 
        /// therefore the test structure reference and the expected references need to be explicitly provided parameters.
        /// </summary>
        /// <param name="filename">filename a file that should contain a structure with references</param>
        /// <param name="sdmxSchema">the schema version that the file is using.</param>
        /// <param name="structureReference">the structure that we want to test retrieving its references</param>
        /// <param name="expectedReferences">
        /// expected structure references of the tested structure:
        ///  -  If a list of structures is provided, then these structures are expected to be returned as references.
        ///  -  If null/empty is provided, then all artefacts contained in the test xml file are considered to be expected.
        ///     Therefore in that case the test file should only contain the test structure and its references and no other structures.
        /// </param>
        /// <param name="retrieveMethod">The method to test the retrieval of artefacts.</param>
        protected void TestInsertRetrieveArtefacts(
            string filename, SdmxSchemaEnumType sdmxSchema, IStructureReference structureReference,
            List<IStructureReference> expectedReferences, Func<ISdmxObjects> retrieveMethod)
        {
            ISdmxObjects sdmxObjects = ReadStructures(filename, sdmxSchema);

            // delete those already exist in database
            Delete(sdmxObjects);

            // test import
            // it should not exist before importing it
            Assert.AreEqual(((SdmxObjectsImpl)GetMaintainables(StructureReferenceDetailEnumType.None, structureReference)).AllMaintainables.Count, 0);
            var importResults = Import(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }

            // evaluate results
            // expected objects are the test structure reference and its references
            ISdmxObjects expectedObjects = new SdmxObjectsImpl();

            // if no expected references are specified, we assume the test file contains only the test structure and its references
            if (!expectedReferences.Any())
            {
                // if expected references are specified, then expected beans are the test structure and the specified references
                expectedObjects = sdmxObjects;
            }
            else
            {
                IMaintainableObject testObject = sdmxObjects.GetAllMaintainables().FirstOrDefault(m => m.Urn.Equals(structureReference.TargetUrn));
                expectedObjects.AddIdentifiable(testObject);
                IEnumerable<IMaintainableObject> maintainablesFromSdmxObjects = sdmxObjects.GetAllMaintainables()
                    .Where(m => expectedReferences.Select(r => r.TargetUrn).Contains(m.Urn));
                //make sure all requested maintainables are returned
                if (maintainablesFromSdmxObjects.Count() != expectedReferences.Count)
                {
                    throw new SdmxException("could not find all maintainables in SdmxObjects.");
                }
                expectedObjects.AddIdentifiables(maintainablesFromSdmxObjects);
            }

            //Retrieve test structureReference bean and its references
            ISdmxObjects retrievedObjectWithReferences = retrieveMethod.Invoke();

            AssertSdmxObjectsAfterWriteRead(expectedObjects, retrievedObjectWithReferences, true);

            // make sure to leave the db clean
            Delete(sdmxObjects);
        }

        protected void AssertSdmxObjectsAfterWriteRead(ISdmxObjects expectedSdmxObjects, ISdmxObjects actualSdmxObjects, bool ignoreAgencySchemes)
        {
            // Get expected and actual structures so that we can do assertions
            // We sort them by id to make sure they are in same position in both lists when comparing them later

            // NOTE the retrieved beans do not contain agency schemes, we need to explicitly retrieve them.
            // For assertion not to fail, ignore agency schemes from comparison
            // specifically for agency scheme test we need to change this so that it does not ignore it
            IList<IMaintainableObject> expectedMaintainableObjects;
            if (ignoreAgencySchemes)
            {
                expectedMaintainableObjects = expectedSdmxObjects.GetAllMaintainables()
                    .Where(m => m.StructureType.EnumType != SdmxStructureEnumType.AgencyScheme)
                    .OrderBy(m => m.Id).ToList();
            }
            else
            {
                expectedMaintainableObjects = expectedSdmxObjects.GetAllMaintainables()
                    .OrderBy(m => m.Id).ToList();
            }
            IList<IMaintainableObject> actualMaintainableObjects = actualSdmxObjects.GetAllMaintainables().OrderBy(m => m.Id).ToList();

            Assert.AreEqual(expectedMaintainableObjects.Count, actualMaintainableObjects.Count);
            for (int i = 0; i < expectedMaintainableObjects.Count; i++)
            {
                Assert.IsTrue(expectedMaintainableObjects[i].DeepEquals(actualMaintainableObjects[i], true),
                    string.Format("comparison failed between expected {0} and actual {1} ", expectedMaintainableObjects[i].Urn, actualMaintainableObjects[i].Urn));
            }
        }

        protected ISdmxObjects GetMaintainables(StructureReferenceDetailEnumType detailLevel, IStructureReference structureReference)
        {
            ICommonStructureQuery structureQuery = GetQueryFull(detailLevel, structureReference);
            return _retrievalManager.GetMaintainables(structureQuery);
        }

        protected ISdmxObjects GetSpecificMaintainables(IStructureReference structureReference, params SdmxStructureEnumType[] specificTypes)
        {
            ICommonStructureQuery structureQuery = GetQuerySpecificFull(structureReference, specificTypes);
            return _retrievalManager.GetMaintainables(structureQuery);
        }

        /// <summary>
        /// Imports the structures in the <paramref name="xmlFileName"/>
        /// </summary>
        /// <param name="xmlFileName"></param>
        /// <param name="schemaVersion"></param>
        /// <returns>The objects inserted</returns>
        protected ISdmxObjects ImportStructures(string xmlFileName, SdmxSchemaEnumType schemaVersion)
        {
            var sdmxObjects = ReadStructures(xmlFileName, schemaVersion);
            var importResults = Import(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }
            return sdmxObjects;
        }

        protected IList<IResponseWithStatusObject> Import(ISdmxObjects sdmxObjects)
        {
            _log.Info("importing structures " + sdmxObjects);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            return _submitterEngine.SubmitStructures(sdmxObjects);
        }

        protected IList<IResponseWithStatusObject> Delete(ISdmxObjects sdmxObjects)
        {
            _log.Info("deleting structures " + sdmxObjects);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Delete);
            return _submitterEngine.SubmitStructures(sdmxObjects);
        }

        protected static ICommonStructureQuery GetQueryFull(StructureReferenceDetailEnumType detail, IStructureReference structureReference)
        {
            var builder = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.REST, StructureOutputFormatEnumType.SdmxV3StructureDocument)
                .SetRequestedDetail(ComplexStructureQueryDetailEnumType.Full)
                .SetReferencedDetail(ComplexMaintainableQueryDetailEnumType.Full)
                .SetReferences(StructureReferenceDetail.GetFromEnum(detail))
                .SetMaintainableTarget(structureReference.MaintainableStructureEnumType);

            if (structureReference.HasVersion())
            {
                var versionRequests = structureReference.Version.Split(',')
                    .Select(v => new VersionRequestCore(v)).Cast<IVersionRequest>().ToArray();
                builder.SetVersionRequests(versionRequests);
            }
            if (structureReference.HasMaintainableId())
            {
                builder.SetMaintainableIds(structureReference.MaintainableId.Split(","));
            }
            if (structureReference.HasAgencyId())
            {
                builder.SetAgencyIds(structureReference.AgencyId.Split(","));
            }

            return builder.Build();
        }

        protected static ICommonStructureQuery GetQuerySpecificFull(IStructureReference structureReference, SdmxStructureEnumType[] specificTypes)
        {

            var builder = CommonStructureQueryCore.Builder
                .NewQuery(CommonStructureQueryType.REST, StructureOutputFormatEnumType.SdmxV3StructureDocument)
                .SetRequestedDetail(ComplexStructureQueryDetailEnumType.Full)
                .SetReferencedDetail(ComplexMaintainableQueryDetailEnumType.Full)
                .SetSpecificReferences(specificTypes)
                .SetMaintainableTarget(structureReference.MaintainableStructureEnumType)
                .SetReferences(StructureReferenceDetailEnumType.Specific);

            if (structureReference.HasVersion())
            {
                var versionRequests = structureReference.Version.Split(',')
                    .Select(v => new VersionRequestCore(v)).Cast<IVersionRequest>().ToArray();
                builder.SetVersionRequests(versionRequests);
            }
            if (structureReference.HasMaintainableId())
            {
                builder.SetMaintainableIds(structureReference.MaintainableId.Split(","));
            }
            if (structureReference.HasAgencyId())
            {
                builder.SetAgencyIds(structureReference.AgencyId.Split(","));
            }

            return builder.Build();
        }

        protected ISdmxObjects ReadStructures(string filepath, SdmxSchemaEnumType sdmxSchema)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            using (IReadableDataLocation rdl = _readableDataLocationFactory.GetReadableDataLocation(fileInfo))
            {
                IStructureWorkspace workspace = _parsingManager.ParseStructures(rdl);

                ISdmxObjects structureBeans = workspace.GetStructureObjects(false);

                XMLParser.ValidateXml(rdl, sdmxSchema);

                return structureBeans;
            }
        }
    }
}
