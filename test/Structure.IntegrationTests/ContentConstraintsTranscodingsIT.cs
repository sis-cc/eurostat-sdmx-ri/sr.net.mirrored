// -----------------------------------------------------------------------
// <copyright file="ContentConstraintsTranscodingsIT.cs" company="EUROSTAT">
//   Date Created : 2023-03-29
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Structure.IntegrationTests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Manager;
    using DryIoc;
    using Estat.Sdmxsource.Extension.Manager;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Engine;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sri.MappingStore.Store.Factory;
    using log4net;
    using System;
    using System.Diagnostics;
    using System.Configuration;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Estat.Sri.Mapping.Api.Constant;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using DryIoc.ImTools;
    using Estat.Sdmxsource.Extension.Constant;

    [TestFixture("odp_scratch")]
    [TestFixture("mysql_scratch")]
    [TestFixture("sqlserver_scratch")]
    public class ContentConstraintsTranscodingsIT : IntegrationTestBase
    {
        private long _datasetPK;
        private long _mappingSetPK;
        private IStructureReference _dataflowRef;
        private readonly List<ComponentMappingEntity> _componentMappings = new List<ComponentMappingEntity>();

        #region test setup

        private static readonly ILog _log = LogManager.GetLogger(typeof(ContentConstraintsTranscodingsIT));

        private readonly IEntityPersistenceManager _entityPersistenceManager;
        private readonly IEntityRetrieverManager _entityRetrieverManager;
        private readonly IConstraintsFromTranscodingsEngine _constraintsFromTranscodingsEngine;
        private readonly IRulesFromConstraintEngine _rulesFromConstraintEngine;

        public ContentConstraintsTranscodingsIT(string storeId)
            : base(storeId)
        {
            try
            {
                var configurationStoreManager = IoCContainer.Resolve<IConfigurationStoreManager>();
                Func<string, ConnectionStringSettings> connectionStringBuilder = (x) =>
                     configurationStoreManager.GetSettings<ConnectionStringSettings>().First(s => s.Name.Equals(x, StringComparison.InvariantCulture));
                IoCContainer.Register<IStructureSubmitFactory, StructureSubmitMappingStoreFactory>(
                    made: Made.Of(() => new StructureSubmitMappingStoreFactory(connectionStringBuilder)));
                IoCContainer.Register<IStructureSubmitter, StructureSubmitter>(reuse: Reuse.Singleton);
                IoCContainer.Register<IRetrieverManager, MockRetrieverManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
                StructureRetrieverSettings settings = CreateSettings(SdmxSchemaEnumType.VersionTwoPointOne);
                IoCContainer.Register<IAvailableDataManager, AvailableDataManager>(
                    made: Made.Of(() => new AvailableDataManager(settings)));

                _constraintsFromTranscodingsEngine = IoCContainer.Resolve<IConstraintsFromTranscodingsEngine>();
                _rulesFromConstraintEngine = IoCContainer.Resolve<IRulesFromConstraintEngine>();
                _entityPersistenceManager = IoCContainer.Resolve<IEntityPersistenceManager>();
                _entityRetrieverManager = IoCContainer.Resolve<IEntityRetrieverManager>();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                Trace.WriteLine(ex);
                throw;
            }
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            InitializeMappingStore();
            var sdmxObjects = ImportStructures(@"tests/v21/DSD_OECD_STS.xml", SdmxSchemaEnumType.VersionTwoPointOne);
            _dataflowRef = sdmxObjects.Dataflows.Single().AsReference;
            AddMappings(_dataflowRef.MaintainableUrn.AbsoluteUri);
        }

        [SetUp]
        public void SetUp()
        {
            DeleteAnyContentConstraints();
            DeleteAnyTranscodingRules();
        }

        private void AddMappings(string dataflowUrn)
        {
            DdbConnectionEntity ddbConnectionEntity = new DdbConnectionEntity()
            {
                Name = "STS 2.0 connection",
                Description = "DDB connection for new ESTAT.STS.2.0 datastructure",
                DbType = "SqlServer",
                DbUser = "ddb",
                Password = "123",
                AdoConnString = "Data Source=test-ddb-mssql;Initial Catalog=DDB_B_TEST_JANUARY2014;Integrated Security=False;User ID=ddb;Password=123"
            };
            long ddbConnectionPK = _entityPersistenceManager.GetEngine<DdbConnectionEntity>(StoreId).Add(ddbConnectionEntity);
            Assert.NotNull(ddbConnectionPK);

            var datasetEntity = new DatasetEntity()
            {
                Name = "STS 2.0 dataset",
                Description = "Dataset for new ESTAT.STS.2.0 datastructure",
                Query = "select * from ESA_MAIN_TOT_ESTAT_10",
                EditorType = "org.estat.ma.gui.userControls.dataset.QueryEditor.CustomQueryEditor, MappingAssistant",
                ParentId = ddbConnectionPK.ToString()
            };
            _datasetPK = _entityPersistenceManager.GetEngine<DatasetEntity>(StoreId).Add(datasetEntity);
            Assert.NotNull(_datasetPK);

            var mappingSetEntity = new MappingSetEntity()
            {
                Name = "STS 2.0 mapping set",
                Description = "Mapping set for new ESTAT.STS.2.0 datastructure",
                DataSetId = _datasetPK.ToString(),
                ParentId = dataflowUrn
            };
            _mappingSetPK = _entityPersistenceManager.GetEngine<MappingSetEntity>(StoreId).Add(mappingSetEntity);
            Assert.NotNull(_mappingSetPK);

            // add component mappings
            List<ComponentMappingEntity> componentMappings = new List<ComponentMappingEntity>
            {
                // component id, dataset column name(s)
                // dimensions
                CreateTestComponentMappingEntity("FREQ", "FREQ"),
                CreateTestComponentMappingEntity("REFERENCE_AREA", "REF_AREA"),
                CreateTestComponentMappingEntity("ADJUSTMENT", "ADJUSTMENT"),
                CreateTestComponentMappingEntity("ACTIVITY", "ACTIVITY"),
                CreateTestComponentMappingEntity("EXPENDITURE", "EXPENDITURE"),
                CreateTestComponentMappingEntity("UNIT_MEASURE", "UNIT_MEASURE"),
                CreateTestComponentMappingEntity("SUBJECT", "TITLE"),
                // attributes
                CreateTestComponentMappingEntity("DOMAIN", "REF_SECTOR"), // mandatory, dimension
                // measures
                CreateTestComponentMappingEntity("OBS_VALUE", "OBS_VALUE")
            };
            var entitiesWithPK = _entityPersistenceManager.AddEntities(componentMappings);
            Assert.AreEqual(componentMappings.Count, entitiesWithPK.Count(), "required component mappings");
            _componentMappings.AddRange(entitiesWithPK);

            // special case - time mapping
            // time dimension is TIME_PERIOD
            var timeMapping = new TimeDimensionMappingEntity()
            {
                EntityId = _mappingSetPK.ToString(),
                StoreId = this.StoreId,
                SimpleMappingColumn = "REF_PERIOD"
            };
            var timeMappingWithPK = _entityPersistenceManager.Add(timeMapping);
            Assert.IsNotNull(timeMappingWithPK);
        }

        #endregion

        [TestCase(true)]
        [TestCase(false)]
        public void TestCreatingContentConstraintsFromTranscodingRules_all(bool isActual)
        {
            // add transcoding rules
            // no need for real data
            var usedMappings = _componentMappings.Take(2).ToList();
            var usedTranscodings = CreateTestTranscodingRules(usedMappings);

            // create content constraints
            bool isNewEntity = _constraintsFromTranscodingsEngine.CreateOrUpdateFor(StoreId, _mappingSetPK.ToString(), isActual);
            Assert.IsTrue(isNewEntity, "new entity");

            // validate cc
            var cc = RetrieveAndValidateContentConstaints(isActual, usedMappings.Count);

            // validate each keyvalue
            for (int i = 0; i < cc.IncludedCubeRegion.KeyValues.Count; i++)
            {
                string expectedComponentId = usedMappings[i].Component.ObjectId;
                var keyValue = cc.IncludedCubeRegion.KeyValues[i];
                Assert.AreEqual(expectedComponentId, keyValue.Id, "component id");
                Assert.AreEqual(3, keyValue.Values.Count, "constraint values count");
                var transcodingIds = usedTranscodings
                    .Where(t => t.ParentId.Equals(usedMappings[i].EntityId, StringComparison.InvariantCulture))
                    .Select(t => t.DsdCodeEntity.ObjectId);
                CollectionAssert.AreEquivalent(transcodingIds, keyValue.Values, "constrained codes");
            }
        }

        [TestCase(true, "REFERENCE_AREA")]
        [TestCase(false, "REFERENCE_AREA")]
        public void TestCreatingContentConstraintsFromTranscodingRules_specific(bool isActual, string componentId)
        {
            // add transcoding rules
            // no need for real data
            var usedMappings = _componentMappings.Take(2).ToList();
            var usedTranscodings = CreateTestTranscodingRules(usedMappings);

            // create content constraints
            bool isNewEntity = _constraintsFromTranscodingsEngine.CreateOrUpdateFor(StoreId, _mappingSetPK.ToString(), isActual, componentId);
            Assert.IsTrue(isNewEntity, "new entity");

            // validate cc
            var cc = RetrieveAndValidateContentConstaints(isActual, 1);

            // validate each keyvalue
            // should expect one key
            var keyValue = cc.IncludedCubeRegion.KeyValues.Single();
            Assert.AreEqual(componentId, keyValue.Id, "component id");
            Assert.AreEqual(3, keyValue.Values.Count, "constraint values count");
            string mappingId = usedMappings
                .Single(m => m.Component.ObjectId.Equals(componentId, StringComparison.InvariantCulture)).EntityId;
            var transcodingIds = usedTranscodings
                .Where(t => t.ParentId.Equals(mappingId, StringComparison.InvariantCulture))
                .Select(t => t.DsdCodeEntity.ObjectId);
            CollectionAssert.AreEquivalent(transcodingIds, keyValue.Values, "constrained codes");
        }

        [Test]
        public void TestCreatingContentConstraintsFromTranscodingRules_timePeriod()
        {
            // create simple time transcoding 
            // other tests could also be created...
            // but the main objective of these tests is to check that the application is wired correctly 
            // so that it works as it used to work for msdb 6.x

            DataSetColumnEntity yearColumn = new()
            {
                ParentId = _datasetPK.ToString(),
                StoreId = StoreId,
                Description = "Workaround",
                Name = "REF_PERIOD"
            };
            TimeTranscodingEntity timeTranscodingEntity = new()
            {
                Frequency = TimeFormat.GetFromEnum(TimeFormatEnumType.Year).FrequencyCode,
                Year = new TimeTranscoding
                {
                    Column = yearColumn,
                    Start = 0,
                    Length = 4
                }
            };
            TimeDimensionMappingEntity timeDimensionMapping = new()
            {
                EntityId = _mappingSetPK.ToString(),
                StoreId = StoreId
            };
            timeDimensionMapping.ConvertFromTranscoding(new[] { timeTranscodingEntity }, "FREQ");

            // add time transcoding
            var timeMappingWithPk = _entityPersistenceManager.Add(timeDimensionMapping);
            Assert.IsNotNull(timeMappingWithPk, "new time mapping with transcoding");

            // create cc
            bool isActual = true;
            bool isNewEntity = _constraintsFromTranscodingsEngine.CreateOrUpdateFor(StoreId, _mappingSetPK.ToString(), isActual);
            Assert.IsTrue(isNewEntity, "new entity");

            // validate cc
            var cc = RetrieveAndValidateContentConstaints(isActual, 1);
            var timeRange = cc.IncludedCubeRegion.KeyValues.Single().TimeRange;
            Assert.IsNotNull(timeRange, "time range constraint");
            Assert.IsTrue(timeRange.Range);
            Assert.IsNotNull(timeRange.StartDate);
            Assert.AreEqual(1993, timeRange.StartDate.Date.Year);
            Assert.NotNull(timeRange.EndDate);
            Assert.AreEqual(1996, timeRange.EndDate.Date.Year);
        }

        [TestCase(false)]
        [TestCase(true)]
       // [Ignore("Fails due to reported bug SDMX2-4490")]
        public void TestCreatingTranscodingRulesFromContentConstraints(bool withExistingTranscodings)
        {
            // import content constraints
            var sdmxObjects = ImportStructures(@"tests/v21/DSD_OECD_STS_constraints.xml", SdmxSchemaEnumType.VersionTwoPointOne);
            var ccObjs = sdmxObjects.ContentConstraintObjects;

            // these are the values that should be converted to transcodings
            // we only do included cube regions
            // and NOT time range values
            var componentValues = ccObjs
                .SelectMany(cc => cc.IncludedCubeRegion.KeyValues)
                .Where(kv => kv.TimeRange == null)
                .GroupBy(kv => kv.Id)
                .ToDictionary(g => g.Key, g => g.SelectMany(v => v.Values));

            // add transcodings (should be deleted when creating new rules)
            if (withExistingTranscodings)
            {
                var useMappings = _componentMappings.Where(m => componentValues.ContainsKey(m.Component.ObjectId)).ToList();
                CreateTestTranscodingRules(useMappings);
            }

            // create transcoding rules
            foreach (var ccRef in ccObjs)
            {
                _rulesFromConstraintEngine.CreateRules(StoreId, _mappingSetPK.ToString(), ccRef.AgencyId, ccRef.Id, ccRef.Version);
            }

            // retrieve mappings
            EntityQuery mappingQuery = new()
            {
                ParentId = new Criteria<string>(OperatorType.Exact, _mappingSetPK.ToString()),
                EntityType = EntityType.Mapping
            };
            var mappings = _entityRetrieverManager.GetEntities<ComponentMappingEntity>(StoreId, mappingQuery, Detail.Full);

            // retrieve transcodings and assert
            foreach (var mapping in mappings)
            {
                bool hasValues = componentValues.TryGetValue(mapping.Component.ObjectId, out IEnumerable<string> values);
                bool hasTranscodings = mapping.HasTranscoding();
                Assert.AreEqual(hasValues, hasTranscodings);
                if (hasTranscodings)
                {
                    EntityQuery transcodingQuery = new()
                    {
                        ParentId = new Criteria<string>(OperatorType.Exact, mapping.EntityId),
                        EntityType = EntityType.TranscodingRule
                    };
                    var transcodingRules = _entityRetrieverManager.GetEntities<TranscodingRuleEntity>(StoreId, transcodingQuery, Detail.Full);

                    Assert.AreEqual(values.Count(), transcodingRules.Count(), $"count for {mapping.Component.ObjectId}");
                    CollectionAssert.AreEqual(values.OrderBy(s => s), 
                        transcodingRules.SelectMany(r => r.LocalCodes.Select(l => l.ObjectId)).OrderBy(s => s),
                        $"local values for {mapping.Component.ObjectId}");
                    CollectionAssert.AreEqual(values.OrderBy(s => s), 
                        transcodingRules.SelectMany(r => r.DsdCodeEntities.Select(c => c.ObjectId)).OrderBy(s => s),
                        $"sdmx values for {mapping.Component.ObjectId}");
                }
            }
        }

        [Test]
        public void TestImportDuplicateConstraints()
        {
            // import content constraints
            var sdmxObjects = ReadStructures(@"tests/v21/duplicate_constraints.xml", SdmxSchemaEnumType.VersionTwoPointOne);
            var importResults = Import(sdmxObjects);
            Assert.AreEqual(4, importResults.Count);
            for (int i = 0; i < importResults.Count; i++)
            {
                var res = importResults[i];
                if(i == 0 || i == 2)
                {
                    Assert.AreEqual(ResponseStatus.Success, res.Status, $"{i}: {res.StructureReference.MaintainableId}");
                }
                else
                {
                    Assert.AreEqual(ResponseStatus.Failure, res.Status, $"{i}: {res.StructureReference.MaintainableId}");
                    var errors = importResults[i].Messages.SelectMany(m => m.Text).Select(t => t.Value);
                    Assert.That(errors.Any(e => e.Contains("There is already a content constraint for dimension(s)", StringComparison.InvariantCulture)));
                }
            }
        }

        #region helper methods

        private List<TranscodingRuleEntity> CreateTestTranscodingRules(List<ComponentMappingEntity> useMappings)
        {
            var transcodingRules = new List<TranscodingRuleEntity>();
            foreach (var componentMapping in useMappings)
            {
                int index = _componentMappings.IndexOf(componentMapping);
                // local value, sdmx codes
                var transcodings = new Dictionary<string, List<string>>()
                {
                    { $"ANY_VAL_{index}1", new(){ $"CODE_{index}1" } },
                    { $"ANY_VAL_{index}2", new(){ $"CODE_{index}2" } },
                    { $"ANY_VAL_{index}3", new(){ $"CODE_{index}3" } }
                };
                transcodingRules.AddRange(
                    AddTranscodingRules(transcodings, componentMapping.GetColumns().First().Name, componentMapping.EntityId));
            }
            return transcodingRules;
        }

        private void DeleteAnyContentConstraints()
        {
            var contentConstraints = GetMaintainables(StructureReferenceDetailEnumType.Parents, _dataflowRef).ContentConstraintObjects;
            var objectsToDelete = new SdmxObjectsImpl(DatasetActionEnumType.Delete);
            objectsToDelete.AddIdentifiables(contentConstraints);
            Delete(objectsToDelete);
        }

        private void DeleteAnyTranscodingRules()
        {
            foreach (var mapping in _componentMappings)
            {
                _entityPersistenceManager.GetEngine<TranscodingRuleEntity>(StoreId)
                    .DeleteChildren(mapping.EntityId, EntityType.TranscodingRule);
            }
        }

        private IContentConstraintObject RetrieveAndValidateContentConstaints(bool isActual, int expectedConstraintNo)
        {
            ISdmxObjects retrievedObjects = GetMaintainables(StructureReferenceDetailEnumType.Parents, _dataflowRef);
            Assert.IsTrue(retrievedObjects.HasContentConstraintBeans, "retrieved content constraints");
            Assert.AreEqual(1, retrievedObjects.ContentConstraintObjects.Count, "content constraints count");
            var cc = retrievedObjects.ContentConstraintObjects.Single();
            Assert.AreEqual(isActual, cc.IsDefiningActualDataPresent, "actual or allowed");
            Assert.IsNotNull(cc.IncludedCubeRegion, "included cube region");
            Assert.AreEqual(expectedConstraintNo, cc.IncludedCubeRegion.KeyValues.Count, "key-values count");

            return cc;
        }

        private ComponentMappingEntity CreateTestComponentMappingEntity(string componentName, params string[] datasetColumnNames)
        {
            var mappingEntity = new ComponentMappingEntity()
            {
                Name = componentName,
                ParentId = _mappingSetPK.ToString(),
                Type = "test",
                Component = new Estat.Sri.Mapping.Api.Model.Component() { ObjectId = componentName },
                StoreId = this.StoreId
            };
            foreach (string columnName in datasetColumnNames)
            {
                mappingEntity.AddColumn(new DataSetColumnEntity() { Name = columnName });
            }
            return mappingEntity;
        }

        private IEnumerable<TranscodingRuleEntity> AddTranscodingRules(Dictionary<string, List<string>> transcodingRules, string ddbColumn, string parentId)
        {
            foreach (var rule in transcodingRules)
            {
                var transcodingRule = new TranscodingRuleEntity()
                {
                    ParentId = parentId,
                    StoreId = this.StoreId
                };
                transcodingRule.LocalCodes.Add(new LocalCodeEntity()
                {
                    ObjectId = rule.Key,
                    ParentId = ddbColumn
                });
                foreach (string sdmxCode in rule.Value)
                {
                    transcodingRule.DsdCodeEntities.Add(new IdentifiableEntity() { ObjectId = sdmxCode });
                }
                var ruleWithPK = _entityPersistenceManager.Add(transcodingRule);
                Assert.IsNotNull(ruleWithPK);
                yield return ruleWithPK;
            }
        }

        #endregion
    }
}
