// -----------------------------------------------------------------------
// <copyright file="RetrieveDescendantStructuresIT.cs" company="EUROSTAT">
//   Date Created : 2022-06-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Structure.IntegrationTests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    //[TestFixture("odp")]
    //[TestFixture("mysql")]
    [TestFixture("sqlserver")]
    public class RetrieveDescendantStructuresIT : IntegrationTestBase
    {
        public RetrieveDescendantStructuresIT(string storeId)
            : base(storeId)
        {
        }

        [TestCaseSource(nameof(RetrieveDescendantsTestCases))]
        public void TestRetrieveDescendants(
            string filename, SdmxSchemaEnumType sdmxSchema, 
            IStructureReference structureReference, List<IStructureReference> expectedReferences)
        {
            TestInsertRetrieveArtefacts(filename, sdmxSchema, structureReference, expectedReferences,
                () => GetMaintainables(StructureReferenceDetailEnumType.Descendants, structureReference));
        }

        private static IEnumerable<object> RetrieveDescendantsTestCases()
        {
            return new List<object>()
            {
                // DSD
                new object[] {
                    "tests/v21/dsd_NA_MAIN_1.6.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TEST_AGENCY123:NA_MAIN(1.6)"),
                    new List<IStructureReference>()
                },
                new object[] {
                    "tests/v21/dataflow_ESTAT+STS+2.0.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS_TEST(2.0)"),
                    GetDsdWithDescendants()
                },
                // Dataflow
                new object[] {
                    "tests/v21/dataflow_ESTAT+STS+2.0.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ESTAT:SSTSCONS_PROD_M_TEST(2.0)"),
                    new List<IStructureReference>()
                }
            };
        }

        private static List<IStructureReference> GetDsdWithDescendants()
        {
            List<IStructureReference> children = new List<IStructureReference>();

            // DSD
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS_TEST(2.0)"));

            // CONCEPTSCHEME
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=ESTAT:STS_SCHEME_TEST(1.0)"));

            // CODELISTS
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_ADJUSTMENT_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_AREA_EE_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_COLLECTION_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_DECIMALS_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_FREQ_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_OBS_CONF_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_OBS_STATUS_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_ORGANISATION_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ECB:CL_UNIT_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ESTAT:CL_AVAILABILITY_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ESTAT:CL_STS_ACTIVITY_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ESTAT:CL_STS_BASE_YEAR_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ESTAT:CL_STS_INDICATOR_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ESTAT:CL_STS_INSTITUTION_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ESTAT:CL_TIME_FORMAT_TEST(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ESTAT:CL_UNIT_MULT_TEST(1.0)"));

            return children;
        }
    }
}
