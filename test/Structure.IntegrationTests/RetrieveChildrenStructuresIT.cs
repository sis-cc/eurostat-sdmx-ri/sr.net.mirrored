// -----------------------------------------------------------------------
// <copyright file="RetrieveChildrenStructuresIT.cs" company="EUROSTAT">
//   Date Created : 2022-06-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------


namespace Structure.IntegrationTests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    //[TestFixture("odp")]
    //[TestFixture("mysql")]
    [TestFixture("sqlserver")]
    public class RetrieveChildrenStructuresIT : IntegrationTestBase
    {
        public RetrieveChildrenStructuresIT(string storeId)
            : base(storeId)
        {
        }

        [TestCaseSource(nameof(DsdTestParams))]
        public void DsdChildren(
            string filename, SdmxSchemaEnumType sdmxSchema, 
            IStructureReference structureReference, List<IStructureReference> expectedReferences)
        {
            TestInsertRetrieveArtefacts(filename, sdmxSchema, structureReference, expectedReferences,
                () => GetMaintainables(StructureReferenceDetailEnumType.Children, structureReference));
        }

        private static IEnumerable<object> DsdTestParams()
        {
            return new List<object>()
            {
                new object[] {
                    "tests/v21/dsd_NA_MAIN_1.6.xml", SdmxSchemaEnumType.VersionTwoPointOne,
                    new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TEST_AGENCY123:NA_MAIN(1.6)"),
                    GetDsdWithChildren()
                }
            };
        }

        private static List<IStructureReference> GetDsdWithChildren()
        {
            List<IStructureReference> children = new List<IStructureReference>();

            // DSD
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TEST_AGENCY123:NA_MAIN(1.6)"));

            // CONCEPTSCHEME
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=TEST_AGENCY123:CS_NA(1.6)"));

            // CODELISTS
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_STD123:CL_CONF_STATUS(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY123:CL_COICOP(1.1)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_STD123:CL_UNIT_MULT(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_STD123:CL_TIME_FORMAT(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY123:CL_SECTOR(1.4)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY123:CL_NA_PRICES(1.1)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY123:CL_ADJUSTMENT(1.4)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY9999:CL_TIME_COLLECT(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_STD123:CL_FREQ(2.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY123:CL_INSTR_ASSET(1.5)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY123:CL_ACTIVITY_TEST_CODELIST(1.3)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_STD123:CL_OBS_STATUS(2.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY9999:CL_ACCOUNT_ENTRY(1.2)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY123:CL_REF_PERIOD_DTL(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_STD123:CL_DECIMALS(1.0)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY123:CL_NA_STO(1.6)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY9999:CL_AREA(1.6)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY9999:CL_ORGANISATION(1.5)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY9999:CL_UNIT(1.6)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY123:CL_NA_TABLEID(1.5)"));
            children.Add(new StructureReferenceImpl("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=TEST_AGENCY123:CL_TRANSFORMATION(1.2)"));

            return children;
        }
    }
}

